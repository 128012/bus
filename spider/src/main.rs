/*

initial initialisation
- naptan
- construct services
- create Map 
  - from filtered services

assign services to segments - HashMap<usize,HashSet<String>>?

svg

top left
51.7809, -1.2751
bottom right
51.7766, -1.2651

*/


const MAGDALEN_STREET:(f32,f32)=(51.7543528376,-1.2590858453);
const WHEATLEY:f32=0.1196968192;

const BLENHEIM_DRIVE:(f64,f64)=(51.78118,-1.27485);
const OSBERTON:(f64,f64)=(51.78003, -1.26873);

// bbox
const WYNDHAM:(f64,f64)=(51.78242, -1.27718);
//const MICHAEL:(f64,f64)=(51.77972,-1.26438);
const OAKTHORPE:(f64,f64)=(51.77632,-1.26458);
const FAIRLAWN:(f64,f64)=(51.78657, -1.28056);
const BLADON:(f64,f64)=(51.78514, -1.27769);





use std::collections::{HashMap,HashSet,BTreeSet};
//use std::cmp::Ordering;
use petgraph::visit::EdgeRef;

use types::{service,stop};

//const RADIUS:f32=400.0;
//const POINTS:bool=false;
//const INTERSECTION:bool=true;

fn main(){
    let bbox=stops_on_roads::coordinates::BBox::new_wsen_degrees((FAIRLAWN.1,BLADON.0,BLADON.1,FAIRLAWN.0));
    let gtfs=gtfs_structures::Gtfs::from_path("../bus/data/downloaded/gtfs/obc")
        .expect("The GTFS parser is not happy");
    let mut naptan:HashMap<String,stop::Stop>=stop::read_naptan_csv("../bus/data/downloaded/naptan.csv").unwrap()
        .map(|stop|stop.unwrap())
        .map(|stop|(stop.atco.clone(),stop))
        .collect();
    for stop in naptan.values_mut(){
        if let Some(gtfs_stop)=gtfs.stops.get(&stop.atco){
            if let (Some(lat),Some(lng))=(gtfs_stop.latitude,gtfs_stop.longitude){
                stop.add_latlng((lat,lng))
            }
        }
    }
    let trips={
        let mut all_trips:HashMap::<_,Vec<service::Trip>>=HashMap::new();
        for route in gtfs.routes.values(){
            let trips:Vec<&gtfs_structures::Trip>=gtfs.trips.values()
                .filter(|trip|trip.route_id==route.id)
                .collect();
            if trips.iter().any(|trip|trip.stop_times.iter()
                // if GTFS stop has its position (they do so don't bother looking in NaPTAN)
                .filter_map(|stop_time|stop_time.stop.latitude
                    .and_then(|lat|stop_time.stop.longitude
                        .map(|long|(lat as f32,long as f32))
                    )
                )
                .any(|pos|stop::distance_squared(MAGDALEN_STREET,pos).sqrt()<WHEATLEY)
            ){
                let code=route.short_name.clone();
                all_trips.entry(code)
                    .or_default()
                    .extend(trips.into_iter()
                        .map(|trip|service::Trip::from_gtfs(trip,&gtfs))
                    );
            }
        }
        all_trips
    };
    let mut map=stops_on_roads::Map::new("../bus/data/downloaded/osm.json",&naptan);
    for (code,trips) in &trips{
        map.add_code(&code,&trips);
    }
    println!("{}",spider::Spider::new(&map).at_bbox(bbox));
}
fn old_main() {
    // WSEN
    //let bbox=(WYNDHAM.1,OAKTHORPE.0,OAKTHORPE.1,WYNDHAM.0);
    let bbox=(FAIRLAWN.1,BLADON.0,BLADON.1,FAIRLAWN.0);

    let gtfs=gtfs_structures::Gtfs::from_path("../bus/data/downloaded/gtfs/obc")
        .expect("The GTFS parser is not happy");

    let colours=colours(std::fs::read_to_string("../bus/data/service_colours.csv").unwrap());
    let mut naptan:HashMap<String,stop::Stop>=stop::read_naptan_csv("../bus/data/downloaded/naptan.csv").unwrap()
        .map(|stop|stop.unwrap())
        .map(|stop|(stop.atco.clone(),stop))
        .collect();
    for stop in naptan.values_mut(){
        if let Some(gtfs_stop)=gtfs.stops.get(&stop.atco){
            if let (Some(lat),Some(lng))=(gtfs_stop.latitude,gtfs_stop.longitude){
                stop.add_latlng((lat,lng))
            }
        }
    }

    let stops_in_bbox:HashSet<String>=naptan.iter()
        .filter(|(_,stop)|stop.lat
            .and_then(|lat|stop.lng
                .map(|lng|bbox.1<lat&&lat<bbox.3
                     &&bbox.0<lng&&lng<bbox.2
                 )
            )
            .unwrap_or(false)
        )
        .map(|(atco,_)|atco.to_string())
        .collect();
    for atco in &stops_in_bbox{
        eprintln!("{} {}",atco,naptan[atco].names[0]);
    }

    /*let mut biggest_nodeid=0;
    let mut node_positions:HashMap<usize,(f32,f32)>=HashMap::new(); // useful
    let mut roads:Vec<Road>=std::fs::read_to_string("../roads/out.txt")
        .expect("Couldn't read ../roads/out.txt")
        .lines()
        .skip(1)
        .map(|line|{
            let params=line.split(":").collect::<Vec<_>>();

            let points=params[3].split_whitespace()
                .map(Point::new)
                .collect::<Vec<_>>();

            let start_node=params[4].parse::<usize>().unwrap();
            let end_node=params[6].parse::<usize>().unwrap();

            node_positions.entry(start_node)
                .or_insert_with(||points[0].pos());
            node_positions.entry(end_node)
                .or_insert_with(||points.last().unwrap().pos());

            biggest_nodeid=std::cmp::max(std::cmp::max(biggest_nodeid,start_node),end_node);

            Road{
                name:params[1].to_string(),
                points,
                length:params[2].parse().unwrap(),

                start_node,
                end_node,
                
                unordered_services:HashSet::new(),

                ordered_services:Vec::new(),
            }
        })
        .collect();

    println!("Has read the file, after {}ms",reading.elapsed().as_millis());

    let (cx,cy)=(450516.31,209631.88);
    //let (cx,cy)=(449528.0,206278.0);
    //let (cx,cy)=(451785.0,204462.0);
    //let (cx,cy)=(452535.0,208557.0);
    //let (cx,cy)=(456156.85,207864.26);

    let naptan:HashMap<String,(f32,f32)>=csv::ReaderBuilder::new()
        .from_path("../bus/data/naptan.csv")
        .expect("Couldn't open `../bus/data/naptan.csv`")
        .records()
        .collect::<Result<Vec<_>,_>>()
        .expect("Error reading a record in ../bus/data/naptan.csv")
        .into_iter()
        .filter(|record|if let Some(x)=record.get(42){x!="del"}else{true})
        .map(|record|(record.get(0).unwrap().to_string(),(
            record.get(27).unwrap().parse::<f32>().unwrap(),
            record.get(28).unwrap().parse::<f32>().unwrap()
        )))
        .collect();*/

    // filtered naptan
    /*let stops_in_circle:HashMap<String,(f64,f64)>=naptan.iter()
        .filter(|stop::Stop{lat,lng}|stops_on_roads::coordinates::distance((lat,lng),centre)<RADIUS)
        .map(|(atco,stop::Stop{lat,lng})|(atco.to_string(),(lat,lng)))
        .inspect(|(atco,_)|println!("Stop: {}",atco))
        .collect();*/

    // read services

    /*let mut txc=txc::Builder::new();
    txc.add_dir("../bus/data/txc".as_ref()).unwrap();
    let txc=txc.build();*/

    let (trips,service_codes_in_bbox):(HashMap<String,Vec<service::Trip>>,HashSet<String>)={
        let mut all_trips:HashMap::<_,Vec<service::Trip>>=HashMap::new();

        for route in gtfs.routes.values()
            //.filter(|route|["6","S3"].contains(&route.short_name.as_str()))
        {
            let trips:Vec<&gtfs_structures::Trip>=gtfs.trips.values()
                .filter(|trip|trip.route_id==route.id)
                .collect();

            if trips.iter().any(|trip|trip.stop_times.iter()
                // if GTFS stop has its position (they do so don't bother looking in NaPTAN)
                .filter_map(|stop_time|stop_time.stop.latitude
                    .and_then(|lat|stop_time.stop.longitude
                        .map(|long|(lat as f32,long as f32))
                    )
                )
                .any(|pos|stop::distance_squared(MAGDALEN_STREET,pos).sqrt()<WHEATLEY)
            ){
                let code=route.short_name.clone();
                for trip in trips.iter(){
                    for stop_time in &trip.stop_times{
                        naptan.get_mut(&stop_time.stop.id)
                            .map(|stop|stop.service_codes.insert(code.clone()));
                    }
                }
                all_trips.entry(code)
                    .or_default()
                    .extend(trips.into_iter()
                        .map(|trip|service::Trip::from_gtfs(trip,&gtfs))
                    );
            }
        }
        //dbg!(all_trips.keys().collect::<Vec<_>>());
        let service_codes_in_bbox:HashSet<String>=stops_in_bbox.iter()
            .map(|atco|&naptan[atco].service_codes)
            .flatten()
            .map(|s|s.to_string())
            .collect();

        (
            all_trips.into_iter()
                .filter(|(code,_)|service_codes_in_bbox.contains(code))
                .collect(),
            service_codes_in_bbox
        )
    };

    let relevant_stops:HashMap<String,stop::Stop>=trips.values()
        .map(|trips|service::atco_pairs(trips).into_iter()
             .filter(|(a,b)|stops_in_bbox.contains(*a)||stops_in_bbox.contains(*b))
             .map(|(a,b)|[a,b])
             .flatten()
        )
        .flatten()
        // possibly not in NaPTAN;
        // and likely already removed on a previous iteration - no duplicates
        .filter_map(|atco|naptan.remove_entry(atco))
        .collect();
    eprintln!();
    for (atco,stop) in &relevant_stops{
        eprintln!("{} {}",atco,stop.names[0]);
    }


    //// at this point relevant_stops is just those stopping within the circle
    /*let services_in_circle:Vec<&txc::Service>=stops_in_circle.keys()
        .filter_map(|atco|txc.by_atco(atco) // might not be in txc
            .map(|stop|stop.served_by())
        )
        .flatten()
        .collect::<HashSet<String>>()
        .into_iter()
        .filter(|code|["2","500"].contains(&code.as_str()))
        .map(|code|txc.by_code(code.as_str()).unwrap())
        .inspect(|service|println!("Service: {}",service.code()))
        .collect();*/

    let mut map=stops_on_roads::Map::new("../bus/data/downloaded/osm.json",&relevant_stops);
    for (code,trips) in &trips{
        map.add_code(&trips,code.clone());
    }

    let graph=map.graph();

    // optimisation: collapse consecutive segments
    //let mut segments:Vec<(Vec<usize>,usize,usize)>=Vec::new();

    //let mut junctions:Vec<HashSet<usize>>=Vec::new();
    let mut junctions_with_lines:HashSet<usize>=HashSet::new();

    // segments is too long; mustn't iterate over it
    // a segment with multiple map segments will be one created by collapsing
    let (segments,map_segment_to_segment,eaten_up_nodes):(Vec<Vec<usize>>,HashMap<usize,(i8,usize)>,Vec<usize>)=collapse_consecutive_segments(&map);

    //let groups:HashMap<usize,HashSet<HashSet<String>>>;
    /*let groups:Vec<(usize,_)>=groups.iter()
        .map(|(segment,groups)|(segment,groups.iter().cycle()))
        .collect();*/

    // a group is a set of lines in a segment which both go to and come from the same places
    // map segment idx->set of groups of lines
    //                ==      [groups of lines]
    // - order within a group doesn't matter
    // - order of groups is not decided
    let mut groups:Vec<((BTreeSet<usize>,BTreeSet<usize>),HashSet<&str>)>=Vec::new();
    let mut unordered_groups_on_segment:HashMap<usize,Vec<usize>>=HashMap::new();

    // create groups
    for (segment_i,map_segment_idxs) in segments.iter().enumerate()
        .filter(|(_,idxs)|idxs.len()>0)
    {
        let mut groups_of_this_segment:HashMap<(BTreeSet<usize>,BTreeSet<usize>),HashSet<&str>>=HashMap::new();
        for code in &map.segments()[map_segment_idxs[0]].services{
            let from:BTreeSet<usize>=graph.edges(map.segments()[map_segment_idxs[0]].start_node.into())
                .map(|edge_reference|edge_reference.id().index())
                .filter(|id|*id!=map_segment_idxs[0]
                    &&map.segments()[*id].services.contains(code)
                )
                .collect();
            let to:BTreeSet<usize>=graph.edges(map.segments()[*map_segment_idxs.last().unwrap()].end_node.into())
                .map(|edge_reference|edge_reference.id().index())
                .filter(|id|id!=map_segment_idxs.last().unwrap()
                    &&map.segments()[*id].services.contains(code)
                )
                .collect();
            groups_of_this_segment.entry((from,to))
                .or_default()
                .insert(code);
        }
        if groups_of_this_segment.len()>0{
            junctions_with_lines.insert(map.segments()[map_segment_idxs[0]].start_node);
            junctions_with_lines.insert(map.segments()[*map_segment_idxs.last().unwrap()].end_node);
        }
        for (route,codes) in groups_of_this_segment{
            groups.push((route,codes));
            unordered_groups_on_segment.entry(segment_i)
                .or_default()
                .push(groups.len()-1);
        }
    }
    //dbg!(&groups);
    //dbg!(groups.len());

    // only of junctions with lines
    // could be a method of Map, but then the graph would have to be created again
    let clockwise_segments:HashMap<usize,Vec<usize>>=junctions_with_lines.iter()
        .map(|&node|{
            let mut bearings=graph.edges(node.into())
                .map(|edge|(
                    edge.id().index(),
                    map.segments()[edge.id().index()].angle_at_node(node) // increasing as next goes anticlockwise so need to reverse
                ))
                .collect::<Vec<_>>();
            bearings.sort_unstable_by(|(_,bearing1),(_,bearing2)|bearing2 // reverse
                .partial_cmp(bearing1)
                .unwrap()
            );
            (
                node,
                bearings.into_iter()
                    .map(|(map_segment_index,_)|map_segment_index) // don't need the bearings, just the order
                    .map(|map_segment_index|map_segment_to_segment[&map_segment_index].1) // segments not map segments
                    .collect::<Vec<usize>>()
            )
        })
        .collect();

    let cheapest_orderings:HashMap<usize,Vec<usize>>=cartesian_product(&unordered_groups_on_segment.into_iter()
        .map(|(segment,unordered_groups)|(segment,permutations(unordered_groups)))
        .collect::<Vec<(usize,Vec<Vec<usize>>)>>()
    ).into_iter()
        .map(|ordering_on_each_segment|{
            //dbg!(&ordering_on_each_segment);
            let cost=clockwise_segments.iter()
                .map(|(node,segments_around_node)|if segments_around_node.len()>1{ // not end of line
                    let clockwise_groups:Vec<Vec<usize>>=segments_around_node.iter()
                        .filter_map(|&segment_i|if map.segments()[segment_i].start_node==*node{
                            // ignore segments without lines - they are not in this map
                            ordering_on_each_segment.get(&segment_i).cloned()
                        }else{
                            ordering_on_each_segment.get(&segment_i).cloned()
                                .map(|mut r|{r.reverse();r})
                        })
                        .collect();
                    //dbg!(segments_around_node.len(),clockwise_groups.len());

                    // from,to,group id
                    let mut seen:HashSet<(usize,usize,usize)>=HashSet::new();
                    clockwise_groups.iter().enumerate()
                        // junction_entry all ignoring roads without lines
                        .map(|(junction_entry,groups)|{
                            //dbg!(groups.len());
                            groups.iter().enumerate()
                                .map(|(group_i_in_segment,group)|{
                                    let before=&groups[..group_i_in_segment];
                                    let after=&groups[group_i_in_segment+1..];

                                    clockwise_groups.iter().enumerate()
                                        .filter(|(to_junction_entry,_)|*to_junction_entry!=junction_entry)
                                        .filter_map(|(to_junction_entry,groups)|groups.iter()
                                            .position(|to_group|to_group==group)
                                            .map(|to_group_position|(to_junction_entry,to_group_position))
                                        )
                                        .map(|(to_junction_entry,to_group_position)|if seen.insert((junction_entry,to_junction_entry,*group)){
                                            clockwise_groups.iter().cycle()
                                                .skip(junction_entry+1)
                                                .take({
                                                    let number_to_take_maybe_negative:i8=(to_junction_entry as i8)-(junction_entry as i8);
                                                    //dbg!(number_to_take_maybe_negative);
                                                    if number_to_take_maybe_negative<0{ // to is before from in this circle
                                                        clockwise_groups.len()-1-TryInto::<usize>::try_into(number_to_take_maybe_negative.abs()).unwrap()
                                                    }else{
                                                        usize::try_from(number_to_take_maybe_negative).unwrap()-1
                                                    }
                                                })
                                                .flatten()
                                                .chain(clockwise_groups[to_junction_entry].iter()
                                                    .take(to_group_position)
                                                )
                                                .filter(|to_group|after.contains(to_group))
                                                .count()
                                            +clockwise_groups.iter().cycle()
                                                .skip(to_junction_entry+1)
                                                .take({
                                                    let number_to_take_maybe_negative:i8=(junction_entry as i8)-(to_junction_entry as i8);
                                                    //dbg!(number_to_take_maybe_negative);
                                                    if number_to_take_maybe_negative<0{ // from is before to in this circle
                                                        clockwise_groups.len()-1-TryInto::<usize>::try_into(number_to_take_maybe_negative.abs()).unwrap()
                                                    }else{
                                                        usize::try_from(number_to_take_maybe_negative).unwrap()-1
                                                    }
                                                })
                                                .flatten()
                                                .chain(clockwise_groups[to_junction_entry].iter()
                                                    .skip(to_group_position+1)
                                                )
                                                .filter(|to_group|before.contains(to_group))
                                                .count()
                                        }else{0})
                                        .sum::<usize>()
                                })
                                .sum::<usize>()
                        })
                        .sum::<usize>()
                }else{0})
                .sum::<usize>();
                (ordering_on_each_segment,cost)
        })
        .min_by_key(|(_,cost)|*cost)
        .unwrap()
        .0;
    //dbg!(&cheapest_orderings);

    // remove each when drawn to avoid duplications
    //let mut segment_orderings_to_remove=cheapest_orderings;
                                
    let mut lines=vec![];
    let mut roads=vec![];

    let bbox2=stops_on_roads::coordinates::BBox::new_wsen_degrees(bbox);
    //dbg!(map.project(bbox2));
    for (segment_i,[a,b]) in map.project(bbox2){
        let width_if_services=map.segments()[segment_i].services.len()*5;
        roads.push(format!(
            r##"<line stroke-width="{width}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
            width=if width_if_services==0{
                2
            }else{
                width_if_services
            },
            x1=a.0,y1=a.1,
            x2=b.0,y2=b.1
        ));
        // if there are groups/lines on this segment
        if let Some(this_segment_groups)=cheapest_orderings.get(&map_segment_to_segment[&segment_i].1){
            //dbg!("y");
            //if let [_,_,.]=segments[map_segment_of_segment[&segment_i]{
                /*let segment_direction=direction(
                    map.segments()[segments[*map_segment_of_segment[&segment_i]].0].unprojected_first_node,
                    map.segments()[segments[*map_segment_of_segment[&segment_i]].last().unwrap()].unprojected_last_node
                );*/
                //dbg!(a,b);
                let codes:Vec<&str>=this_segment_groups.iter()
                    .map(|&group_idx|&groups[group_idx].1)
                    .flatten()
                    .copied()
                    .collect();
                let (a,b)=if map_segment_to_segment[&segment_i].0==1{
                    (a,b)
                }else{
                    (b,a)
                };
                //assert_eq!(offsets_from_segment(a,b,&vec![0]),vec![(0,a,b)]);
                for (code,a,b) in offsets_from_segment(a,b,&codes){
                    //dbg!(a,b);
                    lines.push(format!(
                        r##"<line data-info="{code}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
                        code=code,
                        //segment=map_segment_to_segment[&segment_i].1,
                        x1=a.0,y1=a.1,
                        x2=b.0,y2=b.1
                    ));
                }
            //}
        }
    }
    //dbg!(offsets_from_segment((3.,5.),(7.,11.),&vec![1,2,3]));
    println!(
    r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg xmlns="http://www.w3.org/2000/svg" width="{w}" height="{h}">
        <style>{colours}</style>
        <rect fill="#fdfdda" x="0" y="0" width="{w}" height="{h}"/>
        <g stroke="white" stroke-linecap="round">{roads}</g>
        <g stroke-linecap="round" stroke-width="4">{lines}</g>
        <text x="{text_x}" y="{text_y}" text-anchor="end" font-size="8pt">
            Lines drawn from timetables (so may not show actual route)
        </text>
        </svg>"##,
        w=bbox2.width(),
        h=bbox2.height(),
        colours=service_codes_in_bbox.iter()
            .map(|code|format!(
                r##"[data-info="{code}"]{{stroke:{colour};}}"##,
                code=code,
                colour=colours.get(&code.to_lowercase()).map(|x|x.as_str()).unwrap_or("#ccc")
            ))
            .collect::<Vec<_>>()
            .join(""),
        text_x=bbox2.width()-10.,
        text_y=bbox2.height()-10.,
        roads=roads.join(""),
        lines=lines.join(""),
    );

    /*println!("{}",svg(
            map.project(bbox)
                .into_values()
                .collect::<Vec<_>>().as_slice()
    ,bbox));*/

    //println!("{}",map.svg_from_points_and_bbox(&[],None,bbox));
    /*println!("{}",map.svg_from_bbox_and_info(stops_on_roads::coordinates::BBox::new_wsen_degrees(bbox),&HashMap::new()/*cheapest_orderings.into_iter()
        .map(|(segment,groups)|(segment,groups.into_iter().map(|x|x.to_string()).collect::<Vec<_>>().join(" ")))
        .collect()*/
    ));*/





/*
    // having read services
    // - find the relevant stops just outside the circle
    // - tell all the relevant stops their services
    // - tell all the relevant stops the next stop for each line

    // at this point not including stops just outside the circle; that happens after services are read
    //                             atco   (node     pos   )
    let mut relevant_stops:HashMap<String,(usize,(f64,f64))>=services.into_iter()
        .flat_map(|trips|trips.iter()
            .flat_map(|trip|trip.visits.windows(2)
                .filter_map(|visits|stops.get(&visits[0].atco)
                    .and_then(|a|stops.get(&visits[1].atco)
                        .map(|b|(a,b))
                    )
                )
            )
        )
        .collect::<HashSet<stop::Stop>>()
        .into_iter()
        .filter_map(|stop::Stop{atco,lat,lng}|map.stop_on_road((lat,lng))
            .map(|node|(atco.clone(),(node,(lat,lng))))
        )
        .collect();

    /*let mut service_stop_pairs:HashMap<String,HashSet<(String,String)>>=HashMap::new();

    for service in &services_in_circle{
        let mut all_this_service_stops:HashSet<String>=HashSet::new();
        /*service.stops().into_iter()
            .filter_map(|atco|naptan.get(&atco) // in NaPTAN
                .map(|stop|(atco,stop))
            )
            .collect::<Vec<_>>();*/

        for journey in service.journeys(){
            for pair in journey.visits()
                .filter_map(|cell|cell.move_visit())
                .map(|visit|visit.atco)
                .filter(|atco|naptan.contains_key(atco.as_str())
                //    .map(|pos|(visit.atco,*pos))
                )
                .collect::<Vec<String>>()
                .windows(2)
                .map(|two_stops|if two_stops[0]>two_stops[1]{ // order doesn't matter
                    (two_stops[1].clone(),two_stops[0].clone())
                }else{
                    (two_stops[0].clone(),two_stops[1].clone())
                })
                // either of these in the circle+pair is not seen before? If not, don't bother with them
                .filter(|two_stops|stops_in_circle.contains_key(&two_stops.0)
                    ||stops_in_circle.contains_key(&two_stops.1)
                )
            {
                // doesn't matter if it is already there
                service_stop_pairs.entry(service.code().to_lowercase())
                    .or_default()
                    .insert(pair.clone());

                all_this_service_stops.insert(pair.0.clone());
                all_this_service_stops.insert(pair.1);
            }
        }

        // put this service's stops on roads and add them to relevant_stops
        for atco in all_this_service_stops{
            relevant_stops.entry(atco.clone())
                .or_insert_with(||{
                    let xy=naptan.get(&atco).unwrap();

                    let (road_i,road)=roads.iter().enumerate()
                        .min_by_key(|(_,road)|road.points.windows(2)
                            .map(|points|line_segment_to_point(
                                points[0].pos(),
                                points[1].pos(),
                                *xy
                            )as u32)
                            .min()
                        ).unwrap();

                    // which of the segments of that road
                    let part=road.points.windows(2).enumerate()
                        .min_by_key(|(_,points)|line_segment_to_point(
                                points[0].pos(),
                                points[1].pos(),
                                *xy
                        )as u32).unwrap().0;

                    // split this road into two
                    let (a,b)=roads.remove(road_i).split(part,Point::Stop(atco.clone(),(0.0,0.0),*xy),biggest_nodeid);
                    assert!(a.points.len()>=2);
                    assert!(b.points.len()>=2);
                    //assert!(a.points.iter().filter(|p|p.not_stop().is_some()).count()>=2);
                    //assert!(b.points.iter().filter(|p|p.not_stop().is_some()).count()>=2);

                    roads.push(a);
                    roads.push(b);

                    node_positions.insert(biggest_nodeid+1,*xy);
                    biggest_nodeid+=1;

                    (biggest_nodeid,*xy)
                });
        }
    }

    // edges and nodes will stay the same from here

    //                        node weight weight                      node x
    let graph:petgraph::Graph<u8, /*=0*/  u16,   petgraph::Undirected,usize >=petgraph::Graph::from_edges(
        roads.clone()
    );*/

    // for each node, the indices of the roads, in order
    let clockwise_roads:Vec<Vec<usize>>=graph.node_indices()
        .map(|node|{
            let node_index=node.index();
            let mut bearings=graph.edges(node)
                .map(|edge|{
                    let road_index=edge.id().index();

                    // the point after this node
                    let next_point=if roads[road_index].points[0].pos()==node_positions[&node_index]{
                        roads[road_index].points.get(1)
                    }else{
                        roads[road_index].points.last()
                    }.unwrap();

                    let x=node_positions[&node_index].0-next_point.pos().0;
                    let y=node_positions[&node_index].1-next_point.pos().1;

                    (
                        road_index,
                        match x.partial_cmp(&0.0).unwrap(){ // gradient is ambiguous
                            Ordering::Less   =>270.0-(y/x).atan(),
                            Ordering::Greater=>90.0-(y/x).atan(),
                            Ordering::Equal  =>std::f32::consts::PI // 180deg
                        }
                    )
                })
                .collect::<Vec<_>>();
            bearings.sort_unstable_by(|(_,bearing1),(_,bearing2)|bearing1
                .partial_cmp(bearing2)
                .unwrap()
            );
            bearings.into_iter()
                .map(|(road_index,_)|road_index) // don't need the bearings, just the order
                .collect::<Vec<usize>>()
        })
        .collect();

    {
        for (i,to_roads) in clockwise_roads.iter().enumerate(){
            let mut a=to_roads.clone();
            let mut b=graph.edges(i.into()).map(|edge|edge.id().index()).collect::<Vec<_>>();
            a.sort();
            b.sort();
            assert_eq!(a,b);
            assert_eq!(graph.edges(i.into()).count(),to_roads.len());
        }
        let mut x=clockwise_roads.iter().flatten().collect::<Vec<_>>();
        x.sort_unstable();
        x.dedup();
        assert!(x.into_iter().zip(0..).all(|(x,y)|*x==y));
    }

    /*//                                                two road indices
    // - like node indices but knowing the roads entering and leaving
    let mut service_road_middle_pairs:HashMap<String,HashSet<(usize,usize)>>=HashMap::new();

    // use these pairs tock tell each road its services
    for (code,pairs) in &service_stop_pairs{
        for pair in pairs{
            let to_index=relevant_stops[&pair.1].0;
            let path=petgraph::algo::astar(
                &graph,
                petgraph::graph::NodeIndex::new(relevant_stops[&pair.0].0),
                |maybe_to|maybe_to.index()==to_index,
                |edge|*edge.weight(),
                |_|0 // no useful heuristic -> just Dijkstra
            ).unwrap().1;
            //dbg!(path_length,&path);
            path.into_iter()
                .map(|node_index|node_index.index())
                .collect::<Vec<_>>()
                .windows(2)
                // tell the road its services
                // and return the road id
                .map(|node_pair|{
                    let road_index=graph.find_edge(
                        node_pair[0].into(),
                        node_pair[1].into()
                    ).unwrap().index();
                    roads[road_index].unordered_services.insert(code.clone());
                    road_index
                })
                .collect::<Vec<_>>()
                .windows(2)
                .for_each(|road_index_pair|{
                    service_road_middle_pairs
                        .entry(code.clone())
                        .or_default()
                        .insert(if road_index_pair[0]<road_index_pair[1]{
                            (road_index_pair[0],road_index_pair[1])
                        }else{
                            (road_index_pair[1],road_index_pair[0])
                        });
                });
        }
    }*/
    /*dbg!(roads.iter()
        .filter(|road|road.unordered_services.len()>0)
        .collect::<Vec<_>>()
    );*/

    for (service,mut road_middle_pairs) in service_road_middle_pairs{
        // for each path
        // make each path long but there is no good algorithm for finding the longest path
        while let Some(pair)=road_middle_pairs.iter().next(){
            let pair=*pair;

            // a,b are two initial road indices
            let (a,b)=road_middle_pairs.take(&pair).unwrap();
            let mut path=vec![a,b];

            // add to the beginning and end of the path until there is nothing left
            while let Some(to_remove)=road_middle_pairs.iter()
                .find_map(|&(a,b)|{
                    if a==*path.last().unwrap(){
                        path.push(b);
                        Some((a,b))
                    }else if b==*path.last().unwrap(){
                        path.push(a);
                        Some((a,b))
                    }else if a==path[0]{
                        path.splice(0..0,std::iter::once(b));
                        Some((a,b))
                    }else if b==path[0]{
                        path.splice(0..0,std::iter::once(a));
                        Some((a,b))
                    }else{
                        None
                    }
                })
            {
                road_middle_pairs.remove(&to_remove);
            }
            //dbg!(dbg!(&path).iter().map(|x|&roads[*x].name).collect::<Vec<_>>());

            add_path_to_ordered_services(&path,&service,&mut roads,&clockwise_roads);
        }
    }

    let top_left_pos=(cx-RADIUS,cy-RADIUS);

    let mut points_not_in_middle=roads.iter()
        .map(|road|road.lines().into_iter()
            .map(|(code,points)|(code,points.into_iter()
                .map(|point|relative(point,top_left_pos))
                .collect()
            ))
            .collect()
        )
        .collect::<Vec<HashMap<String,Vec<(f32,f32)>>>>();

    if INTERSECTION{
        for (i,to_roads) in clockwise_roads.iter().enumerate(){
            for &road1_i in to_roads{
                for code in points_not_in_middle[road1_i].keys().cloned().collect::<Vec<_>>(){
                    let a=if roads[road1_i].end_node==i{
                        points_not_in_middle[road1_i][&code].windows(2).last().unwrap()
                    }else{
                        points_not_in_middle[road1_i][&code].windows(2).next().unwrap()
                    };

                    to_roads.iter()
                        .filter(|&road2_i|*road2_i!=road1_i)
                        .filter_map(|&road2_i|points_not_in_middle[road2_i].get(&code)
                            .and_then(|_|{
                                let b=if roads[road2_i].start_node==i{
                                    points_not_in_middle[road2_i][&code].windows(2).next().unwrap()
                                }else{
                                    points_not_in_middle[road2_i][&code].windows(2).last().unwrap()
                                };
                                intersection((a[0],a[1]),(b[0],b[1]))
                                    .map(|xy|(road2_i,xy))
                            })
                        )
                        .collect::<Vec<_>>().into_iter()
                        .for_each(|(road2_i,xy)|for &road_i in [road1_i,road2_i].iter(){
                            if roads[road_i].start_node==i{
                                *points_not_in_middle[road_i].get_mut(&code).unwrap().get_mut(0).unwrap()=xy;
                            }else{
                                assert!(roads[road_i].end_node==i);
                                *points_not_in_middle[road_i].get_mut(&code).unwrap().last_mut().unwrap()=xy;
                            }
                        });
                }
            }
        }
    }

    for (i,road ) in roads.iter().enumerate(){
        if road.ordered_services.len()>0{//[245754,245757].contains(&i){
            println!("Road {}: {:?} {}",i,road.ordered_services,road.is_upwards());
        }
    }





        /*for road_i in 0..roads.len(){
            for code in points_not_in_middle[road_i].keys().cloned().collect::<Vec<_>>(){
                for (end_i,&end) in [roads[road_i].start_node,roads[road_i].end_node].iter().enumerate(){
                    let a=if end_i==1{
                        points_not_in_middle[road_i][&code].windows(2).last().unwrap()
                    }else{
                        points_not_in_middle[road_i][&code].windows(2).next().unwrap()
                    };

                    let mut intersecting_roads=clockwise_roads[end].iter()
                        .filter(|a_road_i|**a_road_i!=road_i)
                        .filter_map(|&other_road_i|{
                            if points_not_in_middle[other_road_i].contains_key(&code){
                                let b=if roads[other_road_i].end_node==end{
                                    points_not_in_middle[other_road_i][&code].windows(2).last().unwrap()
                                }else{
                                    points_not_in_middle[other_road_i][&code].windows(2).next().unwrap()
                                };
                                intersection((a[0],a[1]),(b[0],b[1]))
                                    .map(|xy|(other_road_i,xy))
                            }else{
                                None
                            }
                        });

                    if let Some((other_road_i,xy))=intersecting_roads.next(){
                        if intersecting_roads.next().is_none(){
                            (if end_i==0{
                                *points_not_in_middle[road_i].get_mut(&code).unwrap().get_mut(0).unwrap()=xy;
                            }else{
                                *points_not_in_middle[road_i].get_mut(&code).unwrap().last_mut().unwrap()=xy;
                            });
                            (if roads[other_road_i].end_node==end{
                                *points_not_in_middle[other_road_i].get_mut(&code).unwrap().last_mut().unwrap()=xy;
                            }else{
                                *points_not_in_middle[other_road_i].get_mut(&code).unwrap().get_mut(0).unwrap()=xy;
                            });
                        }
                    }
                }
            }
        }
    }*/


    let svg=format!(
        r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 {diameter} {diameter}">
        <defs><mask id="circle">
        <rect fill="white" width="{diameter}" height="{diameter}"/>
        <circle r="{radius}" cx="{radius}" cy="{radius}" fill="black"/>
        </mask></defs>
        <g stroke="#ccc" stroke-width="6">{roads}</g>
        <g stroke="red" stroke-linecap="round" stroke-width="1">{lines}{joins}</g>
        <g>{points}</g>
        <rect width="{diameter}" height="{diameter}" fill="purple" mask="url(#circle)"/>
        </svg>"##,
        diameter=RADIUS*2 as f32,
        radius=RADIUS,
        roads=roads.iter()
            .filter(|road|road.through_circle((cx,cy)))
            .map(|road|road.svg(top_left_pos))
            .collect::<Vec<_>>()
            .join(""),
        lines=points_not_in_middle.iter().enumerate()
            .map(|(i,lines)|lines.iter()
                .map(|(code,points)|path(
                    &points,
                    None,
                    &format!("{}+{}",code,i),
                    colours.get(code.as_str()).map(|x|x.as_str())
                ))
                .collect::<Vec<_>>()
                .join("")
            )
            .collect::<Vec<_>>()
            .join(""),
        // doesn't need to be clockwise, but this is easier
        // than dealing with petgraph
        joins=clockwise_roads.iter()
            .map(|to_roads|{
                // don't want to add each pair twice once forwards and once backwards
                let mut already_joined_pairs=HashSet::<(usize,usize)>::new();
                to_roads.iter().map(|&road1_i|{
                    let road1=&roads[road1_i];
                    to_roads.iter()
                        // in order so they can be added to already_joined_pairs
                        .map(|&road2_i|if road1_i<road2_i{
                            (road1_i,road2_i)
                        }else{
                            (road2_i,road1_i)
                        })
                        .filter(|&(road1_i,road2_i)|road2_i!=road1_i
                            &&!already_joined_pairs.contains(&(road1_i,road2_i))
                        )
                        .collect::<Vec<_>>().into_iter()
                        .map(|(_,road2_i)|{
                            already_joined_pairs.insert((road1_i,road2_i));

                            let road2=&roads[road2_i];

                            points_not_in_middle[road1_i].iter()
                                .filter_map(|(code,road1_points)|points_not_in_middle[road2_i].get(code.as_str())
                                    .map(|road2_points|(code,if road1.end_node==road2.start_node{
                                        (*road1_points.last().unwrap(),road2_points[0])
                                    }else if road1.end_node==road2.end_node{
                                        (*road1_points.last().unwrap(),*road2_points.last().unwrap())
                                    }else if road1.start_node==road2.start_node{
                                        (road1_points[0],road2_points[0])
                                    }else if road1.start_node==road2.end_node{
                                        (road1_points[0],*road2_points.last().unwrap())
                                    }else{unreachable!()}))
                                )
                                .filter(|(_,(xy1,xy2))|xy1!=xy2)
                                .map(|(code,((x1,y1),(x2,y2)))|format!(
                                    r##"<line data-service-code="{code}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"{stroke}/>"##,
                                    code=code,
                                    x1=x1,y1=y1,
                                    x2=x2,y2=y2,
                                    stroke=colours.get(code.as_str())
                                        .map(|colour|format!(r##" stroke="{}""##,colour))
                                        .unwrap_or_else(String::new)
                                ))
                                .collect::<Vec<_>>()
                                .join("")
                        })
                        .collect::<Vec<_>>()
                        .join("")
                })
                .collect::<Vec<_>>()
                .join("")
            })
            .collect::<Vec<_>>()
            .join(""),
        points=if POINTS{
            points_not_in_middle.iter()
                .map(|lines|lines.iter()
                    .map(|(code,points)|points.iter()
                        //.map(|point|relative(*point,top_left_pos))
                        .map(|(x,y)|format!(
                            r##"<circle r="2" cx="{}" cy="{}" fill="{}" stroke="none" data-service-code="{}"/>"##,
                            x,y,
                            colours.get(code.as_str()).map(|x|x.as_str()).unwrap_or("black"),
                            code
                        ))
                        .collect::<Vec<_>>()
                        .join("")
                    )
                    .collect::<Vec<_>>()
                    .join("")
                )
                .collect::<Vec<_>>()
                .join("")
        }else{
            String::new()
        }
        /*lines=roads.iter()
            .map(|road|road.lines_as_svg(top_left_pos,&colours))
            .collect::<Vec<_>>()
            .join("")*//*""lines.iter()
            .map(|(code,sections)|sections.iter()
                .map(|section|path(
                    &section.windows(2)
                        .flat_map(|two_nodes|&roads[graph.find_edge(
                                two_nodes[0].into(),
                                two_nodes[1].into()
                            ).unwrap().index()].points
                        )
                        .map(|point|point.pos())
                        .collect::<Vec<_>>(),
                    (cx-RADIUS,cy-RADIUS),
                    code
                ))
                .collect::<Vec<_>>()
                .join("")
            )
            .collect::<Vec<_>>()
            .join("")*/
    );
    std::fs::write("/tmp/svg",svg).expect("Error writing file");

    println!("Edges: {}",graph.edge_count());*/

}

/*#[test]
fn test_not_overlap(){
    let mut straight_roads=vec![
        Road{
            name:"a".to_string(),
            points:vec![Point::Point(1.0,2.0),Point::Point(1.0,4.0)],
            length:2,
            start_node:0,
            end_node:1,
            unordered_services:["X","Y"].into_iter().map(|x|x.to_string()).collect(),
            ordered_services:Vec::new()
        },
        Road{
            name:"b".to_string(),
            points:vec![Point::Point(2.0,4.0),Point::Point(2.0,8.0)],
            length:4,
            start_node:1,
            end_node:2,
            unordered_services:["X","Y"].into_iter().map(|x|x.to_string()).collect(),
            ordered_services:Vec::new()
        }
    ];
    assert!(straight_roads.iter().all(|road|road.is_upward()));
    let clockwise_roads=vec![
        vec![0],
        vec![0,1],
        vec![1]
    ];
    add_path_to_ordered_services(&[0,1],"a",&mut straight_roads,&clockwise_roads);
    dbg!(&straight_roads);
    add_path_to_ordered_services(&[0,1],"b",&mut straight_roads,&clockwise_roads);
    dbg!(&straight_roads);
    assert!(straight_roads[0].ordered_services==straight_roads[1].ordered_services);

    let mut downwards=vec![
        Road{
            name:"a".to_string(),
            points:vec![Point::Point(2.0,8.0),Point::Point(2.0,4.0)],
            length:2,
            start_node:0,
            end_node:1,
            unordered_services:["X","Y"].into_iter().map(|x|x.to_string()).collect(),
            ordered_services:Vec::new()
        },
        Road{
            name:"b".to_string(),
            points:vec![Point::Point(1.0,4.0),Point::Point(1.0,2.0)],
            length:4,
            start_node:1,
            end_node:2,
            unordered_services:["X","Y"].into_iter().map(|x|x.to_string()).collect(),
            ordered_services:Vec::new()
        }
    ];
    assert!(downwards.iter().all(|road|!road.is_upward()));
    let clockwise_roads=vec![
        vec![0],
        vec![0,1],
        vec![1]
    ];
    add_path_to_ordered_services(&[0,1],"a",&mut downwards,&clockwise_roads);
    dbg!(&straight_roads);
    add_path_to_ordered_services(&[0,1],"b",&mut downwards,&clockwise_roads);
    dbg!(&straight_roads);
    assert!(straight_roads[0].ordered_services==straight_roads[1].ordered_services);
}*/



fn permutations<T:Clone>(mut of:Vec<T>)->Vec<Vec<T>>{
    fn generate<T:Clone>(n:usize,a:&mut Vec<T>)->Vec<Vec<T>>{
        if n == 1 {
            vec![a.to_vec()]
        }else{
            let mut r=vec![];
            for i in  0..n-1{
                r.append(&mut generate(n - 1, a));
                if n%2==0{
                    a.swap(i,n-1);
                }
                else {
                    a.swap(0,n-1);
                }
            }
            r.append(&mut generate(n-1,a));
            return r;
        }
    }
    return generate(of.len(),&mut of);
}

// maybe?
fn cartesian_product(of:&[(usize,Vec<Vec<usize>>)])->Vec<HashMap<usize,Vec<usize>>>{
    if of.len()==0{ // only when called from elsewhere;
                    // shouldn't happen when recursing
        vec![]
    }else if of.len()>1{
        let after=cartesian_product(&of[1..]);
        of[0].1.iter()
            .flat_map(|ordering|{
                after.iter()
                    .cloned()
                    .map(move|segments|{
                        let mut r=[(of[0].0,ordering.clone())].into_iter().collect::<HashMap<usize,Vec<usize>>>();
                        r.extend(segments);
                        r
                    })
            })
            .collect()
    }else{
        //errorindex out of bounds: the len is 0 but the index is 0
        of[0].1.iter()
            .cloned()
            .map(|ordering|[(of[0].0,ordering)].into_iter().collect())
            .collect()
    }
}

fn direction(a:(f64,f64),b:(f64,f64))->i8{
    // upwards or the left
    if a.1<b.1||(a.1==b.1 && a.0>b.0){
        1
    }else{ // downwards or the right
        -1
    }
}

fn offsets_from_segment<T:Copy>(a:(f64,f64),b:(f64,f64),codes:&[T])->Vec<(T,(f64,f64),(f64,f64))>{
    // https://www.desmos.com/calculator/wecddwzzxy
    // that is a version of this code
    let m=if a.1==b.1{
        9999.
    }else if a.0==b.0{
        0.
    }else{
        -1./((b.1-a.1)/(b.0-a.0))
    };

    // upwards or the left
    let reverse=if a.1<b.1||(a.1==b.1 && a.0>b.0){
        1.
    }else{ // downwards or the right
        -1.
    };

    let ac=a.1-m*a.0;
    let bc=b.1-m*b.0;

        /*if a.0==b.0{
        a.0
    }else{
        (b.1-a.1)/(b.0-a.0)
    };*/
    let sqrt_one_plus_m_squared=(1.+m.powi(2)).sqrt();
    let n_minus_one_divided_by_two=((codes.len() as f64)-1.)/2.;

    let width=5;

    codes.iter().copied().enumerate().map(|(i,code)|{
        let d=width as f64*(i as f64-n_minus_one_divided_by_two);
        let dx=(d as f64/sqrt_one_plus_m_squared)*reverse;
        let ax=a.0+dx;
        let ay=m*ax+ac;
        let bx=b.0+dx;
        let by=m*bx+bc;
        (code,(ax,ay),(bx,by))
    }).collect()
}

fn collapse_consecutive_segments(map:&stops_on_roads::Map)->(Vec<Vec<usize>>,HashMap<usize,(i8,usize)>,Vec<usize>){//,Vec<HashSet<usize>>,HashSet<usize>){
    let graph=map.graph();

    let mut map_segment_to_segment:HashMap<usize,(i8,usize)>=HashMap::new();
    // may contain too many segments; the opposite hashmap is correct
    let mut segments:Vec<Vec<usize>>=Vec::new();
    let mut nodes_eaten_up:Vec<usize>=Vec::new();

    for node in graph.node_indices(){
        let edges:Vec<usize>=graph.edges(node)
            .map(|edge|edge.id().index())
            .collect();
        // services of each segment at this junction
        /*let these_segments:Vec<(usize,&Segment)>=edges.iter().enumerate()
            .map(|(i,edge)|(i,&map.segments()[*edge]))
            .collect();*/
        let edges_with_lines:Vec<usize>=edges.iter()
            .copied()
            .filter(|&edge|map.segments()[edge].services.len()>0)
            .collect();
        let edges_without_lines:Vec<usize>=edges.iter()
            .copied()
            .filter(|&edge|map.segments()[edge].services.len()==0)
            .collect();
        /*let segments_with_lines:Vec<(usize,&Segment)>=these_segments.iter()
            .filter(|(_,segment)|segment.services.len()>0)
            .copied()
            .collect();
        let segments_without_lines:Vec<(usize,&Segment)>=these_segments.iter()
            .filter(|(_,segment)|segment.services.len()==0)
            .copied()
            .collect();*/

        if edges_with_lines.len()==2 // could be more permissive? crossroads etc.
            && map.segments()[edges_with_lines[0]].services.len()>0
            && map.segments()[edges_with_lines[1]].services.len()>0
            && map.segments()[edges_with_lines[0]].services==map.segments()[edges_with_lines[1]].services
        {
            nodes_eaten_up.push(node.index());
            /*let edges_without_lines:Vec<usize>=these_segments.iter()
                .filter(|(_,segment)|!segment.services.len()==0)
                .map(|(edge,_)|edge)
                .collect();*/

            if edges_with_lines.iter().all(|edge|!map_segment_to_segment.contains_key(edge)){
                /*let edges_with_lines:Vec<usize>=segments_with_lines.into_iter()
                    .map(|(edge,_)|edge)
                    .collect();*/

                let reverse_a=if map.segments()[edges_with_lines[0]].end_node==node.index(){1}else{-1};
                let reverse_b=if map.segments()[edges_with_lines[1]].start_node==node.index(){1}else{-1};

                map_segment_to_segment.insert(edges_with_lines[0],(reverse_a,segments.len()));
                map_segment_to_segment.insert(edges_with_lines[1],(reverse_b,segments.len()));

                segments.push(vec![
                    edges_with_lines[0],
                    edges_with_lines[1]
                ]);
                dbg!(node.index(),edges_with_lines[1],
                    edges_with_lines[0]);
            }else{
                let (existing_segment_idx,map_segments_to_add)=if edges_with_lines.iter().all(|edge|map_segment_to_segment.contains_key(edge)){
for &(_,segment) in map_segment_to_segment.values(){
    assert!(segments[segment].len()>0);
}
                    let segment_a=map_segment_to_segment[&edges_with_lines[0]].1;

                    assert!(segments[segment_a].len()>0);
                    assert!(segments[map_segment_to_segment[&edges_with_lines[1]].1].len()>0);
                    dbg!(&segments[dbg!(segment_a)]);
                    dbg!(&segments[map_segment_to_segment[&edges_with_lines[1]].1]);
                    //dbg!(&segments);

                    /*for &map_segment_idx in &segments[map_segment_to_segment[&edges_with_lines[1]]]{
                        dbg!(map_segment_idx);
                        *map_segment_to_segment.get_mut(&map_segment_idx).unwrap()=segment_a;
                    }*/

                    //dbg!(&segments);

                    let map_segments_to_add=segments[map_segment_to_segment[&edges_with_lines[1]].1].split_off(0);
for (&map_segment,&(_,segment)) in &map_segment_to_segment{
    if !map_segments_to_add.contains(&map_segment){
        assert!(segments[segment].len()>0);
    }
}

                    //dbg!(&segments);
                    dbg!(&map_segments_to_add);

                    (segment_a,map_segments_to_add) // remove map segments from segment b
                }else if edges_with_lines.iter().any(|edge|map_segment_to_segment.contains_key(edge)){
                    let (already,other)=if map_segment_to_segment.contains_key(&edges_with_lines[0]){
                        (edges_with_lines[0],edges_with_lines[1])
                    }else{
                        (edges_with_lines[1],edges_with_lines[0])
                    };
                    let segment_idx=map_segment_to_segment[&already].1;

                    assert!(segments[segment_idx].len()>0);
                    dbg!(&segments[segment_idx]);

                    //map_segment_to_segment.insert(other,segment_idx);//get_mut(&other).unwrap()=segment_idx;

                    (segment_idx,vec![other])
                }else{
                    unreachable!()
                };
for (&map_segment,&(_,segment)) in &map_segment_to_segment{
    if !map_segments_to_add.contains(&map_segment){
        assert!(segments[segment].len()>0);
    }
}

                dbg!(&map_segments_to_add);
                let (a_start,a_end)=(
                    if map_segment_to_segment[&segments[existing_segment_idx][0]].0==1{
                        map.segments()[segments[existing_segment_idx][0]].start_node
                    }else{
                        map.segments()[segments[existing_segment_idx][0]].end_node
                    },
                    if map_segment_to_segment[segments[existing_segment_idx].last().unwrap()].0==1{
                        map.segments()[*segments[existing_segment_idx].last().unwrap()].end_node
                    }else{
                        map.segments()[*segments[existing_segment_idx].last().unwrap()].start_node
                    }
                );
                let (b_start,b_end)=(
                    if let Some((-1,_))=map_segment_to_segment.get(&map_segments_to_add[0]){
                        map.segments()[map_segments_to_add[0]].end_node
                    }else{
                        map.segments()[map_segments_to_add[0]].start_node
                    },
                    if let Some((-1,_))=map_segment_to_segment.get(map_segments_to_add.last().unwrap()){
                        map.segments()[*map_segments_to_add.last().unwrap()].start_node
                    }else{
                        map.segments()[*map_segments_to_add.last().unwrap()].end_node
                    }
                );
for (&map_segment,&(_,segment)) in &map_segment_to_segment{
    if !map_segments_to_add.contains(&map_segment){
        assert!(segments[segment].len()>0);
    }
}

                dbg!(&segments[existing_segment_idx]);
                /*dbg!(
                    map.segments()[segments[existing_segment_idx][0]].end_node,
                    map.segments()[*segments[existing_segment_idx].last().unwrap()].start_node
                );*/

                dbg!(a_start,a_end);
                dbg!(b_start,b_end);
                dbg!(map.segments()[map_segments_to_add[0]].start_node,map.segments()[*map_segments_to_add.last().unwrap()].end_node);
                let reverse_b=if       a_end==b_start{
                    segments[existing_segment_idx].append(&mut map_segments_to_add.clone());
                    dbg!(&segments[existing_segment_idx]);
                    1
                }else if a_end==b_end{
                    let mut reversed=map_segments_to_add.clone();
                    reversed.reverse();
                    /*for edge in &reversed{
                        map_segment_to_segment.get_mut(edge).unwrap().0*=-1;
                    }*/
                    segments[existing_segment_idx].append(&mut reversed);
                    dbg!(&segments[existing_segment_idx]);
                    -1
                }else if a_start==b_end{
                    segments[existing_segment_idx].splice(0..0,map_segments_to_add.clone());
                    dbg!(&segments[existing_segment_idx]);
                    1
                }else if a_start==b_start{
                    let mut reversed=map_segments_to_add.clone();
                    reversed.reverse();
                    /*for edge in &reversed{
                        map_segment_to_segment.get_mut(edge).unwrap().0*=-1;
                    }*/
                    segments[existing_segment_idx].splice(0..0,reversed);
                    dbg!(&segments[existing_segment_idx]);
                    -1
                }else{
                    unreachable!();
                };
for (&map_segment,&(_,segment)) in &map_segment_to_segment{
    if !map_segments_to_add.contains(&map_segment){
        assert!(segments[segment].len()>0);
    }
}

                for &map_segment_idx in &map_segments_to_add{
                    map_segment_to_segment.insert(
                        map_segment_idx,
                        (
                            map_segment_to_segment.get(&map_segment_idx)
                                .map(|(r,_)|*r)
                                .unwrap_or(1)*reverse_b,
                            existing_segment_idx
                        )
                    );
                }
for map_segments in &segments{
//assert_ne!(map_segments.len(),1);
for x in map_segments.windows(2){
let a=x[0];
let b=x[1];
let node_a=if map_segment_to_segment[&a].0==1{
    map.segments()[a].end_node
}else{
    map.segments()[a].start_node
};
let node_b=if map_segment_to_segment[&b].0==1{
    map.segments()[b].start_node
}else{
    map.segments()[b].end_node
};
//dbg!(&map_segments);
assert_eq!(node_a,node_b);
}
}

            }
for (_,&(_,segment)) in &map_segment_to_segment{
    assert!(segments[segment].len()>0);
}
            for &edge in &edges_without_lines{
                if !map_segment_to_segment.contains_key(&edge){
                    map_segment_to_segment.insert(edge,(1,segments.len()));
                    segments.push(vec![edge]); //// not read but need to increase the length
                }
            }
for (_,&(_,segment)) in &map_segment_to_segment{
    assert!(segments[segment].len()>0);
}
        }else{
            //dbg!("x");
            // in the other if branch the junction is subsumed
            for &edge in &edges{
                if !map_segment_to_segment.contains_key(&edge){
                    map_segment_to_segment.insert(edge,(1,segments.len()));
                    segments.push(vec![edge]); //// not read but need to increase the length
                }
            }
                for map_segments in &segments{
                for x in map_segments.windows(2){
                    let a=x[0];
                    let b=x[1];
                    let node_a=if map_segment_to_segment[&a].0==1{
                        map.segments()[a].end_node
                    }else{
                        map.segments()[a].start_node
                    };
                    let node_b=if map_segment_to_segment[&b].0==1{
                        map.segments()[b].start_node
                    }else{
                        map.segments()[b].end_node
                    };
                    //dbg!(&map_segments);
            assert_eq!(node_a,node_b);
                }
                }
        }
for (_,&(_,segment)) in &map_segment_to_segment{
    assert!(segments[segment].len()>0);
}

    }
    (segments,map_segment_to_segment,nodes_eaten_up)
}

/*fn svg(lines:&[[(f64,f64);2]],bbox:(f64,f64,f64,f64))->String{
    let (_,(width,height))=stops_on_roads::coordinates::sf_from_bbox(bbox);
    format!(
    r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg xmlns="http://www.w3.org/2000/svg" width="{w}" height="{h}">
        <g stroke="#ccc" stroke-width="2" stroke-linecap="round">{roads}</g>
        <text x="0" y="0" text-anchor="end" font-size="8pt">
            Lines drawn from timetables (so may not show actual route)
        </text>
        </svg>"##,
        w=width,
        h=height,
        roads=lines.iter()
            .map(|[a,b]|format!(
                r##"<line data-info="{info}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
                info="",
                x1=a.0,
                y1=a.1,
                x2=b.0,
                y2=b.1
            ))
            .collect::<Vec<_>>()
            .join("")
    )
}*/




/*fn add_path_to_ordered_services(path:&[usize],service:&str,roads:&mut [Road],clockwise_roads:&[Vec<usize>]){
    let mut starting_index_of_next_path_positions=0;
    // how many path positions are there on each road
    let numbers_of_path_positions=path.iter()
        .map(|&road_index|(road_index,roads[road_index].ordered_services.len()+1))
        .collect::<Vec<(usize,usize)>>();

    let roads_and_positions_of_path_position_indices:Vec<(usize,usize)>=numbers_of_path_positions.iter()
        .flat_map(|&(road_index,number_of_positions)|(0..number_of_positions)
            .map(move|position|(road_index,position))
        )
        .collect();
    /*for (a,b) in &numbers_of_path_positions{
        dbg!(&roads[*a].name);
    }*/
    //dbg!(&numbers_of_path_positions);
    //dbg!(&roads_and_positions_of_path_position_indices);

    let mut path_positions_graph:petgraph::Graph<u8,u16,petgraph::Undirected,usize>
        =petgraph::Graph::from_edges
    (
        numbers_of_path_positions
            .windows(2) // array_windows
            .flat_map(|two|{
                let number_of_path_positions1=two[0].1;
                let number_of_path_positions2=two[1].1;
                let path_position_pairs=(0..number_of_path_positions1)
                    .flat_map(|path_position1|(0..number_of_path_positions2)
                        .map(move|path_position2|(
                            starting_index_of_next_path_positions+path_position1,
                            starting_index_of_next_path_positions+path_position2
                                +number_of_path_positions1
                        ))
                    )
                    .collect::<Vec<(usize,usize)>>();

                starting_index_of_next_path_positions+=number_of_path_positions1;

                path_position_pairs
            })
    );

    let fake_start_node=path_positions_graph.add_node(0);
    for to in 0..numbers_of_path_positions[0].1{
        path_positions_graph.add_edge(fake_start_node,to.into(),0);
    }
    let fake_end_node=path_positions_graph.add_node(0);
    for from in (path_positions_graph.node_count() // subtractions to ignore these fake nodes
        -numbers_of_path_positions.last().unwrap().1-2)..path_positions_graph.node_count()-2
    {
        path_positions_graph.add_edge(from.into(),fake_end_node,0);
    };
    //dbg!(path_positions_graph.node_count());
    //dbg!(path_positions_graph.raw_nodes());
    //dbg!(path_positions_graph.raw_edges());
    //println!("{}",petgraph::dot::Dot::with_config(&path_positions_graph,&[petgraph::dot::Config::NodeIndexLabel]));

    let path_positions_path:Vec<usize>=petgraph::algo::astar(
        &path_positions_graph,
        fake_start_node,
        |maybe_end|maybe_end==fake_end_node,
        |edge|{
            let (from,to)=path_positions_graph.edge_endpoints(edge.id()).unwrap();
            if from==fake_start_node||to==fake_end_node{
                0
            }else{
                let (
                    from_road_index,
                    position_in_from_road
                )=roads_and_positions_of_path_position_indices[from.index()];
                let (
                    to_road_index,
                    position_in_to_road
                )=roads_and_positions_of_path_position_indices[to.index()];
                let from_road=&roads[from_road_index];
                let to_road=&roads[to_road_index];
                //println!("{}->{}|||- -{}- - -> - -{}- -",from_road_index,to_road_index,position_in_from_road,position_in_to_road);

                assert!(clockwise_roads[from_road.start_node].contains(&to_road_index)||clockwise_roads[from_road.end_node].contains(&to_road_index));

                // relative to the orientation of this road?
                // are the positions on this road reversed from what would be expected
                // - travelling down it backwards
                // - the road goes downwards
                let from_reversed=clockwise_roads[from_road.start_node].contains(&to_road_index);
                let to_reversed=clockwise_roads[roads[to_road_index].end_node].contains(
                    &from_road_index
                );
                let from_positions_reversed=from_reversed^!from_road.is_upward();
                let to_positions_reversed=to_reversed^!to_road.is_upward();
                let reversal=from_positions_reversed^to_positions_reversed;

                let clockwise_roads_here:&[usize]=if from_reversed{
                    &clockwise_roads[from_road.start_node]
                }else{
                    &clockwise_roads[from_road.end_node]
                };

                let number_of_to_roads=clockwise_roads_here.len()-1; // ignoring this one
                // roads, clockwise, starting where this road is
                let to_roads:Vec<usize>=clockwise_roads_here.iter().cycle()
                    .skip_while(|&road|*road!=to_road_index)
                    .take(number_of_to_roads)
                    .map(|&road_index|road_index)
                    .collect();

                let to_index_in_to_roads=to_roads.iter()
                    .position(|maybe_to|*maybe_to==to_road_index)
                    .unwrap();

                let (before_roads,after_roads)=to_roads
                    .split_at(to_index_in_to_roads);
                let before_and_after_roads=[before_roads,after_roads];
                let mut before_and_after_services=before_and_after_roads.iter()
                    .map(|these_roads|these_roads.iter()
                        .map(|road_index|&roads[*road_index].ordered_services)
                        .flatten()
                    );

                let (before_in_middle,after_in_middle)=roads[
                    to_roads[to_index_in_to_roads]
                ]
                    .ordered_services
                    .split_at(position_in_to_road);
                let mut before_and_after_in_middle_road=if to_reversed{
                    [after_in_middle,before_in_middle]
                }else{
                    [before_in_middle,after_in_middle]
                }
                    .iter()
                    .copied()
                    .collect::<Vec<&[String]>>()
                    .into_iter();

                let (before,after)=( // all services before and after this one in to roads
                    before_and_after_services.next().into_iter() // None -> empty iterator
                        .flatten()
                        .chain(before_and_after_in_middle_road
                            .next()
                            .into_iter()
                            .flatten()
                        ),
                    before_and_after_services.next().into_iter().flatten()
                        .chain(before_and_after_in_middle_road.next().into_iter().flatten()),
                );
                
                // reverse these (XOR) if the positions are reversed between from and to

                // left - services in this road should be to the left of this service
                // in the from road
                let c=before
                    .filter_map(|code|from_road.ordered_services.iter()
                        .position(|code_in_from_road|code_in_from_road==code)
                        .filter(|&idx|reversal^(idx>=position_in_from_road)) // if it becomes None it is good
                        .map(|_|1)
                    )
                    .sum::<u16>()
                // right - services in this road should be to the right of this service
                // in the from road
                +after
                    .filter_map(|code|from_road.ordered_services.iter()
                        .position(|code_in_from_road|code_in_from_road==code)
                        .filter(|&idx|reversal^(idx<position_in_from_road)) // if it becomes None it is good
                        .map(|_|1)
                    )
                    .sum::<u16>();
                //println!("Cost: {}",c);
                c
            }
        },
        |_|0 // no useful heuristic -> just Dijkstra
    ).unwrap().1
        .into_iter()
        .map(|node_index|node_index.index())
        .collect();

    for position_index in path_positions_path{
        if position_index<roads_and_positions_of_path_position_indices.len(){ // not fake
            let (road_index,mut position)=roads_and_positions_of_path_position_indices[position_index];
            // is there a previous road?
            let backwards=(position_index>0).then(||roads_and_positions_of_path_position_indices.get(position_index-1)
                // does it mean that this road is being travelled down backwards?
                .filter(|(other_road_i,_)|clockwise_roads[roads[road_index].start_node].contains(other_road_i))
                // yes
                .map(|_|true)
            )
            .flatten()
                // there wasn't a previous road, but is there one afterwards?
                .unwrap_or_else(||roads_and_positions_of_path_position_indices.get(position_index+1)
                    .map(|(other_road_i,_)|clockwise_roads[roads[road_index].end_node].contains(other_road_i))
                    // there wasn't a road afterwards either; assume forwards; this probably will never happen
                    .unwrap_or(false)
                );

            if backwards{ // because positions are always from left to right,
                          // here it is not necessary to check the road's orientation
                position=roads[road_index].ordered_services.len()-position;
            }

            if position==roads[road_index].ordered_services.len(){
                roads[road_index].ordered_services.push(service.to_string());
            }else{
                roads[road_index].ordered_services.splice(
                    position..position,
                    std::iter::once(service.to_string())
                );
            }
        }
    }
}*/

/*#[derive(Clone,Debug)]
struct Road{
    name:String,
    points:Vec<Point>,
    length:u16,

    start_node:usize,
    end_node:usize,

    unordered_services:HashSet<String>,

    ordered_services:Vec<String>, // if the road goes upwards, left to right
}
impl Road{
    fn through_circle(&self,c:(f32,f32))->bool{
        self.points.windows(2)
            .any(|points|through_circle(points[0].pos(),points[1].pos(),c))
        /*self.points.iter()
            .any(|point|point.distance(c)<RADIUS)*/
    }
    fn svg(&self,pos:(f32,f32))->String{
        format!("{}{}",
            path(
                &self.points.iter()
                    //.filter_map(|point|point.not_stop())
                    .map(|point|point.pos())
                    .collect::<Vec<_>>(),
                Some(pos),
                &self.name,
                None
            ),
            self.points.iter()
                .filter_map(|point|if let Point::Stop(atco,_,xy)=point{ // use actual position
                    let (x,y)=relative(*xy,pos);
                    Some(format!(
                        r##"<circle stroke="none" fill="blue" cx="{x}" cy="{y}" r="10" data-atco="{atco}"/>"##,
                        x=x,
                        y=y,
                        atco=atco
                    ))
                }else{None})
                .collect::<Vec<_>>()
                .join("")
        )
    }
    fn lines/*_as_svg*/(&self/*,pos:(f32,f32),colours:&HashMap<String,String>*/)->HashMap<String,Vec<(f32,f32)>>{
        // middle as in the middle of the road
        let middle_points=self.points.iter()
            //.filter_map(|point|point.not_stop())
            .map(|point|point.pos())
            .collect::<Vec<_>>();
        /*assert!(self.points.len()>=2);
        assert!(middle_points.len()>=2);*/
        /*let without_1st_and_last_middle_points=middle_points.iter()
            .enumerate()
            .filter(|(i,_)|*i!=0&&*i!=middle_points.len()-1)
            // now have got rid of 1st and last
            .map(|(_,point)|point)
            .collect::<Vec<_>>();*/

        self.ordered_services.iter().enumerate()
            .map(|(i,code)|{
                let distance=(i as f32-(self.ordered_services.len()-1) as f32/2.0)*5.0;
                let without_1st_and_last=middle_points//without_1st_and_last_middle_points
                    .windows(3)
                    .map(|points|{
                        let angle=bisecting_angle(points[0],points[1],points[2]);
                        //dbg!(distance);
                        let horizontal_distance=angle.cos()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        let vertical_distance=angle.sin()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        (points[1].0+horizontal_distance,points[1].1+vertical_distance)
                    })
                    .collect::<Vec<_>>();

                let first={
                    let horizontal=middle_points[1].0-middle_points[0].0;
                    let vertical=middle_points[1].1-middle_points[0].1;

                    if without_1st_and_last.len()==0{
                        let g=vertical/horizontal;
                        let perpendicular_angle=if g.is_nan(){
                            0.0
                        }else{
                            (-1.0/g).atan()
                        };

                        let horizontal_distance=perpendicular_angle.cos()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        let vertical_distance=perpendicular_angle.sin()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        (middle_points[0].0+horizontal_distance,middle_points[0].1+vertical_distance)
                    }else{
                        (without_1st_and_last[0].0-horizontal,without_1st_and_last[0].1-vertical)
                    }
                };
                let last={
                    let penultimate=middle_points[middle_points.len()-2];
                    let last=middle_points.last().unwrap();

                    let horizontal=last.0-penultimate.0;
                    let vertical=last.1-penultimate.1;

                    let last=middle_points.last().unwrap();
                    if without_1st_and_last.len()==0{
                        let g=vertical/horizontal;
                        let perpendicular_angle:f32=if g.is_nan(){
                            0.0
                        }else{
                            (-1.0/g).atan()
                        };

                        let horizontal_distance=perpendicular_angle.cos()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        let vertical_distance=perpendicular_angle.sin()*(distance as f32)
                            *if self.is_upward(){1_f32}else{-1_f32};
                        // there are only two
                        (last.0+horizontal_distance,last.1+vertical_distance)
                    }else{
                        let last_not_middle=without_1st_and_last.last().unwrap();
                        (last_not_middle.0+horizontal,last_not_middle.1+vertical)
                    }
                };

                /*let first=relative(first,pos);
                let last=relative(last,pos);

                format!(
                    r##"{}<circle r="2" cx="{}" cy="{}" fill="turquoise" stroke="none"/><circle r="2" cx="{}" cy="{}" fill="green" stroke="none"/>"##,
                    without_1st_and_last.into_iter()
                        .map(|point|relative(point,pos))
                        .map(|(x,y)|format!(r##"<circle r="2" cx="{}" cy="{}" fill="{}" stroke="none" data-service-code="{}"/>"##,x,y,colours.get(code).map(|x|x.as_str()).unwrap_or("orange"),code))
                        .collect::<Vec<String>>()
                        .join(""),
                    first.0,first.1,
                    last.0,last.1
                )*/

                (
                    code.clone(),
                    std::iter::once(first)
                        .chain(without_1st_and_last)
                        .chain(std::iter::once(last))
                        .collect::<Vec<_>>()
                )
            })
            .collect()
            //.join("")
    }
    fn split(self,in_a:usize,mut stop:Point,biggest_nodeid:usize)->(Self,Self){
        let (a,b)=self.points.split_at(in_a+1);

        &a[0];
        &b[0];

        stop.stop_is_on_line(a.last().unwrap().pos(),b[0].pos());

        (
            Road{
                name:self.name.clone(),
                points:a.into_iter()
                    .cloned()
                    //.chain(std::iter::once(Point::Point(x,y)))
                    .chain(std::iter::once(stop.clone()))
                    .collect(),
                length:self.length/2, // doesn't matter because no other path to take

                start_node:self.start_node,
                end_node:biggest_nodeid+1,

                unordered_services:self.unordered_services.clone(),

                ordered_services:Vec::new(),
            },
            Road{
                name:self.name,
                points:std::iter::once(stop)
                    //.chain(std::iter::once(Point::Point(x,y)))
                    .chain(b.to_vec())/*if b.len()==1{
                        b.to_vec()
                    }else{
                        b.into_iter()
                            .cloned()
                            .skip(1)
                            .collect()
                    })*/.collect(),
                length:self.length/2,

                start_node:biggest_nodeid+1,
                end_node:self.end_node,

                unordered_services:self.unordered_services,

                ordered_services:Vec::new(),
            }
        )
    }
    /*fn points_not_stops(&self)->Vec<(f32,f32)>{
        self.points.iter().enumerate()
            .filter_map(|(i,point)|if i==0||i==self.points.len()-1{
                Some(point.pos())
            }else{
                point.not_stop()
            })
            .collect()
    }*/
    // for reversing the positions of lines
    // so that they will always be left to right
    // in the segments whose orientation agrees with
    // the rest of the road (for this agreement to happen,
    // the road's ends nearest each of the segment's ends
    // have to compare the same
    fn is_upward(&self)->bool{
        if self.points[0].pos().1==self.points.last().unwrap().pos().1{
            self.points[0].pos().0<self.points.last().unwrap().pos().0
        }else{
            self.points[0].pos().1<self.points.last().unwrap().pos().1
        }
    }
}
impl petgraph::IntoWeightedEdge<u16> for Road{
    type NodeId=usize;
    fn into_weighted_edge(self)->(Self::NodeId,Self::NodeId,u16){
        (self.start_node,self.end_node,self.length)
    }
}*/

/*fn relative((x,y):(f32,f32),(left,bottom):(f32,f32))->(f32,f32){
    (x-left,RADIUS*2 as f32-(y-bottom))
}*/

/*// gradient of the line passing through c and halfway between a and b
fn average_perpendicular_gradient(a:(f32,f32),c:(f32,f32),b:(f32,f32))->f32{
    /*-1.0/(
        ((a.1-c.1)/(a.0-c.0)+(c.1-b.1)/(c.0-b.0))/2.0
    )*/
    // halfway
    //let h=((a.0+b.0)/2.0,(a.1+b.1)/2.0);
    //let g=(h.1-c.1)/(h.0-c.0);

    let ac_g=(a.1-c.1)/(a.0-c.0);
    let bc_g=(b.1-c.1)/(b.0-c.0));

    let ac=if ac_g.is_nan(){90.0}else{ac.atan()};
    let bc=if bc_g.is_nan(){90.0}else{bc.atan()};

    let one_g=((ac+bc)/2.0).tan();

    let right_g=if a.0<c.0&&c.0<b.0{
        -1.0/g
    }else{g}*/

// angle between the x axis and bisector of ACB (radians)
fn bisecting_angle(a:(f32,f32),c:(f32,f32),b:(f32,f32))->f32{
    let ac_g=(a.1-c.1)/(a.0-c.0);
    let bc_g=(b.1-c.1)/(b.0-c.0);

    let ac=if ac_g.is_nan(){std::f32::consts::FRAC_PI_2}else{ac_g.atan()};
    /*if ac<0.0{
        ac=std::f32::consts::PI-ac;
    }*/
    let bc=if bc_g.is_nan(){std::f32::consts::FRAC_PI_2}else{bc_g.atan()};
    /*if bc<0.0{
        bc=std::f32::consts::PI-bc;
    }*/

    // there are two angle bisectors
    let one=(ac+bc)/2.0;
    //    return std::f32::consts::FRAC_PI_2+one;
    //one
    /*let one_g=((ac+bc)/2.0).tan();

    assert!(!one_g.is_nan());*/

    // if C is horzontally between A and B (probably)
    // use the other bisector
    //dbg!(a.0,c.0,b.0);
    if (a.0<=c.0&&c.0<=b.0)||(b.0<=c.0&&c.0<=a.0){
        std::f32::consts::FRAC_PI_2+one
    }else{one}
}

/*fn w_and_h(angle:f32,l:f32)->(f32,f32){
    (l*angle.cos(),l*angle.sin())
}

fn perpendicular(a:(f32,f32),c:(f32,f32),b:(f32,f32),i:usize,total:usize)->(f32,f32){
    let angle=bisecting_angle(a,c,b)+if a.1>b.1{std::f32::consts::PI}else{0.0};
    assert!(!angle.is_nan());
    let l=5.0;
    /*if g.is_infinite(){
        (c.0,c.1+(l*i as f32)/*-(total as f32)*(l as f32)/2.0*/)
    }else{
        let angle=g.atan();*/
        let (w,h)=w_and_h(angle,l);
        let (total_w,total_h)=w_and_h(angle,total as f32*l);
        let (offset_x,offset_y)=(total_w/2.0,total_h/2.0);
        (c.0+w*i as f32/*-offset_x*/,c.1+h*i as f32/*-offset_y*/)
    //}
}*/

/*fn path(points:&[(f32,f32)],pos:Option<(f32,f32)>,info:&str,stroke:Option<&str>)->String{
    format!(
        r##"<path d="M {d}" fill="none" {stroke}stroke-linecap="round" data-f="{info}"/>"##,
        d=points.iter()
            .map(|xy|pos.map(|pos|relative(*xy,pos)).unwrap_or(*xy))
            .map(|(x,y)|format!("{},{}",x,y))
            .collect::<Vec<_>>()
            .join(" "),
        stroke=stroke.map(|colour|format!(r##"stroke="{}" "##,colour)).unwrap_or_else(String::new),
        info=info
    )
}*/

// does the line segment from A to B pass through the circle with centre C
/*fn through_circle(a:(f32,f32),b:(f32,f32),c:(f32,f32))->bool{
    let on_line=Point::Point(c.0,c.1).closest_on_line(a,b);
    // is it on the line segment or on a continuation if it were a line
    let d=if (a.0<on_line.0&&on_line.0<b.0)||(b.0<on_line.0&&on_line.0<a.0){
        distance(on_line,c)
    }else{
        // C is closer to an end of the line segment
        // pick the closest
        distance(a,c).min(distance(b,c))
    };
    d<RADIUS // segment intersects circle
    //line_segment_to_point(a,b,c)<RADIUS // segment intersects circle

    /*
    // thinking about triangle ABC
    // compute its area * 2
    let area2=( (bx-ax)*(cy-ay) - (cx-ax)*(by-ay) ).abs();
    
    // length of AB -> the base, with Pythagoras
    let ab=( (bx-ax).powi(2) + (by-ay).powi(2) ).sqrt();

    // A=(1/2)bh => h=2a/b
    let h=area2/ab;*/

    // the line intersects the circle
    //h<RADIUS
}*/
/*fn line_segment_to_point((ax,ay):(f32,f32),(bx,by):(f32,f32),(cx,cy):(f32,f32))->f32{
    let u=( (cx-ax)*(bx-ax)+(cy-ay)*(by-ay) )/distance((ax,ay),(bx,by)).powi(2);

    if (0.0..1.0).contains(&u){
        // C is closer to line segment than A or B
        let x=ax+u*(bx-ax);
        let y=ay+u*(by-ay);
        distance((x,y),(cx,cy))
    }else{
        // c is closer to an end of the line segment
        // pick the closest
        distance((ax,ay),(cx,cy)).min(distance((bx,by),(cx,cy)))
    }
}


#[test]
fn circle_intersection(){
    assert!(!through_circle((395.0,549.0),(226.0,171.5),(-5.0,400.0))); // outside
    assert!(through_circle((395.0,549.0),(160.0,334.0),(-5.0,400.0))); // to in
    assert!(through_circle((395.0,549.0),(-243.0,93.0),(-5.0,400.0))); // through
    assert!(!through_circle((-200.0,118.0), (-243.0,93.0),  (-5.0,400.0))); // pointing in
    assert!( through_circle((21.0,313.0),   (72.0,527.0),   (-5.0,400.0))); // inside
}*/
/*#[test]
fn perpendicular_gradient(){
    assert_eq!(average_perpendicular_gradient((2.0,4.0),(6.0,4.0),(6.0,6.0)),-0.5)
}*/
/*#[test]
fn test_bisecting_angle(){
    let mut x=bisecting_angle((3.0,2.0),(1.0,2.0),(1.0,4.0));
    dbg!(x);
    assert!(nearly_equal(x,std::f32::consts::PI/4.0));
    x=bisecting_angle((-2.0,4.0),(2.0,6.0),(2.0,8.0));
    dbg!(x);
    assert!(nearly_equal(x,2.5880182947));
    // i don't know what this should do
    // - whether it should be the big correct angle or the small one
    /*x=bisecting_angle((-2.0,4.0),(2.0,6.0),(2.0,2.0));
    dbg!(x);
    assert!(nearly_equal(x,2.1243706857));*/
}

fn nearly_equal(a: f32, b: f32) -> bool {
        println!("a={} b={}",a,b);
	let abs_a = a.abs();
	let abs_b = b.abs();
	let diff = (a - b).abs();

	if a == b { // Handle infinities.
		true
	} else if a == 0.0 || b == 0.0 || diff < f32::MIN_POSITIVE {
		// One of a or b is zero (or both are extremely close to it,) use absolute error.
		diff < (f32::EPSILON * f32::MIN_POSITIVE)
	} else { // Use relative error.
		(diff / f32::min(abs_a + abs_b, f32::MAX)) < f32::EPSILON
	}
}

// not a coordinate, but a part of a line
#[derive(PartialEq,Clone,Debug)]
enum Point{
    Point(f32,f32),
    // the first position is useful; snapped to its road
    // the second position is the actual position of the stop
    Stop(String,(f32,f32),(f32,f32))

}
impl Point{
    fn new(s:&str)->Self{
        let mut xy=s.split(",");
        Self::Point(
            xy.next().unwrap().parse::<f32>().unwrap(),
            xy.next().unwrap().parse::<f32>().unwrap()
        )
    }
    fn pos(&self)->(f32,f32){
        match self{
            Self::Point(x,y)=>(*x,*y),
            Self::Stop(_,xy,_)=>*xy
        }
    }
    /*fn not_stop(&self)->Option<(f32,f32)>{
        if let Self::Point(x,y)=*self{
            Some((x,y))
        }else{
            None
        }
    }*/
    fn distance(&self,c:(f32,f32))->f32{
        distance(self.pos(),c)
    }
    fn stop_is_on_line(&mut self,(x1,y1):(f32,f32),(x2,y2):(f32,f32)){
        let xy=self.closest_on_line((x1,y1),(x2,y2));
        if let Self::Stop(_,(x,y),_)=self{
            *x=xy.0;
            *y=xy.1;
        }else{
            unreachable!();
        }
    }
    fn closest_on_line(&self,(x1,y1):(f32,f32),(x2,y2):(f32,f32))->(f32,f32){
        let g=(y2-y1)/(x2-x1);
        let c=y1-g*x1;
        let (x0,y0)=match self{ // if it's a stop, use its actual position
            Self::Point(x,y)=>(*x,*y),
            Self::Stop(_,_,xy)=>*xy
        };

        // https://www.desmos.com/calculator/elaect5g5a
        let y=(g*g*y0+g*x0+c)/(g*g+1.0);
        let x=x0-g*(y-y0);

        (x,y)
    }
    fn move_to(&mut self,(to_x,to_y):(f32,f32)){
        match self{
            Self::Point(x,y)=>{
                *x=to_x;
                *y=to_y;
            },
            Self::Stop(_,(x,y),_)=>{
                *x=to_x;
                *y=to_y;
            }
        }
    }

}

#[test]
fn closest_point_on_line(){
    let (x,y)=Point::Point(3.0,4.0).closest_on_line((1.0,4.0),(0.0,2.0));
    assert!(nearly_equal(4.8,y));
    assert!(nearly_equal(1.4,x));
}
#[test]
fn test_intersection(){
    assert_eq!(intersection(((8.0,2.0),(5.0,3.0)),((-2.0,2.0),(-4.0,1.0))),None);
    assert_eq!(intersection(((8.0,2.0),(-1.0,5.0)),((12.0,9.0),(-4.0,1.0))),Some((2.0,4.0)));
}

fn distance((ax,ay):(f32,f32),(bx,by):(f32,f32))->f32{
    ( (bx-ax).powi(2) + (by-ay).powi(2) ).sqrt()
}*/
fn intersection(((x0,y0),(x1,y1)):((f32,f32),(f32,f32)),((v0,w0),(v1,w1)):((f32,f32),(f32,f32)))->Option<(f32,f32)>{
    // https://www.desmos.com/calculator/muv0rcyjad
    let x=(
        (x1-x0)*(y0-w0)*(v1-v0)
       +v0*(w1-w0)*(x1-x0)
       -x0*(v1-v0)*(y1-y0)
    )/(
        (w1-w0)*(x1-x0)
       -(v1-v0)*(y1-y0)
    );
    let y=(
        (x-x0)*(y1-y0)
    )/(x1-x0)+y0;
    if (x0<x&&x<x1)||(x1<x&&x<x0){
        Some((x,y))
    }else{
        None
    }
}


fn colours(t:String)->HashMap<String,String>{
    t
        .lines()
        .skip(1)
        .map(|line|line.split(",").map(|x|x.to_lowercase()))
        .map(|mut fields|(fields.next().unwrap(),fields.next().unwrap()))
        .collect()
}
