use std::collections::{HashMap,HashSet,BTreeSet};
use petgraph::visit::EdgeRef;
pub struct Spider{
    map:stops_on_roads::Map,
    segments:Vec<Vec<usize>>,
    map_segment_to_segment:HashMap<usize,(i8,usize)>,
    groups:Vec<((BTreeSet<usize>,BTreeSet<usize>),HashSet<String>)>,
    unordered_groups_on_segment:HashMap<usize,Vec<usize>>,
    clockwise_segments:HashMap<usize,Vec<usize>>,
    colours:HashMap<String,String>
}
impl Spider{
    pub fn new(map:stops_on_roads::Map)->Self{
        let graph=map.graph();
        let (segments,map_segment_to_segment):(Vec<Vec<usize>>,HashMap<usize,(i8,usize)>)=collapse_consecutive_segments(&map,&graph);
        let mut junctions_with_lines:HashSet<usize>=HashSet::new();
        let mut groups:Vec<((BTreeSet<usize>,BTreeSet<usize>),HashSet<String>)>=Vec::new();
        let mut unordered_groups_on_segment:HashMap<usize,Vec<usize>>=HashMap::new();
        for (segment_i,map_segment_idxs) in segments.iter().enumerate()
            .filter(|(_,idxs)|idxs.len()>0)
        {
            let mut groups_of_this_segment:HashMap<(BTreeSet<usize>,BTreeSet<usize>),HashSet<String>>=HashMap::new();
            for code in &map.segments()[map_segment_idxs[0]].services{
                let from:BTreeSet<usize>=graph.edges(map.segments()[map_segment_idxs[0]].start_node.into())
                    .map(|edge_reference|edge_reference.id().index())
                    .filter(|id|*id!=map_segment_idxs[0]
                        &&map.segments()[*id].services.contains(code)
                    )
                    .collect();
                let to:BTreeSet<usize>=graph.edges(map.segments()[*map_segment_idxs.last().unwrap()].end_node.into())
                    .map(|edge_reference|edge_reference.id().index())
                    .filter(|id|id!=map_segment_idxs.last().unwrap()
                        &&map.segments()[*id].services.contains(code)
                    )
                    .collect();
                groups_of_this_segment.entry((from,to))
                    .or_default()
                    .insert(code.to_string());
            }
            if groups_of_this_segment.len()>0{
                junctions_with_lines.insert(map.segments()[map_segment_idxs[0]].start_node);
                junctions_with_lines.insert(map.segments()[*map_segment_idxs.last().unwrap()].end_node);
            }
            for (route,codes) in groups_of_this_segment{
                groups.push((route,codes));
                unordered_groups_on_segment.entry(segment_i)
                    .or_default()
                    .push(groups.len()-1);
            }
        }
        // could be a method of stops_on_roads::Map, but then the graph would have to be created again
        let clockwise_segments:HashMap<usize,Vec<usize>>=junctions_with_lines.iter()
            .map(|&node|{
                let mut bearings=graph.edges(node.into())
                    .map(|edge|(
                        edge.id().index(),
                        map.segments()[edge.id().index()].angle_at_node(node) // increasing as next goes anticlockwise so need to reverse
                    ))
                    .collect::<Vec<_>>();
                bearings.sort_unstable_by(|(_,bearing1),(_,bearing2)|bearing2 // reverse
                    .partial_cmp(bearing1)
                    .unwrap()
                );
                (
                    node,
                    bearings.into_iter()
                        .map(|(map_segment_index,_)|map_segment_index) // don't need the bearings, just the order
                        .map(|map_segment_index|map_segment_to_segment[&map_segment_index].1) // segments not map segments
                        .collect::<Vec<usize>>()
                )
            })
            .collect();
        Self{
            map,
            segments,
            map_segment_to_segment,
            groups,
            unordered_groups_on_segment,
            clockwise_segments,
            colours:colours(std::fs::read_to_string("../bus/data/service_colours.csv").unwrap())
        }
    }

    pub fn at_bbox(&self,bbox:stops_on_roads::coordinates::BBox)->String{
        let unordered_groups_on_visible_segment=self.unordered_groups_on_segment.iter()
            .filter(|(segment,_)|self.segments[**segment].iter()
                .any(|idx|self.map.segments()[*idx].project(bbox).is_some())
            )
            .map(|(segment,groups)|(*segment,groups.clone()))
            .collect::<HashMap<usize,Vec<usize>>>();
        let visible_service_codes=unordered_groups_on_visible_segment.values()
            .flatten()
            .flat_map(|group|&self.groups[*group].1)
            .map(|x|x.as_str())
            .collect::<HashSet<&str>>();

        // create groups

        let cheapest_orderings:HashMap<usize,Vec<usize>>=cartesian_product(&unordered_groups_on_visible_segment.into_iter()
            .map(|(segment,unordered_groups)|(segment,permutations(unordered_groups.clone())))
            .collect::<Vec<(usize,Vec<Vec<usize>>)>>()
        ).into_iter()
            .map(|ordering_on_each_segment|{
                //dbg!(&ordering_on_each_segment);
                let cost=self.clockwise_segments.iter()
                    .map(|(node,segments_around_node)|if segments_around_node.len()>1{ // not end of line
                        let clockwise_groups:Vec<Vec<usize>>=segments_around_node.iter()
                            .filter_map(|&segment_i|if self.map.segments()[segment_i].start_node==*node{
                                // ignore segments without lines - they are not in this map
                                ordering_on_each_segment.get(&segment_i).cloned()
                            }else{
                                ordering_on_each_segment.get(&segment_i).cloned()
                                    .map(|mut r|{r.reverse();r})
                            })
                            .collect();
                        //dbg!(segments_around_node.len(),clockwise_groups.len());

                        // from,to,group id
                        let mut seen:HashSet<(usize,usize,usize)>=HashSet::new();
                        clockwise_groups.iter().enumerate()
                            // junction_entry all ignoring roads without lines
                            .map(|(junction_entry,groups)|{
                                //dbg!(groups.len());
                                groups.iter().enumerate()
                                    .map(|(group_i_in_segment,group)|{
                                        let before=&groups[..group_i_in_segment];
                                        let after=&groups[group_i_in_segment+1..];

                                        clockwise_groups.iter().enumerate()
                                            .filter(|(to_junction_entry,_)|*to_junction_entry!=junction_entry)
                                            .filter_map(|(to_junction_entry,groups)|groups.iter()
                                                .position(|to_group|to_group==group)
                                                .map(|to_group_position|(to_junction_entry,to_group_position))
                                            )
                                            .map(|(to_junction_entry,to_group_position)|if seen.insert((junction_entry,to_junction_entry,*group)){
                                                clockwise_groups.iter().cycle()
                                                    .skip(junction_entry+1)
                                                    .take({
                                                        let number_to_take_maybe_negative:i8=(to_junction_entry as i8)-(junction_entry as i8);
                                                        //dbg!(number_to_take_maybe_negative);
                                                        if number_to_take_maybe_negative<0{ // to is before from in this circle
                                                            clockwise_groups.len()-1-TryInto::<usize>::try_into(number_to_take_maybe_negative.abs()).unwrap()
                                                        }else{
                                                            usize::try_from(number_to_take_maybe_negative).unwrap()-1
                                                        }
                                                    })
                                                    .flatten()
                                                    .chain(clockwise_groups[to_junction_entry].iter()
                                                        .take(to_group_position)
                                                    )
                                                    .filter(|to_group|after.contains(to_group))
                                                    .count()
                                                +clockwise_groups.iter().cycle()
                                                    .skip(to_junction_entry+1)
                                                    .take({
                                                        let number_to_take_maybe_negative:i8=(junction_entry as i8)-(to_junction_entry as i8);
                                                        //dbg!(number_to_take_maybe_negative);
                                                        if number_to_take_maybe_negative<0{ // from is before to in this circle
                                                            clockwise_groups.len()-1-TryInto::<usize>::try_into(number_to_take_maybe_negative.abs()).unwrap()
                                                        }else{
                                                            usize::try_from(number_to_take_maybe_negative).unwrap()-1
                                                        }
                                                    })
                                                    .flatten()
                                                    .chain(clockwise_groups[to_junction_entry].iter()
                                                        .skip(to_group_position+1)
                                                    )
                                                    .filter(|to_group|before.contains(to_group))
                                                    .count()
                                            }else{0})
                                            .sum::<usize>()
                                    })
                                    .sum::<usize>()
                            })
                            .sum::<usize>()
                    }else{0})
                    .sum::<usize>();
                    (ordering_on_each_segment,cost)
            })
            .min_by_key(|(_,cost)|*cost)
            .map(|(x,_)|x)
            .unwrap_or_default();

        let mut lines=vec![];
        let mut roads=vec![];
        for (segment_i,[a,b]) in self.map.project(bbox){
            let width_if_services=self.map.segments()[segment_i].services.len()*5;
            roads.push(format!(
                r##"<line stroke-width="{width}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
                width=if width_if_services==0{
                    2
                }else{
                    width_if_services
                },
                x1=a.0,y1=a.1,
                x2=b.0,y2=b.1
            ));
            // if there are groups/lines on this segment
            if let Some(this_segment_groups)=cheapest_orderings.get(&self.map_segment_to_segment[&segment_i].1){
                let codes:Vec<&str>=this_segment_groups.iter()
                    .map(|&group_idx|&self.groups[group_idx].1)
                    .flatten()
                    .map(|x|x.as_str())
                    .collect();
                let (a,b)=if self.map_segment_to_segment[&segment_i].0==1{
                    (a,b)
                }else{
                    (b,a)
                };
                for (code,a,b) in offsets_from_segment(a,b,&codes){
                    lines.push(format!(
                        r##"<line data-info="{code}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
                        code=code,
                        x1=a.0,y1=a.1,
                        x2=b.0,y2=b.1
                    ));
                }
            }
        }

        format!(
        r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="{w}" height="{h}">
            <style>{colours}</style>
            <rect fill="#fdfdda" x="0" y="0" width="{w}" height="{h}"/>
            <g stroke="white" stroke-linecap="round">{roads}</g>
            <g stroke-linecap="round" stroke-width="4">{lines}</g>
            <text x="{text_x}" y="{text_y}" text-anchor="end" font-size="8pt">
                Lines drawn from timetables (so may not show actual route)
            </text>
            </svg>"##,
            w=bbox.width(),
            h=bbox.height(),
            colours=visible_service_codes.iter()
                .map(|code|format!(
                    r##"[data-info="{code}"]{{stroke:{colour};}}"##,
                    code=code,
                    colour=self.colours.get(&code.to_lowercase()).map(|x|x.as_str()).unwrap_or("#ccc")
                ))
                .collect::<Vec<_>>()
                .join(""),
            text_x=bbox.width()-10.,
            text_y=bbox.height()-10.,
            roads=roads.join(""),
            lines=lines.join(""),
        )
    }
    pub fn map(&self)->&stops_on_roads::Map{
        &self.map
    }
}
fn permutations<T:Clone>(mut of:Vec<T>)->Vec<Vec<T>>{
    fn generate<T:Clone>(n:usize,a:&mut Vec<T>)->Vec<Vec<T>>{
        if n == 1 {
            vec![a.to_vec()]
        }else{
            let mut r=vec![];
            for i in  0..n-1{
                r.append(&mut generate(n - 1, a));
                if n%2==0{
                    a.swap(i,n-1);
                }
                else {
                    a.swap(0,n-1);
                }
            }
            r.append(&mut generate(n-1,a));
            return r;
        }
    }
    return generate(of.len(),&mut of);
}

// maybe?
fn cartesian_product(of:&[(usize,Vec<Vec<usize>>)])->Vec<HashMap<usize,Vec<usize>>>{
    if of.len()==0{ // only when called from elsewhere;
                    // shouldn't happen when recursing
        vec![]
    }else if of.len()>1{
        let after=cartesian_product(&of[1..]);
        of[0].1.iter()
            .flat_map(|ordering|{
                after.iter()
                    .cloned()
                    .map(move|segments|{
                        let mut r=[(of[0].0,ordering.clone())].into_iter().collect::<HashMap<usize,Vec<usize>>>();
                        r.extend(segments);
                        r
                    })
            })
            .collect()
    }else{
        //errorindex out of bounds: the len is 0 but the index is 0
        of[0].1.iter()
            .cloned()
            .map(|ordering|[(of[0].0,ordering)].into_iter().collect())
            .collect()
    }
}

fn direction(a:(f64,f64),b:(f64,f64))->i8{
    // upwards or the left
    if a.1<b.1||(a.1==b.1 && a.0>b.0){
        1
    }else{ // downwards or the right
        -1
    }
}

fn offsets_from_segment<T:Copy>(a:(f64,f64),b:(f64,f64),codes:&[T])->Vec<(T,(f64,f64),(f64,f64))>{
    // https://www.desmos.com/calculator/wecddwzzxy
    // that is a version of this code
    let m=if a.1==b.1{
        9999.
    }else if a.0==b.0{
        0.
    }else{
        -1./((b.1-a.1)/(b.0-a.0))
    };

    // upwards or the left
    let reverse=if a.1<b.1||(a.1==b.1 && a.0>b.0){
        1.
    }else{ // downwards or the right
        -1.
    };

    let ac=a.1-m*a.0;
    let bc=b.1-m*b.0;

        /*if a.0==b.0{
        a.0
    }else{
        (b.1-a.1)/(b.0-a.0)
    };*/
    let sqrt_one_plus_m_squared=(1.+m.powi(2)).sqrt();
    let n_minus_one_divided_by_two=((codes.len() as f64)-1.)/2.;

    let width=5;

    codes.iter().copied().enumerate().map(|(i,code)|{
        let d=width as f64*(i as f64-n_minus_one_divided_by_two);
        let dx=(d as f64/sqrt_one_plus_m_squared)*reverse;
        let ax=a.0+dx;
        let ay=m*ax+ac;
        let bx=b.0+dx;
        let by=m*bx+bc;
        (code,(ax,ay),(bx,by))
    }).collect()
}

// don't need to take graph as an argument as there is a method of map, but this takes less work
fn collapse_consecutive_segments(map:&stops_on_roads::Map,graph:&petgraph::Graph<u8,f64,petgraph::Undirected,usize>)->(Vec<Vec<usize>>,HashMap<usize,(i8,usize)>){
    let mut map_segment_to_segment:HashMap<usize,(i8,usize)>=HashMap::new();
    // may contain too many segments; the opposite hashmap is correct
    let mut segments:Vec<Vec<usize>>=Vec::new();


    let mut x=0;

    for node in graph.node_indices(){
        let edges:Vec<usize>=graph.edges(node)
            .map(|edge|edge.id().index())
            .collect();
        let edges_with_lines:Vec<usize>=edges.iter()
            .copied()
            .filter(|&edge|map.segments()[edge].services.len()>0)
            .collect();
        let edges_without_lines:Vec<usize>=edges.iter()
            .copied()
            .filter(|&edge|map.segments()[edge].services.len()==0)
            .collect();

        if edges_with_lines.len()==2 // could be more permissive? crossroads etc.
            && map.segments()[edges_with_lines[0]].services.len()>0
            && map.segments()[edges_with_lines[1]].services.len()>0
            && map.segments()[edges_with_lines[0]].services==map.segments()[edges_with_lines[1]].services
        {
            if edges_with_lines.iter().all(|edge|!map_segment_to_segment.contains_key(edge)){
                let reverse_a=if map.segments()[edges_with_lines[0]].end_node==node.index(){1}else{-1};
                let reverse_b=if map.segments()[edges_with_lines[1]].start_node==node.index(){1}else{-1};

                map_segment_to_segment.insert(edges_with_lines[0],(reverse_a,segments.len()));
                map_segment_to_segment.insert(edges_with_lines[1],(reverse_b,segments.len()));

                segments.push(vec![
                    edges_with_lines[0],
                    edges_with_lines[1]
                ]);
            }else{
                let (existing_segment_idx,map_segments_to_add)=if edges_with_lines.iter().all(|edge|map_segment_to_segment.contains_key(edge)){
                    let segment_a=map_segment_to_segment[&edges_with_lines[0]].1;
                    let map_segments_to_add=segments[map_segment_to_segment[&edges_with_lines[1]].1].split_off(0);

                    (segment_a,map_segments_to_add) // remove map segments from segment b
                }else if edges_with_lines.iter().any(|edge|map_segment_to_segment.contains_key(edge)){
                    let (already,other)=if map_segment_to_segment.contains_key(&edges_with_lines[0]){
                        (edges_with_lines[0],edges_with_lines[1])
                    }else{
                        (edges_with_lines[1],edges_with_lines[0])
                    };
                    let segment_idx=map_segment_to_segment[&already].1;

assert!(segments[segment_idx].len()>0);

                    (segment_idx,vec![other])
                }else{
                    unreachable!()
                };

                let (a_start,a_end)=(
                    if map_segment_to_segment[&segments[existing_segment_idx][0]].0==1{
                        map.segments()[segments[existing_segment_idx][0]].start_node
                    }else{
                        map.segments()[segments[existing_segment_idx][0]].end_node
                    },
                    if map_segment_to_segment[segments[existing_segment_idx].last().unwrap()].0==1{
                        map.segments()[*segments[existing_segment_idx].last().unwrap()].end_node
                    }else{
                        map.segments()[*segments[existing_segment_idx].last().unwrap()].start_node
                    }
                );





                x+=2;





                let (b_start,b_end)=(
                    if let Some((-1,_))=map_segment_to_segment.get(&map_segments_to_add[0]){
                        map.segments()[map_segments_to_add[0]].end_node
                    }else{
                        map.segments()[map_segments_to_add[0]].start_node
                    },
                    if let Some((-1,_))=map_segment_to_segment.get(map_segments_to_add.last().unwrap()){
                        map.segments()[*map_segments_to_add.last().unwrap()].start_node
                    }else{
                        map.segments()[*map_segments_to_add.last().unwrap()].end_node
                    }
                );

                let reverse_b=if a_end==b_start{
                    segments[existing_segment_idx].append(&mut map_segments_to_add.clone());
                    1
                }else if a_end==b_end{
                    let mut reversed=map_segments_to_add.clone();
                    reversed.reverse();
                    segments[existing_segment_idx].append(&mut reversed);
                    -1
                }else if a_start==b_end{
                    segments[existing_segment_idx].splice(0..0,map_segments_to_add.clone());
                    1
                }else if a_start==b_start{
                    let mut reversed=map_segments_to_add.clone();
                    reversed.reverse();
                    segments[existing_segment_idx].splice(0..0,reversed);
                    -1
                }else{
                    unreachable!();
                };

                for &map_segment_idx in &map_segments_to_add{



                    x+=1;




                    map_segment_to_segment.insert(
                        map_segment_idx,
                        (
                            map_segment_to_segment.get(&map_segment_idx)
                                .map(|(r,_)|*r)
                                .unwrap_or(1)*reverse_b,
                            existing_segment_idx
                        )
                    );
                }
            }
            for &edge in &edges_without_lines{
                if !map_segment_to_segment.contains_key(&edge){
                    map_segment_to_segment.insert(edge,(1,segments.len()));
                    segments.push(vec![edge]);
                }
            }
        }else{
            // in the other if branch the junction is subsumed
            for &edge in &edges{
                if !map_segment_to_segment.contains_key(&edge){
                    map_segment_to_segment.insert(edge,(1,segments.len()));
                    segments.push(vec![edge]);
                }
            }
        }

for map_segments in &segments{
for x in map_segments.windows(2){
let a=x[0];
let b=x[1];
let node_a=if map_segment_to_segment[&a].0==1{
    map.segments()[a].end_node
}else{
    map.segments()[a].start_node
};
let node_b=if map_segment_to_segment[&b].0==1{
    map.segments()[b].start_node
}else{
    map.segments()[b].end_node
};
assert_eq!(node_a,node_b);
}
}
for (_,&(_,segment)) in &map_segment_to_segment{
    assert!(segments[segment].len()>0);
}

    }
    (segments,map_segment_to_segment)
}
fn colours(t:String)->HashMap<String,String>{
    t
        .lines()
        .skip(1)
        .map(|line|line.split(",").map(|x|x.to_lowercase()))
        .map(|mut fields|(fields.next().unwrap(),fields.next().unwrap()))
        .collect()
}
