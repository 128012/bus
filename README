In the bus directory:
- To build this run `cargo build`.
  That creates the binary called target/release/bus.
- Building and running this can be done in one step with `cargo run`.
- The first time it is run it will crash if data/downloaded doesn't 
  exist or is empty, when it tries to read the timetables that should be 
  saved there. To avoid this run `target/release/bus download` the 
  first time.

File structure

/bus				The website
  /data
    /downloaded
      /naptan.csv		Stops; Stops.csv in
				http://naptan.app.dft.gov.uk/DataRequest/Naptan.ashx?format=csv&LA=340
      /gtfs			Timetables
				https://data.bus-data.dft.gov.uk/timetable/download/gtfs-file/south_east/
    /service-colours.csv
    /groups.csv			This shows which stops are opposite each 
				other. Some are guessed by comparing the 
				names and locations but the rest are 
				specified in here
  /update.sh			A script which downloads the contents 
  				of bus/data/downloaded
  /asset
  /pages			Pages in the site and templates for 
				creating them.
/txc				A TransXChange parser
/map				The SVG map. The file in here is an
				Inkscape file. The build script is supposed 
				to convert these to normal SVG and PNG, and 
				save them into /bus/asset.
/cert				Certificate and key in pkcs8. Use the 
/key				environmental variables BUS_CERT and 
				BUS_KEY to choose a different path. 
				With these, the ports will be 80 and 443.
				Without, it falls back to http and 
				respects the specified port.

Options

When compiled the binary will be bus/target/release/bus. It has to be run 
in the directory where the asset and data directories are, or in this 
directory.

bus allow-update-request	Allow a GET request to /update to make it 
				download and read new data.
    download			Download data before starting.
    8000			Listen on port 8000.
