pub fn new(l:roxmltree::Node)->Result<Vec<txc_bank::BankHoliday>,&'static str>{
    if let roxmltree::NodeType::Element=l.node_type(){
        Ok(match l.tag_name().name(){
            "AllBankHolidays"=>vec![
                txc_bank::BankHoliday::ChristmasDay,
                txc_bank::BankHoliday::BoxingDay,
                
                txc_bank::BankHoliday::NewYearsDay,

                txc_bank::BankHoliday::GoodFriday,
                txc_bank::BankHoliday::EasterMonday,

                txc_bank::BankHoliday::LateSummer, //NotScotland
                txc_bank::BankHoliday::MayDay,
                txc_bank::BankHoliday::Spring, // SpringBank
            
                // Displacement Holidays
                txc_bank::BankHoliday::ChristmasDaySubstitute,
                txc_bank::BankHoliday::BoxingDaySubstitute,
                txc_bank::BankHoliday::NewYearsDaySubstitute
            ],
            "AllHolidaysExceptChristmas"=>vec![
                txc_bank::BankHoliday::NewYearsDay,

                txc_bank::BankHoliday::GoodFriday,
                txc_bank::BankHoliday::EasterMonday,

                txc_bank::BankHoliday::LateSummer, //NotScotland
                txc_bank::BankHoliday::MayDay,
                txc_bank::BankHoliday::Spring, // SpringBank
            
                // Displacement Holidays
                txc_bank::BankHoliday::ChristmasDaySubstitute,
                txc_bank::BankHoliday::BoxingDaySubstitute,
                txc_bank::BankHoliday::NewYearsDaySubstitute
            ],
            "Holidays"=>vec![txc_bank::BankHoliday::NewYearsDay,txc_bank::BankHoliday::GoodFriday],
            "NewYearsDay"=>vec![txc_bank::BankHoliday::NewYearsDay],
            "GoodFriday"=>vec![txc_bank::BankHoliday::GoodFriday],
            "HolidayMondays"=>vec![
                txc_bank::BankHoliday::EasterMonday,
                txc_bank::BankHoliday::MayDay,
                txc_bank::BankHoliday::Spring,
                txc_bank::BankHoliday::LateSummer
            ],
            "EasterMonday"=>vec![txc_bank::BankHoliday::EasterMonday],
            "MayDay"=>vec![txc_bank::BankHoliday::MayDay],
            "SpringBank"=>vec![txc_bank::BankHoliday::Spring],
            "LateSummerBankHolidayNotScotland"=>vec![txc_bank::BankHoliday::LateSummer],
            "ChristmasEve"=>vec![txc_bank::BankHoliday::ChristmasEve],
            "Christmas"=>vec![txc_bank::BankHoliday::ChristmasDay,txc_bank::BankHoliday::BoxingDay],
            "ChristmasDay"=>vec![txc_bank::BankHoliday::ChristmasDay],
            "BoxingDay"=>vec![txc_bank::BankHoliday::BoxingDay],
            "NewYearsEve"=>vec![txc_bank::BankHoliday::NewYearsEve],
            "DisplacementHolidays"=>vec![
                txc_bank::BankHoliday::ChristmasDaySubstitute,
                txc_bank::BankHoliday::BoxingDaySubstitute,
                txc_bank::BankHoliday::NewYearsDaySubstitute
            ],
            "ChristmasDayHoliday"=>vec![txc_bank::BankHoliday::ChristmasDaySubstitute],
            "BoxingDayHoliday"=>vec![txc_bank::BankHoliday::BoxingDaySubstitute],
            "NewYearsDayHoliday"=>vec![txc_bank::BankHoliday::NewYearsDaySubstitute],
            "EarlyRunOffDays"=>vec![txc_bank::BankHoliday::ChristmasEve,txc_bank::BankHoliday::NewYearsEve],

            "DateRange"=>vec![txc_bank::BankHoliday::Range(
                chrono::naive::NaiveDate::parse_from_str(l.children()
                    .find(|x|x.tag_name().name()=="StartDate")
                    .ok_or("No StartDate in DateRange")?
                    .text()
                    .ok_or("No text in StartDate in DateRange")?,
                    "%Y-%m-%d"
                ).map_err(|_|"Invalid date in StartDate in DateRange")?,
                chrono::naive::NaiveDate::parse_from_str(l.children()
                    .find(|x|x.tag_name().name()=="EndDate")
                    .ok_or("No EndDate in DateRange")?
                    .text()
                    .ok_or("No text in EndDate in DateRange")?,
                    "%Y-%m-%d"
                ).map_err(|_|"Invalid date in EndDate in DateRange")?
            )],
            "OtherPublicHoliday"=>vec![txc_bank::BankHoliday::Other(
                chrono::naive::NaiveDate::parse_from_str(l.children()
                    .find(|x|x.tag_name().name()=="Date")
                    .ok_or("No Date in OtherPublicHoliday")?
                    .text()
                    .ok_or("No text in Date in OtherPublicHoliday")?,
                    "%Y-%m-%d"
                ).map_err(|_|"Invalid date in Date in OtherPublicHoliday")?,
            )],

            "WorkingDays"=>vec![], // ServicedOrganisation
            
            _=>Err("Invalid txc_bank holiday name")?
        })
    }else{
        Err("Bank holiday wasn't an element")
    }
}
