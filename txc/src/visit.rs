#[derive(Clone)]
pub(crate) struct IncompleteStopUsage<'a>{
    atco:Option<&'a str>,
    activity:Option<Activity>,
    wait:Option<chrono::Duration>,
    timing_status:Option<TimingStatus>,
}
impl<'a> IncompleteStopUsage<'a>{
    pub(crate) fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        if element.tag_name().name()=="To"||element.tag_name().name()=="From"{
            Ok(Self{
                atco:element.children()
                    .find(|x|x.tag_name().name()=="StopPointRef")
                    .and_then(|x|x.text()),
                activity:element.children()
                    .find(|x|x.tag_name().name()=="Activity")
                    .map(|l|Activity::new(Some(l)))
                    .transpose()?,
                wait:element.children()
                    .find(|x|x.tag_name().name()=="WaitTime")
                    .and_then(|l|l.text())
                    .map(parse_pt)
                    .transpose()?,
                timing_status:element.children()
                    .find(|x|x.tag_name().name()=="TimingStatus")
                    .map(|l|TimingStatus::new(Some(l)))
                    .transpose()?,
            })
        }else{
            Err("Not a StopUsage element")?
        }
    }
    pub(crate) fn also(self,complete:Visit<'a>)->Visit<'a>{
        Visit{
            atco:self.atco.unwrap_or(complete.atco),
            arrival:chrono::naive::NaiveTime::MIN, // == complete.arrival
            wait:self.wait.or(complete.wait),
            activity:self.activity.unwrap_or(complete.activity),
            timing_status:self.timing_status.unwrap_or(complete.timing_status),
        }
    }
}

pub fn parse_pt(text:&str)->Result<chrono::Duration,&'static str>{
    if text.chars().take(2).collect::<String>().as_str()=="PT"{
        let text=text.chars().skip(2);
        let (mut h,mut m,mut s)=(String::from("0"),String::from("0"),String::from("0"));

        let mut state=String::new();
        text.map(|c|Ok(
            if c.is_ascii_digit(){
                state.push(c);
            }else{
                match c.to_ascii_uppercase(){
                    'H'=>h+=&state,
                    'M'=>m+=&state,
                    'S'=>s+=&state,
                    _=>Err("char which wasn't H/M/S")?
                };
                state.clear();
            }
        ))
            .collect::<Result<Vec<_>,&'static str>>()?;

        Ok(chrono::Duration::hours(h.parse::<i64>()
            .map_err(|_|"couldn't parse h as int")?
        )+chrono::Duration::minutes(m.parse::<i64>()
            .map_err(|_|"couldn't parse m as int")?,
        )+chrono::Duration::seconds(s.parse::<i64>()
            .map_err(|_|"couldn't parse s as int")?
        ))
    }else{
        Err("Duration didn't start with `PT`")
    }
}

#[derive(Clone,Debug)]
pub enum Activity{
    PickUp,
    SetDown,
    PickUpAndSetDown,
    HailAndRideStart,
    HailAndRideEnd,
    Pass
}
impl Activity{
    fn new(l:Option<roxmltree::Node>)->Result<Self,&'static str>{
            if let Some(l)=l{
                    if l.tag_name().name()=="Activity"{
                    Ok(match l.text()
                        .ok_or("Couldn't read Activity")?
                        .to_ascii_uppercase().as_ref(){
                        "PICKUP"=>Self::PickUp,
                        "SETDOWN"=>Self::SetDown,
                        "PICKUPANDSETDOWN"=>Self::PickUpAndSetDown,
                        "HAILANDRIDESTART"=>Self::HailAndRideStart,
                        "HAILANDRIDEEND"=>Self::HailAndRideEnd,
                        "PASS"=>Self::Pass,
                        _=>Err("Invalid Activity")?
                    })
                }else{
                    Err("Not an Activity element")
                }
            }else{
                    Ok(Self::PickUpAndSetDown)
            }
    }
}

#[derive(Copy,Clone,Debug,Ord,PartialOrd,Eq,PartialEq)]
pub enum TimingStatus{
    PTP, // principal+time info point
    TIP, // time information point
    OTH  // other stop
}
impl TimingStatus{
    pub fn new(l:Option<roxmltree::Node>)->Result<Self,&'static str>{
        Ok(if let Some(l)=l{
                if l.tag_name().name()=="TimingStatus"{
                    match l.text()
                        .ok_or("Couldn't get timing status value")?
                        .to_ascii_uppercase().as_ref()
                    {
                        "PTP"=>Self::PTP,
                        "TIP"=>Self::TIP,
                        "OTH"=>Self::OTH,
                        "OTHERPOINT"=>Self::OTH,
                        "PRINCIPALTIMINGPOINT"=>Self::PTP,
                        "TIMEINFOPOINT"=>Self::TIP,
                        _=>Err("Invalid TimingStatus")?
                    }
                }else{
                    Err("Not a TimingStatus element")?
                }
        }else{
                Self::OTH
        })
    }
    pub fn acronym(&self)->&'static str{
        match self{
            Self::PTP=>"PTP",
            Self::TIP=>"TIP",
            Self::OTH=>"OTH"
        }
    }
}

// StopUsage with absolute time
#[derive(Debug,Clone)]
pub struct Visit<'a>{
    pub atco:&'a str,
    pub arrival:chrono::naive::NaiveTime,
    pub wait:Option<chrono::Duration>,
    pub activity:Activity,
    pub timing_status:TimingStatus
}
impl<'a> Visit<'a>{
    pub(crate) fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        if element.tag_name().name()=="To"||element.tag_name().name()=="From"{
            Ok(Self{
                atco:element.children()
                    .find(|x|x.tag_name().name()=="StopPointRef")
                    .and_then(|x|x.text())
                    .ok_or("No StopPointRef")?,
                arrival:chrono::naive::NaiveTime::MIN,
                wait:element.children()
                    .find(|x|x.tag_name().name()=="WaitTime")
                    .and_then(|l|l.text())
                    .map(parse_pt)
                    .transpose()?,
                activity:Activity::new(element.children()
                    .find(|x|x.tag_name().name()=="Activity")
                )?,
                timing_status:TimingStatus::new(element.children()
                    .find(|x|x.tag_name().name()=="TimingStatus")
                )?,
            })
        }else{
            Err("Not a StopUsage element")?
        }
    }
}

