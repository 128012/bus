// Journeys each reference a JourneyPattern which are inside services.
// JourneyPatterns reference JourneyPatternSections which are collections of
// JourneyPatternTimingLinks. These contain `JourneyPatternStopUsage`s

// - A VehicleJourney may override any common property it shares with a JourneyPattern
// - A VehicleJourneyTimingLink may   override   any   common   property   it   shares   with
//   a JourneyPatternTimingLink
// - A VehicleJourneyStopUsage may   override   any   common   property   it   shares   with
//   a JourneyPatternStopUsage, including the StopAccessibility.

// A JourneyPatternSection is a reusable ordered sequences of JourneyPatternTiming-Links)

// Patterns are a bit like classes
// - A Journey is an instance of a JourneyPattern
//   and can override properties of it

// `JP` means JourneyPattern

use std::collections::HashMap;
use crate::{journey,bank};

#[derive(Debug)]
pub struct Service<'a>{
    pub code:String,
    pub(crate) id:&'a str,

    pub operator:String,
    
    pub description:Option<&'a str>,
    pub origin:&'a str,
    pub dest:&'a str,
    pub via:Vec<&'a str>,

    pub(crate) regular_day_operation:Option<RegularDayOperation>,
    pub(crate) not_on:Option<Vec<txc_bank::BankHoliday>>,
    pub(crate) but_on:Option<Vec<txc_bank::BankHoliday>>,
    pub(crate) start:Option<chrono::naive::NaiveDate>,
    pub(crate) end:Option<chrono::naive::NaiveDate>,

    pub to_be_marketed_with:Vec<&'a str>,

    // but these are added later
    pub journeys:Vec<journey::Journey<'a>>, // only journeys that don't belong to a line
    pub lines:HashMap<&'a str,Line<'a>>
}
impl<'a> Service<'a>{
    pub fn new(
        element:roxmltree::Node<'a,'_>,
        operators:&HashMap<&'a str,String>
    )->Result<Self,&'static str>{
        let mut not_on=element.children() // not descendants because ServicedOrganisationDayType has it too
            .find(|x|x.tag_name().name()=="OperatingProfile")
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="BankHolidayOperation")
            )
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="DaysOfNonOperation")
            )
            .map(|l|Ok(l.children()
                .filter(|x|x.node_type()==roxmltree::NodeType::Element)
                .map(bank::new)
                .collect::<Result<Vec<_>,_>>()?
                .into_iter()
                .flatten()
                .collect::<Vec<_>>()
            ))
            .transpose()?;
        let mut but_on=element.children() // not descendants because ServicedOrganisationDayType has it too
            .find(|x|x.tag_name().name()=="OperatingProfile")
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="BankHolidayOperation")
            )
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="DaysOfOperation")
            )
            .map(|l|Ok(l.children()
                .filter(|x|x.node_type()==roxmltree::NodeType::Element)
                .map(bank::new)
                .collect::<Result<Vec<_>,_>>()?
                .into_iter()
                .flatten()
                .collect::<Vec<_>>()
            ))
            .transpose()?;
        not_on.as_mut().map(|x|x.sort_unstable());
        but_on.as_mut().map(|x|x.sort_unstable());

        let id=element.children()
                .find(|x|x.tag_name().name()=="ServiceCode")
                .ok_or("No ServiceCode")?
                .text()
                .ok_or("No service code")?;

        Ok(Self{
            code:element.descendants()
               .find(|x|x.tag_name().name()=="PrivateCode")
               .and_then(|l|l.text())
               .map(str::to_uppercase)
               .unwrap_or_else(||format!("<No proper code, but this instead: {}>",id)),
            id,

            operator:operators.get(element.children()
                .find(|x|x.tag_name().name()=="RegisteredOperatorRef")
                .ok_or("No RegisteredOperatorRef in Service")?
                .text()
                .ok_or("No text in RegisteredOperatorRef in Service")?
            ).ok_or("The RegisteredOperatorRef in Service doesn't exist")?.clone(),

            description:element.children()
                .find(|x|x.tag_name().name()=="Description")
                .and_then(|l|l.text()),
            origin:element.descendants()
                .find(|x|x.tag_name().name()=="Origin")
                .ok_or("No origin")?
                .text()
                .ok_or("No origin name")?,
            dest:element.descendants()
                .find(|x|x.tag_name().name()=="Destination")
                .ok_or("No destination")?
                .text()
                .ok_or("No destination name")?,
            via:element.descendants()
                .filter(|x|x.tag_name().name()=="Via")
                .filter_map(|x|x.text())
                .collect(),

            regular_day_operation:element.descendants()
                .find(|x|x.tag_name().name()=="RegularDayType")
                .map(RegularDayOperation::new)
                .transpose()?,
            not_on,
            but_on,
            start:if let Some(operating_period_l)=element.children()
                .find(|x|x.tag_name().name()=="OperatingPeriod"){
                    operating_period_l.children()
                        .find(|x|x.tag_name().name()=="StartDate")
                        .map(|l|chrono::naive::NaiveDate::parse_from_str(l.text()
                            .ok_or("There is a StartDate element but it has no text")?,
                            "%Y-%m-%d"
                        ).map_err(|_|"Invalid date in StartDate in VehicleJourney"))
                        .transpose()?
            }else{None},
            end:if let Some(operating_period_l)=element.children()
                .find(|x|x.tag_name().name()=="OperatingPeriod"){
                    operating_period_l.children()
                        .find(|x|x.tag_name().name()=="EndDate")
                        .map(|l|chrono::naive::NaiveDate::parse_from_str(l.text()
                            .ok_or("There is an EndDate element but it has no text")?,
                            "%Y-%m-%d"
                        ).map_err(|_|"Invalid date in EndDate in VehicleJourney"))
                        .transpose()?
            }else{None},

            to_be_marketed_with:element.descendants()
                .filter(|x|x.tag_name().name()=="RelatedService")
                .filter_map(|related_service_l|related_service_l.children()
                    .find(|x|x.tag_name().name()=="ServiceRef") // ServiceRef is better than description but might not be there
                    .and_then(|l|l.text())
                    .or_else(||related_service_l.children()
                        .find(|x|x.tag_name().name()=="Description")
                        .and_then(|l|l.text())
                    )
                )
                .collect(),

            journeys:Vec::new(),
            lines:element.descendants()
                .filter(|x|x.tag_name().name()=="Line")
                .map(Line::new)
                .collect::<Result<Vec<_>,_>>()?
                .into_iter()
                .map(|line|(line.id.clone(),line))
                .collect::<HashMap<_,_>>()
        })
    }
    
    pub(crate) fn add_journey(
        &mut self,
        journey:roxmltree::Node<'a,'_>,
        patterns:&HashMap<&'a str,journey::JourneyPattern<'a,'_>>
    )->Result<(),&'static str>{
        let journey=journey::Journey::new(journey,patterns,&self)?; // so it inherits
        if let Some(line_id)=journey.line_id{
            self.lines.get_mut(line_id).ok_or("Journey is on a line which isn't in its service")?
                .journeys
                .push(journey)
        }else{
            self.journeys.push(journey);
        }
        Ok(())
    }
    pub fn is_within(&self,latlng:(f64,f64),r:f64,or:&Option<Box<dyn Fn(&str)->bool>>,stops:&HashMap<String,crate::Stop>)->bool{
        self.journeys.iter()
            .any(|journey|journey.is_within(latlng,r,or,stops))
    }
}

#[derive(Debug)]
pub struct Line<'a>{
    id:&'a str,
    pub code:String,
    
    description:Option<&'a str>,
    origin:Option<&'a str>,
    pub dest:Option<&'a str>,
    via:Option<Vec<&'a str>>,
    
    pub journeys:Vec<journey::Journey<'a>>
}
impl<'a> Line<'a>{
    fn new(l:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        Ok(Self{
            id:l.attribute("id").ok_or("No id in Line")?,
            code:l.children()
                .find(|x|x.tag_name().name()=="LineName")
                .ok_or("No LineName in Line")?
                .text()
                .ok_or("No text in LineName in Line")?.to_uppercase(),

            description:l.children()
                .find(|x|x.tag_name().name()=="Description")
                .and_then(|l|l.text()),
            origin:l.descendants()
                .find(|x|x.tag_name().name()=="Origin")
                .and_then(|x|x.text()),
            dest:l.descendants()
                .find(|x|x.tag_name().name()=="Destination")
                .and_then(|x|x.text()),
            via:l.children()
                .find(|x|x.tag_name().name()=="Vias")
                .map(|vias|vias.children()
                    .filter(|x|x.tag_name().name()=="Via")
                    .filter_map(|x|x.text())
                    .collect::<Vec<_>>(),
                ),

            journeys:vec![]
        })
    }
    
    // make a service inheriting from the service in the argument
    pub fn to_service(self,service:&Service<'a>)->Service<'a>{
        Service{
            id:self.id,
            code:self.code,

            operator:service.operator.clone(),

            lines:HashMap::new(),

            description:self.description.or_else(||service.description.clone()),
            origin:self.origin.unwrap_or(service.origin.clone()),
            dest:self.dest.unwrap_or(service.dest.clone()),
            via:self.via.unwrap_or(service.via.clone()),

            regular_day_operation:service.regular_day_operation.clone(),
            not_on:service.not_on.clone(),
            but_on:service.but_on.clone(),
            start:service.start.clone(),
            end:service.end.clone(),

            to_be_marketed_with:service.to_be_marketed_with.clone(),

            journeys:self.journeys
        }
    }
}


const DAYS:[&str;7]=["monday","tuesday","wednesday","thursday","friday","saturday","sunday"];
fn day_index(s:&str)->Option<usize>{
    DAYS.iter().position(|x|x==&s)
}


#[derive(Debug,Clone)]
pub enum RegularDayOperation{
    HolidaysOnly,
    Weekdays([bool;7])
}
impl RegularDayOperation{
    pub(crate) fn new(regular_day_type_l:roxmltree::Node)->Result<Self,&'static str>{
        if let Some(days_of_week_l)=regular_day_type_l.children()
            .find(|x|x.tag_name().name()=="DaysOfWeek")
        {
            let mut r=[false;7];
            for l in days_of_week_l.children()
                .filter(|node|if let roxmltree::NodeType::Element=node.node_type(){true}else{false})
            {
                let tag=l.tag_name().name().trim().to_lowercase();
                let mut i=tag.split("to")
                    .take(2)
                    .filter(|x|x.len()>0);

                if i.clone().count()==2{ // aTob
                    let (from,to)=(i.next().unwrap(),i.next().unwrap());
                    if DAYS.contains(&from)&&DAYS.contains(&to){
                        let from_x=day_index(from).unwrap();
                        let mut to_x=day_index(to).unwrap();
                        
                        if to_x<from_x{
                            to_x+=7;
                        }

                        for x in from_x..=to_x{
                            r[x%7]=true;
                        }
                    }else{
                        Err("One of From or To wasn't a day")?
                    }
                }else if let Some(day)=i.next(){
                    // A single days rather than *To*
                    r[day_index(day).ok_or("The only day was not a day")?]=true;
                }else{
                    Err("An element in DaysOfWeek was just \"To\"")?
                }
            }
            Ok(Self::Weekdays(r))
        }else if regular_day_type_l.children()
            .find(|x|x.tag_name().name()=="HolidaysOnly")
            .is_some(){
            Ok(Self::HolidaysOnly)
        }else{
            Err("No elements in RegularDayType")
        }
    }
    pub(crate) fn all()->Self{
        // No RegularDayType l so assuming that it runs every day
        Self::Weekdays([true;7])
    }
    pub fn weekdays(&self)->Option<[bool;7]>{
        if let RegularDayOperation::Weekdays(x)=self{
            Some(x.clone())
        }else{None}
    }
}
