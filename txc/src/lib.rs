// lots of default values on p65
use std::collections::HashMap;

mod service;
mod bank;
pub mod journey;
pub mod visit;

pub struct File<'a>{
    document:roxmltree::Document<'a>
}
impl<'a> File<'a>{
    pub fn from_text(text:&'a str)->Result<Self,&'static str>{
        Ok(Self{
            document:roxmltree::Document::parse(&text)
                .map_err(|_|"Error parsing XML")?
        })
    }

    pub fn timetable(&'a self)->Result<Timetable<'a>,&'static str>{
        Timetable::new(self.document.root()
            .children()
            .find(|x|x.tag_name().name()=="TransXChange")
            .ok_or("No TransXChange element")?
        )
    }
}

pub struct Timetable<'b>{ // 'b is the lifetime of the File
    pub stops:HashMap<&'b str,Stop<'b>>,
    pub services:Vec<service::Service<'b>>,
}

impl<'b> Timetable<'b>{
    fn new(root:roxmltree::Node<'b,'_>)->Result<Self,&'static str>{
        let sections=root.children()
            .find(|x|x.tag_name().name()=="JourneyPatternSections")
            .ok_or("No JourneyPatternSections element")?
            .children()
            .filter(|x|x.tag_name().name()=="JourneyPatternSection")
            .map(journey::JourneyPatternSection::new)
            .collect::<Result<Vec<_>,_>>()?
            .into_iter()
            .map(|jps|(jps.id.clone(),jps))
            .collect::<HashMap<_,_>>();

        let patterns=root.descendants()
            .filter(|x|x.tag_name().name()=="JourneyPattern")
            .map(|l|journey::JourneyPattern::new(l,&sections))
            .collect::<Result<Vec<_>,_>>()?
            .into_iter()
            .map(|jp|(jp.id.clone(),jp))
            .collect::<HashMap<_,_>>();

        let stops=root.children()
            .find(|x|x.tag_name().name()=="StopPoints")
            .ok_or("No StopPoints element")?
            .children()
            .filter(|x|x.tag_name().name()=="AnnotatedStopPointRef")
            .map(|element|Stop::new(element))
            .collect::<Result<Vec<_>,&'static str>>()?
            .into_iter()
            .map(|stop|(stop.atco.clone(),stop))
            .collect::<HashMap<_,_>>();

        let operators=root.children()
             .find(|x|x.tag_name().name()=="Operators")
             .ok_or("No Operators element")?
             .children()
             .filter(|x|x.tag_name().name()=="Operator"||x.tag_name().name()=="LicensedOperator")
             .map(|l|Ok((
                l.attribute("id").ok_or("No id on Operator")?,
                l.children()
                    .find(|x|x.tag_name().name()=="OperatorCode")
                    .ok_or("No OperatorCode in Operator")?
                    .text()
                    .ok_or("No text in OperatorCode in Operator")?
                    .to_lowercase()
            )))
            .collect::<Result<HashMap<&str,String>,_>>()?;

        let mut services=root.children()
            .find(|x|x.tag_name().name()=="Services")
            .ok_or("No Services element")?
            .children()
            .filter(|x|x.tag_name().name()=="Service")
            .map(|l|service::Service::new(l,&operators))
            .collect::<Result<Vec<_>,_>>()?;

        for journey in root.children()
            .find(|x|x.tag_name().name()=="VehicleJourneys")
            .ok_or("No VehicleJourneys element")?
            .children()
            .filter(|x|x.tag_name().name()=="VehicleJourney")
        {
            let service_id=journey.children()
                .find(|x|x.tag_name().name()=="ServiceRef")
                .ok_or("No ServiceRef element")?
                .text()
                .ok_or("No service ref text")?;
            services.iter_mut()
                .find(|service|service.id==service_id)
                .ok_or("Journey has a service id which doesn't exist")?
                .add_journey(journey,&patterns)?;
        }
        
        Ok(Self{
            services,
            stops,
        })
    }
}

#[derive(Clone)]
pub struct Stop<'a>{
    // Magdalen Street is -1.2590858453,51.7543528376
    // Lock-up         is -1.1395553385,51.7480451845
    pub latlng:Option<(f64,f64)>,
    pub atco:&'a str,
    pub name:&'a str,
}
impl<'a> Stop<'a>{
    fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        Ok(Stop{
            name:element.children()
                .find(|x|x.tag_name().name()=="CommonName")
                .ok_or("No CommonName element")?
                .text()
                .ok_or("No CommonName")?,
            atco:element.children()
                .find(|x|x.tag_name().name()=="StopPointRef")
                .ok_or("No StopPointRef element in AnnotatedStopPointRef")?
                .text()
                .ok_or("No atco code")?,
            latlng:element.children()
                .find(|x|x.tag_name().name()=="Location")
                .and_then(|location_l|{
                    let mut lat=None;
                    let mut long=None;
                    for l in location_l.children(){
                        if l.tag_name().name()=="Latitude"{
                            if let Some(lat_text)=l.text(){
                                if let Ok(lat_f32)=lat_text.parse(){
                                    lat=Some(lat_f32);
                                }
                            }
                        }else if l.tag_name().name()=="Longitude"{
                            if let Some(long_text)=l.text(){
                                if let Ok(long_f32)=long_text.parse(){
                                    long=Some(long_f32);
                                }
                            }
                        }
                    }
                    lat.and_then(|lat|long.map(|long|(lat,long)))
                })
        })
    }
}


#[derive(Copy,Clone,Debug,PartialEq,Eq,Hash,Ord,PartialOrd)]
pub enum Direction{
    InboundAndOutbound,
    Outbound,
    Inbound,
    Circular,
    Clockwise,
    Anticlockwise
}
impl Direction{
    fn new(s:&str)->Result<Direction,&'static str>{
        Ok(match s.to_lowercase().as_ref(){
            "inbound"=>Direction::Inbound,
            "outbound"=>Direction::Outbound,
            "inboundandoutbound"=>Direction::InboundAndOutbound,
            "circular"=>Direction::Circular,
            "clockwise"=>Direction::Clockwise,
            "anticlockwise"=>Direction::Anticlockwise,
            _=>Err("No such direction")?
        })
    }
}
impl std::fmt::Display for Direction{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f,"{}",match self{
            Self::Inbound=>"Inbound",
            Self::Outbound=>"Outbound",
            Self::InboundAndOutbound=>"Inbound and outbound",
            Self::Circular=>"Circular",
            Self::Clockwise=>"Clockwise",
            Self::Anticlockwise=>"Anticlockwise",
        })
    }
}


// TODO: use
#[allow(unused)]
struct Operator{
    name:String,
    id:String
}
