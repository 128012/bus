use std::collections::HashMap;
use crate::{service,visit,bank};

#[derive(Debug)]
pub struct Journey<'a>{
    pub direction:crate::Direction,
    pub description:Option<&'a str>,
    
    pub regular_day_operation:service::RegularDayOperation,
    pub not_on:Vec<txc_bank::BankHoliday>,
    pub but_on:Vec<txc_bank::BankHoliday>,
    pub start:Option<chrono::naive::NaiveDate>,
    pub end:Option<chrono::naive::NaiveDate>,
    
    pub visits:Vec<visit::Visit<'a>>,

    pub line_id:Option<&'a str>,

    pub operator:String
}
impl<'a> Journey<'a>{
    pub(crate) fn new(
        element:roxmltree::Node<'a,'_>,
        patterns:&HashMap<&'a str,JourneyPattern<'a,'_>>,
        service:&service::Service<'a>
    )->Result<Self,&'static str>{
        let departure_time=chrono::naive::NaiveTime::parse_from_str(
                element.children()
                    .find(|x|x.tag_name().name()=="DepartureTime")
                    .ok_or("No DepartureTime element")?
                    .text()
                    .ok_or("No departure time text")?,
                "%H:%M:%S"
            )
                .map_err(|_|"Invalid DepartureTime")?;

        let jp=patterns
            .get(element.children()
                  .find(|x|x.tag_name().name()=="JourneyPatternRef")
                  .ok_or("No JourneyPatternRef in VJ")?
                  .text()
                  .ok_or("No Journey Pattern Ref in VJ")?
            );
        let jp:&JourneyPattern=jp.as_ref().unwrap();

        let mut vjtls=element.children()
             .filter(|x|x.tag_name().name()=="VehicleJourneyTimingLink")
             .map(IncompleteTL::new)
             .collect::<Result<Vec<_>,_>>()?
             .into_iter()
             .map(|x|Some(x))
             .collect::<Vec<_>>();

        let timing_links=jp.sections.iter()
            .flat_map(|section|&section.timing_links)
            .map(|jptl|jptl.clone())
            .map(|mut jptl|{
                if let Some((x,_))=jptl.id.as_ref()
                   .and_then(|jptl_id|vjtls.iter().enumerate()
                       .find(|(_,vjtl)|if let Some(vjtl)=vjtl{&vjtl.jptl_id==jptl_id}else{false})
                   ){
                   jptl.also(vjtls.get_mut(x).unwrap().take().unwrap());
                }
                jptl
            })
            .collect::<Vec<TL>>(); // some tls
        // mostly use From, but use To if necessary
        let visits=(0..timing_links.len())
            .flat_map(|i|{
                let tl=&timing_links[i];

                if i>0{
                    let before=&timing_links[i-1];

                    if i==timing_links.len()-1{ // last one
                        vec![
                            (before.get_run_time(),tl.get_from()),
                            (tl.get_run_time(),tl.get_to())
                        ]
                    }else{
                        vec![(before.get_run_time(),tl.get_from())]
                    }
                }else{
                    vec![(chrono::Duration::zero(),tl.get_from())]
                }
            })
            // make the visits' times absolute
            .scan(departure_time,|current_time,(run_time,mut visit)|{
                let wait=*visit.wait.as_ref().unwrap_or(&chrono::Duration::zero());
                *current_time+=run_time+wait;
                visit.arrival=*current_time-wait;
                Some(visit)
            })
            .collect::<Vec<visit::Visit>>();

        // operation days
        let mut not_on=element.children() // not descendants because ServicedOrganisationDayType has it too
            .find(|x|x.tag_name().name()=="OperatingProfile")
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="BankHolidayOperation")
            )
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="DaysOfNonOperation")
            )
            .map(|l|Ok(l.children()
                .filter(|x|x.node_type()==roxmltree::NodeType::Element)
                .map(bank::new)
                .collect::<Result<Vec<_>,_>>()?
                .into_iter()
                .flatten()
                .collect::<Vec<_>>()
            ))
            .transpose()?;
        let mut but_on=element.children() // not descendants because ServicedOrganisationDayType has it too
            .find(|x|x.tag_name().name()=="OperatingProfile")
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="BankHolidayOperation")
            )
            .and_then(|l|l.children()
                .find(|x|x.tag_name().name()=="DaysOfOperation")
            )
            .map(|l|Ok(l.children()
                .filter(|x|x.node_type()==roxmltree::NodeType::Element)
                .map(bank::new)
                .collect::<Result<Vec<_>,_>>()?
                .into_iter()
                .flatten()
                .collect::<Vec<_>>()
            ))
            .transpose()?;
        
        not_on.as_mut().map(|x|x.sort_unstable());
        but_on.as_mut().map(|x|x.sort_unstable());
        let not_on=not_on.or_else(||service.not_on.clone())
            .unwrap_or_else(Vec::new);
        let but_on=but_on.or_else(||service.but_on.clone())
            .unwrap_or_else(Vec::new);

        Ok(Self{
            direction:jp.direction.clone(),
            description:jp.description.clone(),
            visits,            
                
            regular_day_operation:element.descendants()
                .find(|x|x.tag_name().name()=="RegularDayType")
                .map(service::RegularDayOperation::new)
                .transpose()?
                .or_else(||service.regular_day_operation.clone())
                .unwrap_or_else(service::RegularDayOperation::all),
            not_on,
            but_on,
            start:service.start,
            end:service.end,
            
            line_id:element.children()
                .find(|x|x.tag_name().name()=="LineRef")
                .and_then(|x|x.text()),

            operator:service.operator.clone()
        })
    }
    
    pub fn happens_on(&self,bank_holidays:&txc_bank::BankHolidays,date:&chrono::naive::NaiveDate)->bool{
        // start date
        (self.start.filter(|start_date|date>=start_date).is_some()||self.start.is_none())
            // end date
            &&(self.end.filter(|end_date|date<=end_date).is_some()||self.end.is_none())
            // weekdays
            &&match &self.regular_day_operation{
                service::RegularDayOperation::Weekdays(days)=>days[chrono::Datelike::weekday(date).num_days_from_monday() as usize],
                service::RegularDayOperation::HolidaysOnly=>self.but_on.iter().any(
                    |bank_holiday|bank_holidays.bank_holiday_happens_on(
                        bank_holiday,
                        &date
                    )
                )
            }
            // bank_holiday days off
            &&(!self.not_on.iter().any(|bank_holiday|bank_holidays.bank_holiday_happens_on(
                bank_holiday,
                &date
            )))
    }
    pub fn is_within(&self,(lat,lng):(f64,f64),r:f64,or:&Option<Box<dyn Fn(&str)->bool>>,stops:&HashMap<String,crate::Stop>)->bool{
        self.visits.iter()
            .any(|visit|stops[visit.atco].latlng // try the TXC file's location first
                .map(|(stop_lat,stop_lng)|((
                    (stop_lat-lat).powi(2)+(stop_lng-lng).powi(2)
                ).sqrt() as f64)<r)
                .unwrap_or_else(||or.as_ref()
                    // There is an alternative to knowing the location from the TXC file
                    .map(|or|or(&visit.atco))
                    // don't know, assume not
                    .unwrap_or(false)
                )
            )
            // distance
            /*.any(|(stop_lat,stop_lng)|((
                (stop_lat-lat).powi(2)+(stop_lng-lng).powi(2)
            ).sqrt() as f32)<r)*/
    }
}


pub(crate) struct JourneyPattern<'a,'b>{
    pub screen:&'a str,
    pub direction:crate::Direction,
    pub description:Option<&'a str>,
    pub(crate) id:&'a str,
    sections:Vec<&'b JourneyPatternSection<'a>>
}
impl<'a,'b> JourneyPattern<'a,'b>{
    pub(crate) fn new(
        element:roxmltree::Node<'a,'_>,
        sections:&'b HashMap<&'a str,JourneyPatternSection<'a>>
    )->Result<Self,&'static str>{
        let section_ids=element.children()
            .filter(|x|x.tag_name().name()=="JourneyPatternSectionRefs")
            .filter_map(|x|x.text())
            .map(|id|sections.get(id)
                .ok_or("A JourneyPatternSectionRef of a JourneyPattern isn't that of a JourneyPatternSection")
            )
            .collect::<Result<Vec<_>,_>>()?;
            
        if section_ids.len()>0{
            Ok(Self{
                screen:element.children()
                    .find(|x|x.tag_name().name()=="DestinationDisplay")
                    .ok_or("No DestinationDisplay element")?
                    .text()
                    .ok_or("No destination display value")?,
                direction:crate::Direction::new(element.children()
                    .find(|x|x.tag_name().name()=="Direction")
                    .ok_or("No Direction element")?
                    .text()
                    .ok_or("No direction value")?
                )?,
                description:element.children()
                    .find(|x|x.tag_name().name()=="Description")
                    .map(|l|Ok(l
                        .text()
                        .ok_or("No description value")?
                    ))
                    .transpose()?,
                id:element.attribute("id")
                    .ok_or("No id")?,
                sections:section_ids
            })
        }else{
            Err("No JourneyPatternSectionRefs in JourneyPattern")
        }
    }
}

pub(crate) struct JourneyPatternSection<'a>{
    pub(crate) id:&'a str,
    timing_links:Vec<TL<'a>>
}
impl<'a> JourneyPatternSection<'a>{
    pub(crate) fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        Ok(Self{
            id:element.attribute("id")
                .ok_or("No id")?,
            timing_links:element.children()
                .filter(|x|x.tag_name().name()=="JourneyPatternTimingLink")
                .map(TL::new)
                .collect::<Result<Vec<_>,_>>()?
        })
    }
}





#[derive(Clone)]
struct TL<'a>{
    // whose arrival=0
    from:visit::Visit<'a>,
    to:visit::Visit<'a>,
    run_time:chrono::Duration,
    id:Option<&'a str>,

    also:Option<IncompleteTL<'a>>
}
impl<'a> TL<'a>{
    /// makes a TL with unset fields "inherited" from the base
    fn also(&mut self,tl:IncompleteTL<'a>){
        self.also.replace(tl);
    }

    fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        if element.tag_name().name()=="JourneyPatternTimingLink"{
             Ok(Self{
                 from:visit::Visit::new(element.children()
                     .find(|x|x.tag_name().name()=="From")
                     .ok_or("No From element in JPTL")?
                 )?,
                 to:visit::Visit::new(element.children()
                     .find(|x|x.tag_name().name()=="To")
                     .ok_or("No To element in JPTL")?
                 )?,
                 run_time:visit::parse_pt(element.children()
                     .find(|x|x.tag_name().name()=="RunTime")
                     .and_then(|l|l.text())
                     .ok_or("No RunTime in JPTL")?
                 )?,
                 id:element.attribute("id"),
                 also:None
             })
         }else{
             Err("Not a JourneyPatternTimingLink element")
         }
    }
    fn get_from(&self)->visit::Visit<'a>{
        if let Some(also)=&self.also{
            if let Some(incomplete_from)=&also.from{
                incomplete_from.clone().also(self.from.clone())
            }else{
                self.from.clone()
            }
        }else{
            self.from.clone()
        }
    }
    fn get_to(&self)->visit::Visit<'a>{
        if let Some(also)=&self.also{
            if let Some(incomplete_to)=&also.to{
                incomplete_to.clone().also(self.to.clone())
            }else{
                self.to.clone()
            }
        }else{
            self.to.clone()
        }
    }
    pub fn get_run_time(&self)->chrono::Duration{
            self.also.as_ref()
            .and_then(|also|also.run_time.clone())
            .unwrap_or(self.run_time.clone())
    }
}

#[derive(Clone)]
pub(crate) struct IncompleteTL<'a>{
        from:Option<visit::IncompleteStopUsage<'a>>,
        to:Option<visit::IncompleteStopUsage<'a>>,
        run_time:Option<chrono::Duration>,
        pub jptl_id:&'a str
}
impl<'a> IncompleteTL<'a>{
    pub fn new(element:roxmltree::Node<'a,'_>)->Result<Self,&'static str>{
        if element.tag_name().name()=="VehicleJourneyTimingLink"{
            Ok(Self{
                from:if let Some(l)=element.children()
                    .find(|x|x.tag_name().name()=="From"){
                    Some(visit::IncompleteStopUsage::new(l)?)
                }else{None},
                to:if let Some(l)=element.children()
                    .find(|x|x.tag_name().name()=="To"){
                    Some(visit::IncompleteStopUsage::new(l)?)
                }else{None},
                run_time:if let Some(t)=element.children()
                    .find(|x|x.tag_name().name()=="RunTime")
                    .and_then(|l|l.text()){
                    Some(visit::parse_pt(t)?)
                }else{None},
                jptl_id:element.children()
                    .find(|x|x.tag_name().name()=="JourneyPatternTimingLinkRef")
                    .ok_or("No JourneyPatternTimingLinkRef in VehicleJourneyTimingLink")?
                    .text()
                    .ok_or("No jptl ref in vjtl")?,
            })
        }else{
            Err("Not a VehicleJourneyTimingLink")
        }
    }
}
