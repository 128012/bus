mod overpass;
pub mod coordinates;

use types::{stop,service};

use std::collections::{HashMap,HashSet};
use std::cmp::Ordering;

#[derive(Clone,Debug)]
pub struct Segment{
    length:f64,
    points:[(f64,f64);2],

    pub start_node:usize,
    pub end_node:usize,

    pub services:HashSet<String>
}
impl Segment{
    // first half keeps its index but is changed, second half is returned
    fn split(&mut self,stop:(f64,f64),number_of_nodes:usize)->Self{
        let [a,b]=self.points;

        // where the segment is divided - the closest point on it to the stop
        let cut_at=coordinates::closest_on_line(stop,a,b);

        self.points=[a,cut_at];

        self.length=self.length/2.; // doesn't matter because no other path to take

        // start node is the same
        let end_node=self.end_node; // will need it later
        self.end_node=number_of_nodes+1; // create a new node

        Segment{
            length:self.length, // already divided by 2
            points:[cut_at,b],
            start_node:number_of_nodes+1, // this new node
            end_node, // copied before self.end_node was overwritten
            services:self.services.clone()
        }
    }
    pub fn svg(&self,bbox:coordinates::BBox)->Option<String>{
        let [a,b]=self.points
            .map(|xy|bbox.project(xy));

        coordinates::line_in_viewport(a,b,bbox).then(||svg_line(a,b,&self.services.iter().map(|x|x.as_str()).collect::<Vec<_>>().join(",")))
    }
    pub fn project(&self,bbox:coordinates::BBox)->Option<[(f64,f64);2]>{
        let [a,b]=self.points
            .map(|xy|bbox.project(xy));
        coordinates::line_in_viewport(a,b,bbox).then(||[a,b])
    }
    fn svg_with_info(&self,bbox:coordinates::BBox,info:&str)->Option<String>{
        let [a,b]=self.points
            .map(|xy|bbox.project(xy));

        coordinates::line_in_viewport(a,b,bbox).then(||svg_line(a,b,info))
    }

    pub fn first(&self)->(f64,f64){
        self.points[0]
    }
    pub fn last(&self)->(f64,f64){
        *self.points.last().unwrap()
    }
    pub fn unprojected_first_point(&self)->(f64,f64){
        self.points[0]
    }
    pub fn unprojected_last_point(&self)->(f64,f64){
        *self.points.last().unwrap()
    }
    // in radians
    // +-+~
    // |  O+
    // +----
    // ?
    pub fn angle_at_node(&self,node:usize)->f64{
        let (this,next)=if self.start_node==node{
            (self.first(),self.last())
        }else{
            (self.last(),self.first())
        };


        let x=this.0-next.0;
        let y=this.1-next.1;

        // increasing as next goes anticlockwise so need to reverse 
        match next.0.partial_cmp(&this.0).unwrap(){
            // next < this
            // next is to the left of this
            Ordering::Less=>std::f64::consts::PI+(y/x).atan(),
            // next> this
            // next is to the right of this
            Ordering::Greater=>(y/x).atan(),
            // vertically aligned
            Ordering::Equal=>if next.1.partial_cmp(&this.1).unwrap()==Ordering::Greater{
                std::f64::consts::FRAC_PI_2
            }else{
                std::f64::consts::FRAC_PI_2*3.0
            }
        }
    }
    pub fn direction(&self)->i8{
        coordinates::direction(self.points[0],self.points[1])
    }
}
impl petgraph::IntoWeightedEdge<f64> for Segment{
    type NodeId=usize;
    fn into_weighted_edge(self)->(Self::NodeId,Self::NodeId,f64){
        (self.start_node,self.end_node,self.length)
    }
}

// avoids repeating work for every single route by persisting after each route's path is created
pub struct Map{
    segments:Vec<Segment>,
    // None if it is outside the OSM area
    // No, it is not present in that case
    node_of_each_atco:HashMap<String,usize>,
    number_of_nodes:usize
}
impl Map{
    pub fn new(path:&str,stops:&HashMap<String,stop::Stop>)->Self{
        let overpass_response:overpass::Response=serde_json::from_reader(
            std::io::BufReader::new(
                std::fs::File::open(path).unwrap()
            )
        ).unwrap();
        let (segments,number_of_nodes):(Vec<Segment>,usize)=overpass_response.segments();
        let mut map=Self{
            segments,
            node_of_each_atco:HashMap::new(), // filled in gradually as stops are added
            number_of_nodes
        };
        for stop in stops.values(){
            if let (Some(lat),Some(lng))=(stop.lat,stop.lng){
                // might not be within the bounds
                if let Some(node_index)=map.stop_on_road((lat,lng)){
                    map.node_of_each_atco.insert(stop.atco.to_string(),node_index);
                }
            }
        }
        map
    }

    // adds the code to the segments the service travels on
    // -> lots of coordinates of paths on the route
    pub fn add_code<'a>(
        &mut self,
        code:&str,
        trips:impl Iterator<Item=&'a service::Trip>,
    )->Vec<[(f64,f64);2]>{
        // - first get indices of segments on route
        let segments:Vec<usize>=self.segments_from_route(trips).into_iter()
            // add code to segments
            .inspect(|&x|{self.segments[x].services.insert(code.to_string());})
            .collect();
            // - then convert these segments to their coordinates
        segments.into_iter()
            .map(|segment_i|self.segments[segment_i].points)
            .collect()
    }

    pub fn segments(&self)->&[Segment]{
        &self.segments
    }

    pub fn graph(&self)->petgraph::Graph<u8,f64,petgraph::Undirected,usize>{
        petgraph::Graph::from_edges(
            self.segments.clone()
        )
    }

    // segments joining the stops in these trips
    fn segments_from_route<'a>(
        &self,
        trips:impl Iterator<Item=&'a service::Trip>,
        //stops:&HashMap<String,stop::Stop>
    )->HashSet<usize>{
        // create graph now that all roads are split etc
        let graph=self.graph();        
        // pairs
        service::atco_pairs(trips).into_iter()
            // atco->index
            .filter_map(|(a_atco,b_atco)|self.node_of_each_atco.get(a_atco)
                .and_then(|&a_idx|self.node_of_each_atco.get(b_atco)
                    .map(|&b_idx|(a_idx,b_idx))
                )
            )

            .map(|(from,to)|segments_on_route(&graph,from,to))
            .flatten()
            .collect()
    }

    // splits the road in two - the node in the middle is the stop
    // but not if the stop is outside the OSM data
    // this is useful for being able to add trips later without roads changing
    fn stop_on_road(
        &mut self,
        (lat,lng):(f64,f64)
    )->Option<usize>{ // index of stop's point
        (coordinates::S<lat&&lat<coordinates::N
            &&coordinates::W<lng&&lng<coordinates::E
        ).then(||{
            let xy=coordinates::from_origin((lat,lng));
            let segment_i=self.segments.iter().enumerate()
                .min_by_key(|(_,segment)|coordinates::line_segment_to_point(
                    segment.points[0],
                    segment.points[1],
                    xy
                )as u32).unwrap().0;

                let new_segment=self.segments.get_mut(segment_i).unwrap()
                    .split(xy,self.number_of_nodes);
                self.segments.push(new_segment);
                self.number_of_nodes+=1;

                self.number_of_nodes // stop's point will be the newly created one
        })
    }

    // svg map framing the route
    // it is either 1000px wide or high
    pub fn svg_with_points(&self,points:&[[(f64,f64);2]],colour:Option<&str>)->String{
        let bbox=coordinates::BBox::new_wsen((
            points.iter().flatten().map(|(x,_)|x).fold(1f64/0.,|a,&b|a.min(b)),
            points.iter().flatten().map(|(_,y)|y).fold(0.,|a,&b|a.max(b)),
            points.iter().flatten().map(|(x,_)|x).fold(0.,|a,&b|a.max(b)),
            points.iter().flatten().map(|(_,y)|y).fold(1f64/0.,|a,&b|a.min(b)),
        ));
        self.svg_from_points_and_bbox(points,colour,bbox)
    }

    // takes a bbox rather than creating from points
    pub fn svg_from_points_and_bbox(&self,points:&[[(f64,f64);2]],colour:Option<&str>,bbox:coordinates::BBox)->String{
        //let (_,(width,height))=coordinates::sf_from_bbox(bbox);
        let width=bbox.width();
        let height=bbox.height();

        format!(
        r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="{w}" height="{h}">
            <g stroke="#ccc" stroke-width="2" stroke-linecap="round">{roads}</g>
            <g stroke="{colour}" stroke-linecap="round" stroke-width="4">{route}</g>
            <text x="{text_x}" y="{text_y}" text-anchor="end" font-size="8pt">
                Lines drawn from timetables (so may not show actual route); and lines leaving the bounds are not shown; © <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>
            </text>
            </svg>"##,
            w=width,
            h=height,
            text_x=width-10.,
            text_y=height-10.,
            colour=colour.unwrap_or("red"),
            roads=self.segments.iter()
                .filter_map(|segment|segment.svg(bbox))
                .collect::<Vec<_>>()
                .join(""),
            route=points.into_iter()
                .map(|path|path
                    .map(|latlng|bbox.project(latlng))
                )
                .filter_map(|[a,b]|coordinates::line_in_viewport(a,b,bbox)
                    .then(||svg_line(a,b,""))
                )
                .collect::<Vec<_>>()
                .join(""),
        )
    }
    pub fn svg_from_bbox_and_info(&self,bbox:coordinates::BBox,info:&HashMap<usize,String>)->String{
        let width=bbox.width();
        let height=bbox.height();

        format!(
        r##"<?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="{w}" height="{h}">
            <g stroke="#ccc" stroke-width="2" stroke-linecap="round">{roads}</g>
            <text x="{text_x}" y="{text_y}" text-anchor="end" font-size="8pt">
                Lines drawn from timetables (so may not show actual route)
            </text>
            </svg>"##,
            w=width,
            h=height,
            text_x=width-10.,
            text_y=height-10.,
            roads=self.segments.iter().enumerate()
                .filter_map(|(i,segment)|segment.svg_with_info(bbox,info.get(&i).map(|x|x.as_str()).unwrap_or("no info")))
                .collect::<Vec<_>>()
                .join(""),
        )
    }

    pub fn project(&self,bbox:coordinates::BBox)->HashMap<usize,[(f64,f64);2]>{
        self.segments.iter().enumerate()
            .filter_map(|(i,segment)|segment.project(bbox).map(|points|(i,points)))
            .collect()
    }
}

// one stop's node+another stop's node -> segments on route
fn segments_on_route(graph:&petgraph::Graph<u8,f64,petgraph::Undirected,usize>,start:usize,end:usize)->Vec<usize>{
    if let Some(x)=petgraph::algo::astar(
        &graph,
        petgraph::graph::NodeIndex::new(start),
        |maybe_to|maybe_to.index()==end,
        |edge|*edge.weight(),
        |_|0. // no useful heuristic -> just Dijkstra
    ){
        x.1
            .into_iter()
            .collect::<Vec<_>>()
            .windows(2)
            // what segment is between these two nodes
            .map(|node_pair|graph.find_edge(
                node_pair[0],
                node_pair[1]
            ).unwrap().index())
            .collect()
    }else{
        vec![]
    }
}

fn svg_line(a:(f64,f64),b:(f64,f64),info:&str)->String{
    format!(
        r##"<line data-info="{info}" x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/>"##,
            info=info,
            x1=a.0,
            y1=a.1,
            x2=b.0,
            y2=b.1
        )
}
