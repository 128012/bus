use serde::Deserialize;
use super::{coordinates,Segment};
use std::collections::{HashMap,HashSet};

#[derive(Deserialize)]
#[serde(tag="type")] // "type":"node" inside object
enum Thing{
    #[serde(rename="node")]
    Node{
        id:u64,
        lat:f64,
        #[serde(rename="lon")]
        lng:f64
    },
    #[serde(rename="way")]
    Way{nodes:Vec<u64>}
}

#[derive(Deserialize)]
pub struct Response{
    elements:Vec<Thing>
}
impl Response{
    pub fn segments(self)->(Vec<Segment>,usize){
        let mut segments:Vec<Segment>=vec![];

        // my node index of each OSM node id
        let mut node_indices_and_positions:HashMap<u64,(usize,(f64,f64))>=HashMap::new();

        for thing in self.elements{
            match thing{
                Thing::Node{id,lat,lng}=>{
                    let node_index=node_indices_and_positions.len();
                    let (x,y)=coordinates::from_origin((lat,lng));
                    node_indices_and_positions.insert(id,(node_index,(x,y)));
                    //node_positions.push((lat,lng));
                },
                Thing::Way{nodes}=>{
                    let positions:Vec<(usize,(f64,f64))>=nodes.iter()
                         // nodes come before ways in the JSON
                        .map(|node_id|node_indices_and_positions[node_id])
                        .collect();
                    //for
                    for two in positions.windows(2){
                        if let &[(node_id_a,xy_a),(node_id_b,xy_b),..]=two{
                            segments.push(Segment{
                                length:coordinates::pythagoras(xy_a,xy_b),
                                points:[xy_a,xy_b],

                                start_node:node_id_a,
                                end_node:node_id_b,

                                services:HashSet::new()
                            });
                        }else{unreachable!()}
                    }
                }
            }
        }

        (segments,node_indices_and_positions.len()-1)
    }
}
