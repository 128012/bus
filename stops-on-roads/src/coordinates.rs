// https://github.com/visualjeff/haversine/blob/master/src/lib.rs
// The MIT License (MIT) Copyright Jeff

// two coordinates -> distance in metres
fn haversine((start_lat,start_lng):(f64,f64),(end_lat,end_lng):(f64,f64)) -> f64 {
    let r: f64 = 6371.; // km

    let d_lat: f64 = (end_lat - start_lat).to_radians();
    let d_lon: f64 = (end_lng - start_lng).to_radians();
    let lat1: f64 = (start_lat).to_radians();
    let lat2: f64 = (end_lat).to_radians();

    let a: f64 = ((d_lat/2.0).sin()) * ((d_lat/2.0).sin()) + ((d_lon/2.0).sin()) * ((d_lon/2.0).sin()) * (lat1.cos()) * (lat2.cos());
    let c: f64 = 2.0 * ((a.sqrt()).atan2((1.0-a).sqrt()));

    // * 1000 - km to m
    return r * c*1000.0;
}

pub fn pythagoras((x0,y0):(f64,f64),(x1,y1):(f64,f64))->f64{
    (
        (x0-x1).powi(2)
        +(y0-y1).powi(2)
    ).sqrt()
}

pub const W:f64=-1.332672; 
pub const S:f64=51.649493;
pub const E:f64=-1.117897;
pub const N:f64=51.831657; 

pub fn from_origin((lat,lng):(f64,f64))->(f64,f64){
    (
        haversine((N,W),(N,lng)),
        haversine((N,W),(lat,W))
    )
}


pub fn line_segment_to_point((ax,ay):(f64,f64),(bx,by):(f64,f64),(cx,cy):(f64,f64))->f64{
    let u=( (cx-ax)*(bx-ax)+(cy-ay)*(by-ay) )/(pythagoras((ax,ay),(bx,by)) as f64).powi(2);

    if (0.0..1.0).contains(&u){
        // C is closer to line segment than A or B
        let x=ax+u*(bx-ax);
        let y=ay+u*(by-ay);
        pythagoras((x,y),(cx,cy)).into()
    }else{
        // c is closer to an end of the line segment
        // pick the closest
        pythagoras((ax,ay),(cx,cy)).min(pythagoras((bx,by),(cx,cy))).into()
    }
}

#[derive(Copy,Clone)]
pub struct BBox{
    w:f64,
    s:f64,
    e:f64,
    n:f64,

    // transformed
    width:f64,
    height:f64,
    sf:f64
}
impl BBox{
    pub fn new_wsen((w,s,e,n):(f64,f64,f64,f64))->Self{
        let east_west=e-w;
        let north_south=s-n;

        let sf=1000./east_west.min(north_south);
        
        Self{
            w,s,e,n,
            width:east_west*sf,
            height:north_south*sf,
            sf
        }
    }
    pub fn new_wsen_degrees((w,s,e,n):(f64,f64,f64,f64))->Self{
        let a=from_origin((n,w));
        let b=from_origin((s,e));
        Self::new_diagonal(a,b)
    }
    pub fn new_diagonal((w,n):(f64,f64),(e,s):(f64,f64))->Self{
        Self::new_wsen((w,s,e,n))
    }
    pub fn new_around_point_degrees((lat,lng):(f64,f64))->Self{
        let (x,y)=from_origin((lat,lng));
        Self::new_wsen((
            x-100.,
            y+100.,
            x+100.,
            y-100.
        ))
    }
    pub fn project(&self,(x,y):(f64,f64))->(f64,f64){
        (
            self.sf*(x-self.w),
            self.sf*(y-self.n)
        )
    }
    pub fn width(&self)->f64{self.width}
    pub fn height(&self)->f64{self.height}
    pub fn wsen(&self)->(f64,f64,f64,f64){
        (self.w,self.s,self.e,self.n)
    }

}


// xy0 - point; xy1+2 - line
pub fn closest_on_line((x0,y0):(f64,f64),(x1,y1):(f64,f64),(x2,y2):(f64,f64))->(f64,f64){
    let g=(y2-y1)/(x2-x1);
    let c=y1-g*x1;

    // https://www.desmos.com/calculator/elaect5g5a
    let y=(g*g*y0+g*x0+c)/(g*g+1.0);
    let x=x0-g*(y-y0);

    (x,y)
}

// - transforms a point to put it at the same place in SVG's space which shall be
//   from 0 to 1000
// - only being correct spherically on the left and bottom sides
/*pub const SVG_SIZE:f64=1000.;
pub fn svg_bbox((w,s,e,n):(f64,f64,f64,f64),(lat,lng):(f64,f64))->(f64,f64){
    let (sf,(_,height))=sf_from_bbox((w,s,e,n));

    // relative to bottom left - south west
    // also in metres
    let relative_x=distance((s,w),(s,lng))*if lng<w{-1.}else{1.}+100.;
    let relative_y=distance((s,w),(lat,w))*if lat<s{-1.}else{1.}+100.;

    let new_x_scaled=relative_x*sf;
    let new_y_scaled=relative_y*sf;

    // height instead of SVG_SIZE so the map doesn't be at the bottom if it isn't tall
    let new_y_flipped=height-new_y_scaled;

    (new_x_scaled,new_y_flipped)
}
pub fn svg_bbox_no_padding((w,s,e,n):(f64,f64,f64,f64),(lat,lng):(f64,f64))->(f64,f64){
    let (sf,(_,height))=sf_from_bbox_no_padding((w,s,e,n));

    // relative to bottom left - south west
    // also in metres
    let relative_x=distance((s,w),(s,lng))*if lng<w{-1.}else{1.}+100.;
    let relative_y=distance((s,w),(lat,w))*if lat<s{-1.}else{1.}+100.;

    let new_x_scaled=relative_x*sf;
    let new_y_scaled=relative_y*sf;

    // height instead of SVG_SIZE so the map doesn't be at the bottom if it isn't tall
    let new_y_flipped=height-new_y_scaled;

    (new_x_scaled,new_y_flipped)
}

pub fn sf_from_bbox((w,s,e,n):(f64,f64,f64,f64))->(f64,(f64,f64)){
    // metres
    let width=distance((s,w),(s,e))+200.; // bottom left to bottom right
    let height=distance((s,w),(n,w))+200.; // bottom left to top left

    let horizontal_sf=SVG_SIZE/width;
    let vertical_sf=SVG_SIZE/height;
    let sf=horizontal_sf.min(vertical_sf);
    (sf,(width*sf,height*sf))
}
pub fn sf_from_bbox_no_padding((w,s,e,n):(f64,f64,f64,f64))->(f64,(f64,f64)){
    // metres
    let width=distance((s,w),(s,e)); // bottom left to bottom right
    let height=distance((s,w),(n,w)); // bottom left to top left

    let horizontal_sf=SVG_SIZE/width;
    let vertical_sf=SVG_SIZE/height;
    let sf=horizontal_sf.min(vertical_sf);
    (sf,(width*sf,height*sf))
}*/

// to use a bbox not just its w+h? todo
pub fn line_in_viewport((x1,y1):(f64,f64),(x2,y2):(f64,f64),bbox:BBox)->bool{
    let w=bbox.width();
    let h=bbox.height();

    // if at least one of these points is within the viewport
    ((0.<x1&&x1<w)&&(0.<y1&&y1<h))
        ||((0.<x2&&x2<w)&&(0.<y2&&y2<h))
        // neither is within the viewport
        ||intersection(((x1,y1),(x2,y2)),((0.,0.),(0.,h))) // left
            .filter(|&(_,i_y)|0.<i_y&&i_y<h)
            // bottom
            .or_else(||intersection(((x1,y1),(x2,y2)),((0.,0.),(w,0.)))
                .filter(|&(i_x,_)|0.<i_x&&i_x<w)
            )
            // right
            .or_else(||intersection(((x1,y1),(x2,y2)),((w,0.),(w,h)))
                .filter(|&(_,i_y)|0.<i_y&&i_y<h)
            )
            // top
            .or_else(||intersection(((x1,y1),(x2,y2)),((0.,h),(w,h)))
                .filter(|&(i_x,_)|0.<i_x&&i_x<w)
            )
            .is_some()
}

fn intersection(((x0,y0),(x1,y1)):((f64,f64),(f64,f64)),((v0,w0),(v1,w1)):((f64,f64),(f64,f64)))->Option<(f64,f64)>{
    // https://www.desmos.com/calculator/muv0rcyjad
    let x=(
        (x1-x0)*(y0-w0)*(v1-v0)
       +v0*(w1-w0)*(x1-x0)
       -x0*(v1-v0)*(y1-y0)
    )/(
        (w1-w0)*(x1-x0)
       -(v1-v0)*(y1-y0)
    );
    let y=(
        (x-x0)*(y1-y0)
    )/(x1-x0)+y0;
    if (x0<x&&x<x1)||(x1<x&&x<x0){
        Some((x,y))
    }else{
        None
    }
}

pub fn direction(a:(f64,f64),b:(f64,f64))->i8{
    // upwards or the left
    if a.1<b.1||(a.1==b.1 && a.0>b.0){
        1
    }else{ // downwards or the right
        -1
    }
}
