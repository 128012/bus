# turns SVG into a text file which is easy enough to read in JS
import xml.etree.ElementTree as ET
from sys import stdin
import math

def px_to_mm(px):
    return float(px)*0.264583
def css(style,property_to_find):
    for kv in style.split(";"):
        if kv.startswith(property_to_find+":"):
            return kv[len(property_to_find)+1:]
def get_property(l,property_to_get):
    found=[]
    if l.get(property_to_get):
        found.append(l.get(property_to_get))
    if l.get("style"):
        in_css=css(l.get("style"),property_to_get)
        if in_css:
            found.append(in_css)
    return found

# returns the swatch name of this element
# and filling in the other parameters of this swatch if
# possible - thickness and fill
def swatch_of(l):
    def get_swatch_name(url): # url(#x) -> x, following an xlink:href
        if url.startswith("url(#") and url[-1]==")":
            id_from_url=url[5:-1]
            if id_from_url in gradient_ids_to_swatches:
                return gradient_ids_to_swatches[id_from_url]
            elif id_from_url in colours:
                return id_from_url
    def get_swatch_value(url):
        if url.startswith("url(#") and url[-1]==")":
            swatch_name=get_swatch_name(url)
            if swatch_name:
                if swatch_name in colours:
                    colours[swatch_name]
        else: # it's not a url: it's a colour
            return url

    strokes=get_property(l,"stroke")

    swatch=None
    for stroke in strokes: # find one that exists
        swatch=get_swatch_name(stroke)
        if swatch:
            break

    # fill swatches
    fills=get_property(l,"fill")
    for fill in fills:
        fill_swatch=get_swatch_name(fill)
        if fill_swatch:
            break

    if fill_swatch=="road":
        swatch="road"
    elif fill_swatch=="place":
        swatch="place"
    elif fill_swatch=="green":
        swatch="green"
    # it wasn't one of those fill swatches
    elif swatch and len(colours[swatch])==1: # add fill and thickness
        fill_urls=get_property(l,"fill")

        fill_colour=None
        for fill_url in fill_urls:
            fill_colour=get_swatch_value(fill_url)
            if fill_colour:
                if len(fill_colour)==7: #no aa
                    opacity=get_property(l,"fill-opacity")
                    if len(opacity):
                        hex_opacity=hex(int(opacity[-1]))[2:]
                        if len(hex_opacity)==1:
                            hex_opacity="0"+hex_opacity
                        fill_colour+=hex_opacity
                break

        colours[swatch].append(fill_colour or "transparent")

        thickness=get_property(l,"stroke-width")
        if len(thickness):
            colours[swatch].append(thickness[-1])
    else: # there wasn't even a stroke swatch
        if "#fdfdda" in fills:
            swatch="bg"

    return swatch or "x"

inkscape_objects={}
for line in stdin:
    bits=line.split(",")
    inkscape_objects[bits[0]]=[
        bits[1],
        bits[2],
        bits[3],
        bits[4]
    ]

gradient_ids_to_swatches={}
colours={
    "road":["transparent","#e2e2e2"],
    "place":["transparent","#393939"],
    "green":["transparent","#a1e3aa"],
    "bg":["transparent","#fdfdda"],
    "x":["black","transparent","1"],
    "stop":["black","white","0.926"]
}
objects=[]
tree=ET.parse("map/8map.svg")

for linear_gradient in tree.findall(
    ".//{http://www.w3.org/2000/svg}linearGradient"
):
    stops=linear_gradient.findall(".//{http://www.w3.org/2000/svg}stop")
    gradient_id=linear_gradient.get("id")

    href=linear_gradient.get(
        "{http://www.w3.org/1999/xlink}href"
    )

    if href:
        gradient_ids_to_swatches[gradient_id]=href[1:]
    elif len(stops)==1 and not gradient_id in colours:
        colours[gradient_id]=[get_property(stops[0],"stop-color")[-1]]

#print("\n".join(colours))

#rect>#x:30,20:20x20
for rect_l in tree.findall(
    ".//{http://www.w3.org/2000/svg}rect"
):
    swatch=swatch_of(rect_l)

    text_id=rect_l.get("id")

    [x,y]=[px_to_mm(px) for px in inkscape_objects[text_id][0:2]]

    w=rect_l.get("width")
    h=rect_l.get("height")

    transformation_properties=get_property(rect_l,"transform")
    transformations=[]
    for transformation in transformation_properties:
        if transformation.startswith("rotate(") and transformation[-1]==")":
            rotation=float(transformation[7:-1])

            if rotation<90:
                x+=float(h)*math.sin(rotation*math.pi/180)
                transformations.append(str(rotation)+"deg")
    any_transformations=" ".join(transformations)+":" if len(transformations) else ""

    objects.append(f"rect>#{swatch}:{x},{y}:{any_transformations}{w}x{h}")

#path>#x:30,0░40,50
for path_l in tree.findall(
    ".//{http://www.w3.org/2000/svg}path"
):
    swatch=swatch_of(path_l)

    widths=get_property(path_l,"stroke-width")
    # choose css if it is a choice
    width=widths[-1] if len(widths) else ".5"

    d=path_l.attrib["d"]
    objects.append(f"path>#{swatch}:{d}")

for circle_l in tree.findall(
    ".//{http://www.w3.org/2000/svg}circle"
)+tree.findall(
    ".//{http://www.w3.org/2000/svg}ellipse"
):
    r=circle_l.get("r") or circle_l.get("rx")

    reverse="#"
    swatch=None

    if 2<float(r)<2.5:
        swatch="stop"
    elif circle_l.get("data-service-code"):
        if circle_l.get("data-service-code").lower() in colours:
            swatch=circle_l.get("data-service-code").lower()
            reverse="-"

    swatch=swatch or swatch_of(circle_l)

    thickness_until_bb=float(colours[swatch][2])/2 if len(colours[swatch])==3 else 0

    xy=inkscape_objects[circle_l.get("id")][0:2]
    pos=",".join([str(px_to_mm(px)+float(r)+thickness_until_bb) for px in xy])

    circle_id=circle_l.get("id")

    also=[]

    parent=tree.find(f".//{{http://www.w3.org/2000/svg}}circle[@id='{circle_id}']/..")
    if parent and not parent.tag=="{http://www.w3.org/2000/svg}a":
        parent=tree.find(f".//{{http://www.w3.org/2000/svg}}circle[@id='{circle_id}']/../..")
    if parent and parent.tag=="{http://www.w3.org/2000/svg}a":
        href=parent.get("href") or parent.get("{http://www.w3.org/1999/xlink}href")
        if href.startswith("file://"):
            href=href[7:]

        if href.startswith("/service"):
            also.append("->"+href)

    maybe_also=":".join(also)+":" if len(also) else ""

    objects.append(f"circle>{reverse}{swatch}:{pos}:{maybe_also}r{r}")

#text>#y:20,30:Snail
for text_l in tree.findall(
    ".//{http://www.w3.org/2000/svg}text"
):
    text="\\n".join(text_l.itertext())
    if len(text):
        text_id=text_l.attrib["id"]

        if text_id in inkscape_objects:
            [top_x,left_y,w,h]=[px_to_mm(px) for px in inkscape_objects[text_id]]
            x=top_x+w/2
            y=left_y+h/2

        size=get_property(text_l,"font-size")[-1]
        if size[-2:]=="px":
            size=str(px_to_mm(size[:-2]))+"mm"

        transformations=[]

        transformation_properties=get_property(text_l,"transform")
        for transformation in transformation_properties:
            if transformation.startswith("rotate(") and transformation[-1]==")":
                transformations.append(transformation[7:-1]+"deg")

        parent=tree.find(f".//{{http://www.w3.org/2000/svg}}text[@id='{text_id}']/..")
        if parent.tag=="{http://www.w3.org/2000/svg}a":
            href=parent.get("href") or parent.get("{http://www.w3.org/1999/xlink}href")
            if href.startswith("file://"):
                href=href[7:]

            if href.startswith("/stop"):
                transformations.append("->"+href)
                transformations.append(str(w)+"x"+str(h));

        transformations=":".join(transformations)
        if len(transformations):
            transformations+=":"

        objects.append(f"text>#x:{x},{y}:@{size} montserrat:{transformations}{text}")


print("\n".join(
    [f"#{tag}>{':'.join(attributes)}" for tag,attributes in colours.items()]
))
print("\n".join(objects))

