// not validating numbers

const GML:&str="http://www.opengis.net/gml/3.2";
const XLINK:&str="http://www.w3.org/1999/xlink";

use std::collections::HashMap;

fn main() {
	let gml=std::fs::read_to_string(
		"OSOpenRoads_SP.gml"
	).unwrap();
	let xml=roxmltree::Document::parse(&gml).unwrap();

	println!("The XML is finally parsed.\n");

	let mut roads=HashMap::new();
	let mut nodes:HashMap<String,(usize,Vec<usize>)>=HashMap::new();

	for maybe_feature_member in xml.root_element().children(){
		if maybe_feature_member.tag_name().name()=="featureMember"{
			let feature_member=maybe_feature_member;
			// It is actually a featureMember
			// does it have an element child?
			if let Some(l)=feature_member.first_element_child(){
				match l.tag_name().name(){
					"RoadLink"=>{
						if let Some((id,road))=Road::new(l){
							roads.insert(id,road);
						}
					},
					"RoadNode"=>{
						if let Some(id)=l.attribute((GML,"id")){
							nodes.insert(id.to_string(),(0,Vec::new()));
						}else{
							println!("A RoadNode had no id attribute.");
						}
					}
					_=>() // MotorwayJunction and possibly even more
				}
			}else{
				println!("A featureMember had no element children.")
			}
		}
	}

	let mut next_node_to_create=nodes.len();

	println!("\nThere are {} RoadLinks and {} RoadNodes.",roads.len(),nodes.len());

	println!("The XML is finished with; all the relevant features have been read. About to see \
		whether hrefs and ids are valid.\n");

	// get rid of roads with a start href which points at no nodes
	roads.retain(|_,road|nodes.get(&road.start).is_some());

	// give each node a number
	for (i,(node_i,_)) in nodes.values_mut().enumerate(){
		*node_i=i;
	}

	// give each road a line number; indices won't change after here
	for ((_,road),i) in roads.iter_mut().zip(1..){
		road.i=i;
	}

	// give each node the indices of its roads
	for (_,road) in &mut roads{
		// I got rid of all the roads disconnected at their start
		nodes.get_mut(&road.start).unwrap().1.push(road.i);
		// End node is optional I think
		if let Some(end)=&road.end{
			if let Some((_,end_node_roads))=nodes.get_mut(end){
				end_node_roads.push(road.i);
			}else{
				road.end.take();
			}
		}
	};

	println!("\nHaving given each node the indices of its roads, now producing the text file.");

	let mut lines=roads.into_iter()
		.map(|(_,road)|{
			let (start_node,start_roads)=nodes.get(&road.start).unwrap(); // I know it exists
			let end_node=road.end.as_ref()
				.map(|end_node_id|nodes.get(end_node_id).unwrap());
			
			(road.i,format!(
				"{i}:{name}:{length}:{line}:{start_node}:{start_roads}:{end_node}:{end_roads}",
				i=road.i,
				name=road.name.as_ref().map(|x|x.as_str()).unwrap_or(""),
				length=road.length,
				line=road.line.iter()
					.map(|(x,y)|format!("{},{}",x,y))
					.collect::<Vec<_>>()
					.join(" "),
				start_node=start_node,
				start_roads=start_roads.into_iter()
					.filter(|&i|*i!=road.i) // not this same road
					.map(|x|x.to_string())
					.collect::<Vec<_>>()
					.join(" "),
				end_node=end_node.as_ref()
					.map(|(i,_)|i.to_string())
					.unwrap_or_else(||{
						next_node_to_create+=1;
						next_node_to_create.to_string()
					}),
				end_roads=end_node.as_ref()
					.map(|(_,roads)|roads
						.into_iter()
						.filter(|&i|*i!=road.i) // not this same road
						.map(|x|x.to_string())
						.collect::<Vec<_>>()
						.join(" ")
					)
					.unwrap_or_else(||String::new())
			))
		})
		.collect::<Vec<_>>();
	lines.sort_unstable_by_key(|(i,_)|*i);
	let lines=lines.into_iter().map(|(_,line)|line).collect::<Vec<_>>();

	let mut file="i:name:length:line:start node i:start road line numbers:end node i:end road line numbers\n".to_string();
	file.push_str(&lines.join("\n"));

	std::fs::write("out.txt",file).expect("Error writing file");
}

struct Road{
	// start is mandatory
	start:String,
	// but end is not
	end:Option<String>,
	// x y
	line:Vec<(String,String)>,
	length:String,
	// name2 isn't used in the data I am using
	name:Option<String>,
	i:usize
}
impl Road{
	fn new(l:roxmltree::Node)->Option<(String,Self)>{Some((
		l.attribute((GML,"id"))
			.or_else(||{
				println!("A RoadLink had no id");
				None
			})?.to_string(),
		Self{
		// optional
		start:l.children()
			.find(|x|x.tag_name().name()=="startNode")
			.and_then(|l|l.attribute((XLINK,"href"))
				.or_else(||{
					println!("There exists a startNode with no href.");
					None
				})
			)?.chars().skip(1).collect::<String>(), // remove hash
		end:l.children()
			.find(|x|x.tag_name().name()=="endNode")
			.and_then(|l|l.attribute((XLINK,"href"))
				.or_else(||{
					println!("There exists an endNode with no href.");
					None
				})
			)
			.map(|x|x.chars().skip(1).collect::<String>()), // remove hash

		line:if let Some(centreline_geometry)=l.children()
			.find(|x|x.tag_name().name()=="centrelineGeometry"){
			if let Some(maybe_line_string)=centreline_geometry.first_element_child(){
				if maybe_line_string.tag_name().name()=="LineString"{
					let line_string=maybe_line_string;
					if let Some(maybe_pos_list)=line_string.first_element_child(){
						if maybe_pos_list.tag_name().name()=="posList"{
							let pos_list=maybe_pos_list;
							if let Some(coords)=pos_list.text(){
								let numbers=coords.split_whitespace()
									.map(|x|x.to_string())
									.collect::<Vec<_>>();
								// array_chunks would be better here
								let coords=numbers.chunks_exact(2);
								if coords.remainder().is_empty(){
									// an even number of numbers
									Some(coords.map(|xy|(
										xy[0].to_string(),
										xy[1].to_string()
									)).collect::<Vec<_>>())
								}else{
									println!("Odd number of numbers \
										in posList");
									None
								}
							}else{
								println!("A posList wasn't text");
								None
							}
						}else{
							println!("A posList wasn't the first element \
								child of a LineString");
							None
						}
					}else{
						println!("A LineString had no element children.");
						None
					}
				}else{
					println!("The first element child of a centrelineGeometry was \
						not a LineString");
					None
				}
			}else{
				println!("A centrelineGeometry didn't have any element children.");
				None
			}
		}else{
			println!("A RoadLink didn't have a centrelineGeometry.");
			None
		}?,

		// has to be in metres according to OS
		length:if let Some(l)=l.children()
			.find(|x|x.tag_name().name()=="length"){
			if let Some(length)=l.text(){
				// supposed to be f64
				/*match length.parse::<f32>(){
					Ok(length)=>{
						// not an integer for my information
						if length.floor()!=length{
							println!("A length was not an integer")
						}
						Some(length)
					},
					Err(e)=>{
						println!("Invalid length: {}",e);
						None
					}
				}*/Some(length)
			}else{
				println!("A length wasn't text");
				None
			}
		}else{
			println!("A RoadLink had no length element.");
			None
		}?.into(),

		name:if let Some(l)=l.children()
                        .find(|x|x.tag_name().name()=="name1"){
			l.text()
				.or_else(||{
					println!("A name1 wasn't text");
					None
				})
                }else{ // this is fine
                        None
                }.map(|x|x.to_string()),

		i:0 // correct later
	}))}
}
