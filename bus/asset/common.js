let common={
    stop(atco,xPos,yPos,popup,href){
        var x=new XMLHttpRequest();
        x.open("get","/stop/"+atco+".json?live");

        this.spin(xPos,yPos);

        x.onreadystatechange=()=>{
            if(x.readyState===4){
                this.spin();
                if(x.status===200){
                    var json=JSON.parse(x.responseText);
                    var stops=json.stops||[json]; // stop or group

                    let [liveText,liveServices]=stops[0].live
                        ?(()=>{
                            var live={};

                            stops.forEach(function(stop){
                                stop.live.forEach(function(visit){
                                    // visit===["2B","Kidlington","11:44","#00729f"]
                                    // code - if no entry in live for this code
                                    if(!live[visit[0]]){live[visit[0]]={dests:{},colour:""};}
                                    // colour - set the service colour for all directions of this service to this
                                    visit[3]&&(live[visit[0]].colour=visit[3]);
                                    // dest
                                    if(!live[visit[0]].dests[visit[1]]){live[visit[0]].dests[visit[1]]=[];}
                                    live[visit[0]].dests[visit[1]].push(visit[2]);
                                });
                            });

                            return [
                                '<ul class="live">'+Object.keys(live)
                                    .map(function(code){return[code,live[code]];})
                                    //.sort(([_,aLive],[_,bLive])=>{
                                    //  Object.keys(aLive.dests)
                                    //      .map(dest=>aLive.dests[dest]
                                    //      .max()
                                    .map(([code,service])=>Object.keys(service.dests)
                                        .map(function(dest){return [dest,service.dests[dest]];})
                                        .map(([dest,visits])=>"<li"
                                            +(service.colour
                                                ?' style="background-color:'+
                                                    service.colour+';" class="'+
                                                    this.contrast(service.colour)+'"'
                                                :""
                                            )+'><a href="/service/'+code+'"><code class="small-service-code border">'+
                                                code+"</code></a>"+
                                                '<div class="times"><h2>'+visits[0]+
                                                "</h2>"+(visits.length>1
                                                    ?"<p>then "+visits.slice(1)
                                                        .join(", ")
                                                    :""
                                                )+'</div><h2 class="dest">'+
                                                dest+"</h2></li>"
                                        )
                                        .join("")
                                    )
                                    .join("")+"</ul>",
                                Object.keys(live)
                            ];
                        })()
                        :["<p>live times not available for some reason.</p>",{}];

                    var otherServices=stops.map(stop=>stop.servedBy)
                        .reduce( // flat
                            (acc,servicesForThisStop)=>acc.concat(servicesForThisStop),
                            []
                        )
                        .sort((a,b)=>a[0]-b[0])
                        .map(code=>code.join("/"))
                        .filter((code,i,self)=>self.indexOf(code)===i)
                        .filter(code=>liveServices.indexOf(code.split("/")[0])<0)
                        .map(function(code){
                            var [code,colour]=code.split("/");
                            return '<li><a href="/service/'+code+'"><code class="small-service-code"'+(colour
                                ?' style="border-color:'+colour+';"'
                                :""
                            )+'>'+code+"</code></a></li>";
                        })
                        .join("");

                    let popupL=popup(
                        '<h1><a href="/stop/'+atco+'">'+(json.name||json.names[0])+'</a></h1>'+stops
                            .map(stop=>stop.street)
                            .filter((street,i,streets)=>street&&streets.indexOf(street)===i)
                            .map(
                                street=>'<code class="street">'+street+'</code>'
                            ).join(" ")+liveText+(otherServices
                                ?'<ul class="services">'+otherServices+"</ul>"
                                :""
                            )
                        );
                    Array.prototype.forEach.call(
                        popupL.querySelectorAll('a[href^="/service"]'),
                        a=>a.onclick=e=>{
                            let color=a.parentNode.style.backgroundColor;
                            if(!color){
                                let codeL=a.querySelector("code.small-service-code");
                                codeL&&(color=codeL.style.borderColor);
                            }

                            href(
                                a.pathname+a.search,
                                {
                                    x:e.x,y:e.y,
                                    colour:color||"#ccc"
                                }
                            );

                            e.preventDefault();
                            return false;
                        }
                    );
                }
            }
        };
        x.send();
    },

    spin(x,y){
        let spinner=document.getElementById("stopSpin");

        if(typeof x!=="undefined"){
            spinner.style.left=x+"px";
            spinner.style.top=y+"px";
            spinner.style.display="block";
            spinner.classList.add("loading");
        }else{
            spinner.classList.remove("loading");
        }
    },
    dot(serviceL,x,y){
        let colour=serviceL.getAttribute("data-colour");
        if(typeof x!=="undefined"){
            let l=document.createElement("div");

            l.style.left=x+"px";
            l.style.top=y+"px";
            l.style.backgroundColor=colour;
            serviceL.appendChild(l);

            document.body.offsetWidth;
            l.classList.add("serviceDot");
            l.classList.add("play");

            l.addEventListener("animationend",()=>{
	        if(serviceL.classList.contains("reverse")){
		    serviceL.remove();
		}else{
		    serviceL.style.backgroundColor=colour;
		    serviceL.classList.add("dot-no-longer-needed"); /* so that scrollbar can be seen */
		}
	    });

        }else{
            serviceL.classList.remove("dot-no-longer-needed");
            serviceL.style.backgroundColor="transparent";
            serviceL.classList.add("reverse");
        }
    },
    
    closeService(pane){
        this.dot(pane);
    },

    service(code,colour,x,y,onclose){
        let pane=document.createElement("section");

        pane.setAttribute("data-colour",colour);
        pane.setAttribute("data-code",code);

        pane.classList.add(this.contrast(colour),"service");

        pane.innerHTML='<article><header><a href="/service/'+code+'" class="no-underline"><code class="like-h1">'+
            code+"</code></a>"+'<h1>Loading...</h1></header></article>'+
            '<button class="close bg">Back</button>';
        let articleL=pane.firstElementChild;

        this.dot(pane,x,y);

        document.getElementById("service").appendChild(pane);


        pane.getElementsByClassName("close")[0].addEventListener(
            "click",
            (onclose&&onclose.bind(null,pane))||this.closeService.bind(this,pane)
        );

        let err=err=>articleL.innerHTML=err;

        this.serviceJSON(
            code,
            this.ymd(new Date()),
            json=>{
                let today=new Date();
                articleL.innerHTML='<header><code class="like-h1">'
                    +json.code+"</code>"+"<h1>"+json.description+"</h1></header>"+
                    '<div class="routeMap"></div>'+
                    /*'<p class="small-text bg">'+
                    "Don't take this computer-generated map too seriously. "+
                    "It should give you an idea of the shape of the route, "+
                    "but the path the bus takes between stops may be different in real life. "+
                    "Stops outside the vicinity of Oxford "+
                    "and route sections connecting to them are missed out.</p>"+*/
                    '<ul class="date">'+
                    this.serviceDateOption(today.getTime()-(3600*1000*24),true)+ // yesterday
                    this.serviceDateOption(today,false)+ // today
                    Array(13).fill(0) // 13 more days
                        .map(function(_,i){return i+1;})
                        .map(function(n){return today.getTime()+(n*3600*1000*24);})
                        .map(date=>this.serviceDateOption(date,true))
                        .join("")+"</ul>"+
                    // not in a div with the label, to be adjacent to the matrices
                    '<input id="showAllStops" type="checkbox"/>'+
                    '<label for="showAllStops">Show line diagrams and all stops</label>'+
                    '<div class="centre"></div>';

                let dates=pane.getElementsByClassName("date")[0].children;
                Array.prototype.forEach.call(dates,
                    li=>li.onclick=()=>{
                        let date=li.getAttribute("data-date")
                        this.serviceJSON(
                            code,
                            date,
                            json=>this.timetable(articleL,json,date),
                            err
                        );
                       // this.timetable(code,li.getAttribute("data-date"),true);
                        Array.prototype.forEach.call(dates,
                            function(li){li.classList.add("unimportant");}
                        );
                        li.classList.remove("unimportant");
                    }
                );

                if(typeof L!=="undefined"){
                    const tiles="https://tile.openstreetmap.org/{z}/{x}/{y}.png";
                    const attrib='Lines from timetables (so may not show actual route) and lines leaving the bounds are not shown; &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>';
                    const bounds=[[51.649493,-1.332672],[51.831657,-1.117897]];

                    const transformed=json.points.map(two=>two.map(xy=>{
                        let x=xy[0];
                        let y=xy[1];

                        let c_at_lat_m=24737610.51;// ( 2 pi r cos(lat) ) *1000
                        let lng=-1.332672+(360*x)/c_at_lat_m;

                        let c_m=40030173.59;// 2 pi r
                        let lat=51.831657-(360*y)/c_m;
                        return [lat,lng]
                    }));

                    let map=L.map(pane.getElementsByClassName("routeMap")[0]).addLayer(L.tileLayer(tiles,{attribution:attrib}));

                    L.rectangle(bounds,{color:"black",fill:false}).addTo(map);
                    // on top of rectangle
                    L.polyline(transformed,{color:"white",weight:7}).addTo(map);
                    let path=L.polyline(transformed,{color:json.colour||"black",weight:5}).addTo(map);
                    map.fitBounds(path.getBounds());
                    //doesn't work with popups
                    //map.setMaxBounds(map.getBounds());
                    //map.setMinZoom(map.getZoom());

                    Object.keys(json.stops).forEach(
                        atco=>L.circleMarker(
                            json.stops[atco],
                            {radius:8.75,color:"black",fillColor:"white",fillOpacity:1,weight:3.5}
                        )
                            .on("click",e=>this.stop(
                                atco,
                                e.originalEvent.clientX,
                                e.originalEvent.clientY,
                                html=>L.popup({className:"stop-popup active"})
                                    .setContent(html)
                                    .setLatLng(json.stops[atco])
                                    .openOn(map)
                                    .getElement(),
                                (href,info)=>this.service(href.slice(9),info.colour,info.x,info.y,onclose)
                            ))
                            .addTo(map)
                    );

                    am=map;
                }
                this.timetable(articleL,json,this.ymd(today));
            },
            err
        );
        return close;
    },

    serviceJSON(code,date,then,err){
        var x=new XMLHttpRequest();
        x.open("GET","/service/"+code+".json"+(date
            ?"?on="+date
            :""
        ),true);
        x.onreadystatechange=()=>{
            if(x.readyState===4){
                if(x.status===200){
                    try{
                        var json=JSON.parse(x.responseText);
                    }catch(_){
                        err("Invalid response from server. Try again later.");
                        return;
                    }
                    then(json);
                }else if(x.status===404){
                    err("<h1>This service either does not exist or is not currently running.</h1>");
                }else{
                    err("<h1>There was an error. The code was "+x.status+".</h1>");
                }
            }
        };
        x.send();
    },


    timetable(pane,json,date){
        // delete any matrices, and delete the message about there being no journeys
        // there are not necesssarily any matrices for the currently selected day
        let centre_l=pane.getElementsByClassName("centre")[0];
        centre_l&&(centre_l.innerHTML="");

        // that is the div.centre
        pane.lastElementChild.innerHTML+=json.matrices.length
            ?('<div class="matrices">'+
                json.matrices.map(function(matrix,matrixI){
                    return '<div class="matrix"><h2>'+matrix.name
                    +'</h2><div class="matrix-scroll"><div class="matrix-margin"><table>'+
                        matrix.rows.map(function(row,i){
                            return '<tr class="timing-status--'+row.timingStatus+'">'+
                                "<th>"+((row.name&&row.atco)
                                    ?'<span class="light">'+row.locality+'</span> <a href="/stop/'+row.atco+'">'+row.name+"</a>"
                                    :row.name||row.atco
                                )+'</th>'+(i?'':'<td class="diagram-cell" rowspan="'
                                +matrix.rows.length+'"><img src="/service/'
                                +json.code+'/'+matrixI+'/diagram.svg'+
                                (date
                                    ?"?on="+date
                                    :""
                                )+'"/></td>')+
                                row.visits.map(function(visit,i){
                                    if(visit==="*"){
                                        return "";
                                    }else if(visit.startsWith("-")){
                                        let operator=visit.slice(2); // after |
                                        return '<td'+(operator?(' class="operator--'+operator)
                                            +-'"':'')+'>-</td>';
                                    }else if(visit.includes("m*")){
                                        let asRepetition=visit.match(/^(\d+)m\*(\d+)$/);
                                        if(asRepetition&&asRepetition[2]){
                                            return '<td rowspan="'+matrix.rows.length+'">'+
                                                "then every "+asRepetition[1]+" minutes ("
                                                    +asRepetition[2]+" times) until"+
                                            "</td>";
                                        }else{console.log(visit);return "<td>"+times+"</td>";}
                                    }else{
                                        // arr-for which is alphanumeric|operator which
                                        // is alphanumeric, then irrelevant
                                        let times=visit.match(/^(\d\d\:\d\d)\-(\w*)\|(\w*)/);
                                        if(times){
                                            if(times[3]){
                                                return '<td class="operator--'+times[3]+'">'
                                                    +times[1]+(visit.endsWith("|s")
                                                        ?"↓"
                                                        :""
                                                    )+(times[2]
                                                        ?'<span class="for">for '+times[2]+'</span>'
                                                        :''
                                                    )+"</td>";
                                            }else{
                                                return "<td>-</td>";
                                            }
                                        }else{console.log(times);return "<td>"+times+"</td>";}
                                    }
                                }).join("")+"</tr>";
                        }).join("")+"</table></div></div></div>"
                }).join("")+'</div>'
            )
            :'<h1 id="noJourneys">There are no journeys on this day.</h1>';
    },

    serviceDateOption(d,unimportant){
        let date=new Date(d);

        let saturday=date.getDay()===6;
        let sunday=date.getDay()===0;
        let classList=[unimportant?"unimportant":false,saturday?"saturday":false,sunday?"sunday":false]
            .filter(x=>x);

        return '<li data-date="'+this.ymd(date)+'"'
            +(classList.length
                ?' class="'+classList.join(" ")+'"'
                :""
            )+">"+date.getDate()+"/"+(date.getMonth()+1)+"</li>";
    },
    ymd(date){
        return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
    },

    search(term){
        var searchSection=document.getElementById("search");
        var list=document.getElementById("searchResults");

        var x=new XMLHttpRequest();
        x.open("get","/search?format=json&q="+encodeURIComponent(term),true);
        x.onreadystatechange=function(){
            if(x.readyState===4){
                if(x.status===200){
                    try{
                        var json=JSON.parse(x.responseText);
                    }catch(_){
                        alert("Invalid response from server. Try again later.")
                    }

                    var noResults=document.getElementById("noResults");

                    if(json.length){
                        noResults&&searchSection.removeChild(noResults);

                        list.innerHTML=json
                            .map(result=>{
                                if(result.code){
                                    return '<li><a href="/service/'+result.code+
                                        '"><code class="small-service-code"'+(
                                            result.colour?' style="border-color:'+result.colour+'"':''
                                        )+'>'+result.code+'</code> <span>'+
                                        result.description+'</span></a></li>';
                                }else{
                                    var stops=result.stops||[result];
                                    let services=stops
                                        .map(stop=>stop.servedBy)
                                        .reduce( // flat
                                            (acc,servicesForThisStop)=>acc.concat(servicesForThisStop),
                                            []
                                        );

                                    return '<li><a href="/stop/'+
                                        stops.map(stop=>stop.atco).join("+")+'">'+(result.name||result.names[0])+'</a>'
                                           +stops
                                            .map(stop=>stop.street)
                                            // dedup
                                            .filter((street,i,streets)=>street&&streets.indexOf(street)===i)
                                            .map(
                                                street=>'<code class="street">'+street+'</code>'
                                            ).join(" ")
                                           +'<span>in '+stops[0].locality+'</span>'
                                           +services.sort(([aCode],[bCode])=>aCode<bCode
                                                ?-1
                                                :aCode>bCode
                                                    ?1
                                                    :0
                                            )
                                            .filter(([code],i,services)=>services[i+1]?services[i+1][0]!==code:true)
                                            .map(([code,colour])=>'<a href="/service/'+code+'"><code class="small-service-code"'+
                                                    (colour?' style="border-color:'+colour+'"':'')+
                                                    '>'+code+'</code></a>'
                                                )
                                                .join("")+
                                    '</li>';
                                }
                            })
                            .join("");
                        list.querySelectorAll('a[href^="/service"]')
                            .forEach(a=>{
                                var code=a.getElementsByTagName("code")[0];
                                var b=code.getBoundingClientRect();
                                a.onclick=e=>service(
                                    a.pathname.slice(9),
                                    code.style.borderColor||"#ccc",
                                    b.left+20,
                                    b.top+2
                                )
                            });
                    }else{
                        if(!noResults){
                            list.innerHTML="";
                            var l=document.createElement("p");
                            l.innerHTML="No results";
                            l.id="noResults";
                            searchSection.appendChild(l);
                        }
                    }
                }else{
                    alert("There was an error. The code was "+x.status+".")
                }
            }
        };
        x.send();
    },

    contrast(s){
        var rgb=s.match(/\((\d{1,3})\,\s*(\d{1,3})\,\s*(\d{1,3})/);
        var hex=s.match(/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i);
        var shortHex=s.match(/^#?([a-f\d])([a-f\d])([a-f\d])$/i);
        if(rgb&&rgb.length===4){
            var r=rgb[1]/255;
            var g=rgb[2]/255;
            var b=rgb[3]/255;
        }else if(hex&&hex.length===4){
            var r=parseInt(hex[1],16)/255;
            var g=parseInt(hex[2],16)/255;
            var b=parseInt(hex[3],16)/255;
        }else if(shortHex&&shortHex.length===4){
            var r=parseInt(shortHex[1]+shortHex[1],16)/255;
            var g=parseInt(shortHex[2]+shortHex[2],16)/255;
            var b=parseInt(shortHex[3]+shortHex[3],16)/255;
        }else{
            return "black";
        }
        return ((Math.max(r,g,b)+Math.min(r,g,b))/2>.5
            ?"black"
            :"white"
        );
    }
};
