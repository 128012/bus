var clicked=[0,0];
var map;
//window.onerror=alert;

function initMap(){
	map=L.map("map",{
		minZoom:1,
		maxZoom:15,
		zoomSnap:0.2,
		zoom:1,
		crs:L.CRS.Simple
	}).addEventListener("click",stop);

	var frame=document.getElementById("svgFrame");
	var svg=frame.contentDocument.documentElement;

	let [w,h]=svg.getAttribute("viewBox").split(" ").slice(2).map(Number);

	var bounds=new L.LatLngBounds(
		map.unproject([0,h],3),
		map.unproject([w,0],3)
	);

	frame.style.display="none";
	document.getElementById("map").style.display="block";

	L.svgOverlay(svg,bounds,{interactive:true}).addTo(map);

	map.fitBounds(bounds);
}

function link(){
	var svg=document.getElementById("svgFrame").contentDocument;

	svg.querySelectorAll("use")
		.forEach(use=>use.onclick=e=>{
			let g=document.querySelector(use.getAttribute("xlink:href"));
			let a=g.children[0];
			let code=(a.getAttribute("xlink:href")||a.getAttribute("href")).slice(9);
			let colour=svgColour(window.getComputedStyle(
				a.getElementsByTagName("circle")[0]
			).fill);
			service(code,colour,e.clientX,e.clientY);
			e.preventDefault();
			return false;
		});

	document.body.addEventListener("click",function(e){
		clicked=[e.clientX,e.clientY];
	},true);
}

function stop(e){
	e.originalEvent.preventDefault();

	var a=e.originalEvent.target;
	while(!(a.tagName.toLowerCase()==="a"&&(a.getAttribute("xlink:href")||a.getAttribute("href")).startsWith("/stop/"))){
		if(!a.parentElement){return;}
		a=a.parentElement;
	}

	var atco=(a.getAttribute("xlink:href")||a.getAttribute("href")).slice(6);

	common.stop(
		atco,
		e.originalEvent.clientX,
		e.originalEvent.clientY,
		html=>L.popup({className:"stop-popup active"})
			.setContent(html)
			.setLatLng(e.latlng)
			.openOn(map)
			.getElement(),
		(href,info)=>{
			service(href.slice(9),info.colour,info.x,info.y);
		}
	);

	return false;
}

function service(code,colour,x,y,inHistory){
	common.service(
            code,
            colour,
            x,
            y
        );

	return false;
};

window.onpopstate=e=>log(e.state)
    ?service(...e.state,true)
    :common.closeService();

function svgColour(colour){
	// if it is a link to a palette colour
        let asURL=colour.match(/^url\(\"?\#(\w+)\"?\)$/);
        if(asURL&&asURL[1]){
                let linearGradient=document.getElementById(asURL[1])||document.getElementById("svgFrame")
			.contentDocument
			.getElementById(asURL[1]);
		let stop=linearGradient.children[0];
		if(stop){
                	return getComputedStyle(stop).stopColor;
		}else{
			let to=linearGradient.getAttribute("xlink:href");
			// is to not a reference to itself
			if(to.slice(1)!==asURL[1]){
				return svgColour("url("+to+")");
			}else{
				return "white";
			}
		}
        }else{
                return colour;
        }
}

function searchSubmit(e){
	e.preventDefault();
	common.search(document.getElementById("q").value);
	return false;
}

function log(x){console.log(x);return x;};
window.onload=function(){
	document.getElementById("searchForm").onsubmit=searchSubmit;
	link();
	initMap();
}
