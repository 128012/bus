window.addEventListener("load",function(){

var atco=document.getElementById("atco").innerHTML;
var x=new XMLHttpRequest();
x.open("get","http://www.oxontime.com/pwi/departureBoard/"+atco,true);
x.addEventListener("readystatechange",function(){
    if(x.readyState===4){
        if(x.statusCode===200){
            try{
                var calls=JSON.parse(x.responseText)[atco].calls
                    .map(function(call){
                        var time=Date.parse(call.expected_arrival_time);
                        if(Number.isNaN(time)){
                            time=new Date(call.aimed_arrival_time);
                        }else{
                            time=Date(time);
                        }
                        
                        return{
                            serviceCode:call.route_code,
                            destination:call.destination,
                            time:time
                        }
                    })
                    .filter(function(call){
                        return call.serviceCode&&call.destination&&call.time
                    });
                    //.sort(function(a,b){a.time-b.time});

                var serviceCodes={};

                calls.forEach(function(call){
                    if(!serviceCodes[call.serviceCode]){
                        serviceCodes[call.serviceCode]={
                            destination:call.destination,
                            times:[]
                        }
                    }
                    serviceCodes[call.serviceCode].times.push(call.time);
                });

                var rows=[];

                Object.keys(rows).forEach(function(serviceCode){
                    rows.push({
                        serviceCode:serviceCode,
                        destination:serviceCodes[serviceCode].destination,
                        times:serviceCodes[serviceCode].times
                            .map(function(time){
                                var hours=time.getHours();
                                if((""+hours).length===1){
                                    hours=" "+hours;
                                }
                                
                                var minutes=time.getMinutes();
                                if((""+minutes).length===1){
                                    minutes=" "+minutes;
                                }
                                return hours+":"+minutes
                            })
                    });
                });

                document.getElementById("liveSection").innerHTML=rows
                    .sort(function(a,b){return a.time-b.time})
                    .map(function(row){
                        var first_eta=row.times.shift();
                        var subsequent_etas="";
                        if(row.times.length){
                            subsequent_etas="then "+row.times.join(", ");
                        }
                        return `<li>
       	                    <a href="/service/${row.serviceCode}" class="inline-service-code">{code}</a>
       	                    <div class="times">
       	                        <h2>${first_eta}</h2>
       	                        ${subsequent_etas}
       	                    </div>
       	                    <h2 class="dest">${row.destination}</h2>
       	                </li>`
                    })
                    .join("");
            }catch{
                document.getElementById("liveStatus").innerHTML="This website cannot understand what Oxontime sent as live times.";
                return;
            }
        }else{
            document.getElementById("liveStatus").innerHTML="Error getting live times";
        }
    }
});
x.send();

});
