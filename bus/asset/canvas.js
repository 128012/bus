"use strict";
//window.onerror=alert
let a,b,c;

function newMap(file){
    let colours={};
    let objects=[];
    for(let line of file.split("\n")
        .filter(line=>line.length&&!line.startsWith("//"))
    ){
        let indexOfGT=line.indexOf(">");
        let tag=line.slice(0,indexOfGT);
        let attributes=line.slice(indexOfGT+1).split(":");

        // colour
        if(tag.startsWith("#")){
            colours[tag.slice(1)]={stroke:attributes[0]};
            attributes[1]&&(colours[tag.slice(1)].fill=attributes[1]);
            attributes[2]&&(colours[tag.slice(1)].thickness=attributes[2]);
        }else{
            ({
                path:(()=>{
                    let last=attributes.pop();
                    let object={
                        tag:"path",
                        of:attributes[0].slice(1),
                    };
                    if(last[0]>="0"&&last[0]<="9"){
                        object.points=last
                            .split(" ")
                            .map(pair=>pair.split(",")
                                .map(n=>Number(n))
                            );
                    }else{
                        object.d=new Path2D(last);
                    }
                    objects.push(object);
                }),
                text:(()=>{//alert("x")
                    let text=attributes.pop();
//                    alert(text);
                    let [x,middle_y,w,h,degrees]=[null,null,null,null,0];

                    let object={
                        tag:"text",
                        text,
                        of:attributes[0].slice(1)
                    };

                    let is45=false;

                    for(let attribute of attributes.slice(1)){
                        if(attribute.startsWith("@")){
                            object.font=attribute.slice(1);
                            object.fontSize=parseFloat(object.font.slice(0,object.font.indexOf(" ")));
                        }else if(attribute.startsWith("_")){
                            object.baseline=attribute.slice(1);
                        }else if(attribute.endsWith("deg")){
                            degrees=parseFloat(attribute);
                            is45=(degrees===45||degrees===-45);
                            object.rotate=degrees*Math.PI/180;
                        }else if(attribute.startsWith("->")){
                            object.href=attribute.slice(2);
                        }else if(/^\d*(\.\d+)?\,\d*(\.\d+)?$/.test(attribute)){
                            [x,middle_y]=attribute.split(",").map(x=>+x);
                        }else if(/^\d*(\.\d+)?x\d*(\.\d+)?$/.test(attribute)){
                            [w,h]=attribute.split("x").map(x=>+x);
                        }
                    }

                    object.font=object.font||("10px monospace"&&(object.fontSize=10));
                    object.rotate=object.rotate||0;

                    object.distance=object.fontSize+2;
                    object.x=x;
                    object.y=middle_y
                        -(object.text.split("\\n").length-1)
                        *object.distance/2;

                    if(object.href){
                        let [insideW,insideH]=is45
                            ?[
                                // don't divide by 0
                                // the bb happens to be a square
                                Math.sqrt(2)*w,3
                            ]
                            :[
                                (
                                    w*Math.cos(object.rotate)-h*Math.sin(object.rotate)
                                )/(
                                    Math.cos(object.rotate)**2-Math.sin(object.rotate)**2
                                ),
                                (
                                    h*Math.cos(object.rotate)-w*Math.sin(object.rotate)
                                )/(
                                    Math.cos(object.rotate)**2-Math.sin(object.rotate)**2
                                )
                            ];

                        object.outline=new Path2D();

                        if(object.rotate){
                            // neither DOMMatrix nor SVGMatrix work properly in safari10 I think
                            let transform=document.createElementNS("http://www.w3.org/2000/svg","svg")
                                .createSVGMatrix()
                                .translate(object.x,object.y)
                                .rotate(degrees);
                            let transformedPath2D=new Path2D();
                            transformedPath2D.rect(-insideW/2,-insideH/2,insideW,insideH);
                            object.outline.addPath(transformedPath2D,transform);
                        }else{
                            object.outline.rect(object.x-w/2,object.y-h/2,insideW,insideH);
                        }
                    }

                    objects.push(object);
                }),
                circle:(()=>{
                    let of=attributes[0];

                    let object={
                        tag:"circle",
                        of:of.slice(1),
                        reversed:of[0]==="-",
                    };
                    for(let attribute of attributes.slice(1)){
                        if(attribute.startsWith("r")){
                            object.r=+attribute.slice(1)
                        }else if(attribute.startsWith("->")){
                            object.href=attribute.slice(2);
                        }else if(/^\d*(\.\d+)?\,\d*(\.\d+)?$/.test(attribute)){
                            let [x,y]=attribute.split(",").map(x=>+x);
                            object.x=+x;
                            object.y=+y;
                        }
                    }

                    if(object.href){
                        object.outline=new Path2D();
                        object.outline.arc(object.x,object.y,object.r,0,Math.PI*2);
                    }

                    objects.push(object);
                }),
                rect:(()=>{
                    let of=attributes[0].slice(1);
                    let [w,h]=attributes.pop().split("x");
                    let [x,y]=attributes[1].split(",");

                    let object={
                        tag:"rect",
                        of,
                        x:+x,y:+y,
                        w:+w,h:+h
                    };
                    
                    if(attributes[2]){
                        for(let transformation of attributes[2].split(" ")){
                            if(transformation.endsWith("deg")){
                                object.rotate=parseInt(transformation)*Math.PI/180;
                            }
                        }
                    }

                    objects.push(object);
                })
            }[tag])();
        }
    }

    objects=objects.map(object=>{
        let swatch=object.of;

        object.stroke=colours[swatch][object.reversed?"fill":"stroke"]||"transparent";
        object.fill=colours[swatch][object.reversed?"stroke":"fill"]||"transparent";
        object.thickness=colours[swatch].thickness||1;
        return object;
    });

    let maxX=Math.max(...objects.map(object=>({
        path:(()=>object.points
            ?Math.max(...object.points
                .map(pair=>pair[0])
            )
            :0
        ),
        text:(()=>object.x),
        circle:(()=>object.x+object.r),
        rect:(()=>object.x+object.w)
    }[object.tag])()));
    let maxY=Math.max(...objects.map(object=>({
        path:(()=>object.points
            ?Math.max(...object.points
                .map(pair=>pair[1])
            )
            :0
        ),
        text:(()=>object.y),
        circle:(()=>object.y+object.r),
        rect:(()=>object.y+object.h)
    }[object.tag])()));

    return {
        objects(){
            return objects;
        },
        hrefPosition(href){
            let object=objects.find(object=>object.href===href);
            return object&&[object.x,object.y];
        },
        max(){
            return [maxX,maxY];
        }
    }
}

function newCanvas(l){
    let ctx=l.getContext("2d",{alpha:false});
    c=ctx;

    let zoom=0;
    let x=0
    let y=0;

    let change={
        zoom:0,

        x:0,
        y:0,
    };
    let changed=false;
    let frameRequested=false;

    return {
        getZoom(){
            return zoom;
        },
        zoomTo(newZoom){
            changed=true;
            return zoom=newZoom;
        },
        zoom(zoomChange){
            changed=true;
            change.zoom+=zoomChange;
            return zoom+change.zoom;
        },
        getPan(){
            return [x,y];
        },
        panTo(newX,newY){
            changed=true;
            return [x=newX,y=newY];
        },
        pan(changeX,changeY){
            changed=true;
            change.x+=changeX;
            change.y+=changeY;
        },
        toMapCoordinates(clientX,clientY){
            return [
                (clientX-x)/zoom,
                (clientY-y)/zoom
            ];
        },
        fromMapCoordinates(mapX,mapY){
            return [
                mapX*zoom+x,
                mapY*zoom+y
            ]
        },
        hrefFromClick(map,clientX,clientY){
            let object=map.objects().filter(object=>object.href)
                .find(object=>ctx.isPointInPath(object.outline,clientX,clientY))

            return object&&object.href;
        },

        resize(){
            l.width=window.innerWidth;
            l.height=window.innerHeight;
        },
        fit(map){
            let [maxX,maxY]=map.max();
            zoom=Math.min(l.width/maxX,l.height/maxY);

            let [offsetX,offsetY]=map.max()
                .map(m=>m*zoom/2);
            x=l.width/2-offsetX;
            y=l.height/2-offsetY;
        },

        drawSoon(map){
            if(changed&&!frameRequested){
                frameRequested=true;
                window.requestAnimationFrame(()=>{
                    frameRequested=false;
                    this.draw(map);
                    this.drawSoon(map);
                });
            }
        },


        draw(map){
            let startTime=Date.now();

            ctx.resetTransform();
            
            ctx.fillStyle="#fdfdda";
            ctx.fillRect(0,0,l.width,l.height);

            zoom=zoom+change.zoom;
            if(!zoom){
                this.fit(map);
            }else if(zoom<0){
                zoom=0.01;
            }

            x=x+change.x;
            y=y+change.y;
            ctx.translate(x,y);
            ctx.scale(zoom,zoom);

            change={
                zoom:0,
                x:0,
                y:0
            }
            changed=false;

            for(let object of map.objects()){
                ({
                   path:(()=>{
                        ctx.strokeStyle=object.stroke;
                        ctx.fillStyle=object.fill;
                        ctx.lineWidth=object.thickness;
                        ctx.beginPath();

                        if(object.d){
                            ctx.stroke(object.d);
                            ctx.fill(object.d);
                        }else{
                            ctx.moveTo(object.points[0][0],object.points[0][1]);
                            for(let [idx,point] of object.points.entries()){
                                idx&&ctx.lineTo(point[0],point[1]);
                            }
                            ctx.stroke();
                            ctx.fill();
                        }
                    }),
                    text:(()=>{
                        ctx.save();

                        ctx.textBaseline=object.baseline||"middle";
                        ctx.textAlign="center";
                        ctx.fillStyle="black";

                        ctx.font=object.font;

                        let x=object.x;
                        let y=object.y;

                        if(object.rotate){
                            ctx.translate(object.x,object.y);
                            x=0;
                            y=0;

                            ctx.rotate(object.rotate);
                        }
                        for(let line of object.text.split("\\n")){
                            ctx.fillText(
                                line,
                                x,y
                            );
                            ctx.translate(0,object.distance);
                        }

                        ctx.restore();
                    }),
                    circle:(()=>{
                        ctx.strokeStyle=object.stroke;
                        ctx.fillStyle=object.fill;
                        ctx.lineWidth=object.thickness;
                        ctx.beginPath();

                        ctx.arc(object.x,object.y,object.r,0,2*Math.PI);
                        ctx.fill();
                        ctx.stroke();
                    }),
                    rect:(()=>{
                        ctx.strokeStyle=object.stroke;
                        ctx.fillStyle=object.fill;
                        ctx.lineWidth=object.thickness;

                        ctx.save();

                        let x=object.x;
                        let y=object.y;

                        if(object.rotate){
                            ctx.translate(object.x,object.y);
                            ctx.rotate(object.rotate);
                            x=0;
                            y=0;
                        }

                        ctx.fillRect(x,y,object.w,object.h);
                        ctx.strokeRect(x,y,object.w,object.h);

                        ctx.restore();
                    })
                }[object.tag])();
            }

            l.style.transform="";

            return Date.now()-startTime;
        }
    };
}

function log(x){console.log(x);return x;}

function closeStop(){
    document.getElementById("stop").style.display="none";
}
// closes the most recent service
function closeService(){
    common.closeService(document.getElementById("service").lastElementChild);
}

let grid="";

let state={
    service:null,
    stop:null,
    stopPosition:null
};

function init(){
    let map=newMap(grid);
    let l=document.getElementById("canvas");
    let canvas=newCanvas(l);

    window.onresize=canvas.resize;
    canvas.resize();

    function openHref(href,info={}){
        if(href.startsWith("/stop")){
            state.stopPosition=null;

            let atco=href.slice(6);
            let [x,y]=info.x
                ?[info.x,info.y]
                :(()=>{
                    state.stopPosition=map.hrefPosition(href);
                    return canvas.fromMapCoordinates(...state.stopPosition);
                })();

            state.stop=href;
            state.service=null;
            closeService();
            history.pushState(state,href,href);

            common.stop(
                atco,
                x,
                y,
                html=>{
                    let popup=document.getElementById("stop");
                    popup.innerHTML=html;
                    popup.style.opacity=0;
                    popup.style.display="block";

                    positionStop(href);

                    popup.style.opacity=1;

                    return popup;
                },
                openHref
            );
        }else if(href.startsWith("/service")){
            let serviceCode=href.slice(9);
            let colour=info.colour
                ?info.colour
                :(
                    map.objects().find(object=>object.href==href)
                        ||{fill:"#ccc"}
                ).fill;

            //common.dot(colour,info.x,info.y);
            common.service(
                serviceCode,
                colour,
                info.x,
                info.y,
                ()=>history.back()
            );
            document.getElementById("service").classList.add(
                common.contrast(colour),
                "active"
            );

            //common.timetable(serviceCode);

            state.service=href;
            history.pushState(state,href,href);
        }
    }
    function positionStop(){
        if(state.stop){
            let popup=document.getElementById("stop");

            let [x,y]=canvas.fromMapCoordinates(...state.stopPosition||map.hrefPosition(state.stop));

            let computed=window.getComputedStyle(popup);
            popup.style.transform=`translate(${x-parseFloat(computed.width)/2}px,${y}px)`;
        }
    }

    function click(x,y){
        let this_href=canvas.hrefFromClick(map,x,y);
        console.log(this_href);
        this_href
            ?openHref(this_href,{x,y})
            :closeStop();
    }

    let lastMousePanX;
    let lastMousePanY;
    let downX;
    let downY;
    function down(downE){
        lastMousePanX=downX=downE.clientX;
        lastMousePanY=downY=downE.clientY;
    }
    function up(e){
        e.target.id==="canvas"&&e.clientX===downX&&e.clientY===downY
            &&click(e.clientX,e.clientY);
    }
    function move(e){
        if(!state.service){
            if(!e.buttons){
                if(canvas.hrefFromClick(map,e.clientX,e.clientY)){
                    l.style.cursor="pointer";
                }else{
                    l.style.cursor="default";
                }
            }else if(e.buttons===1){
                let x=e.clientX-lastMousePanX;
                let y=e.clientY-lastMousePanY;
                canvas.pan(
                    x,
                    y
                );
                canvas.drawSoon(map);

                lastMousePanX=e.clientX;
                lastMousePanY=e.clientY;

                positionStop();
            }
        }
    }
    function wheel(e){
        let currentZoom=canvas.getZoom();
        if(currentZoom-e.deltaY/10>=0){
            let [x,y]=canvas.toMapCoordinates(e.clientX,e.clientY);
            zoom(-e.deltaY/10,x,y);

            let sf=(currentZoom-e.deltaY)/currentZoom;

            canvas.drawSoon(map);
        }
    }

    let touches={};
    let touchDistance;
    let touchCentre;

    function touchstart(e){
        if(!state.service){
            e.preventDefault();
            for(let i=0;i<e.changedTouches.length;i++){
                let newTouch=e.changedTouches[i];
                if(Object.keys(touches).length<2){
                    touches[newTouch.identifier]=[
                        newTouch.clientX,
                        newTouch.clientY
                    ];
                }
            }
            let positions=Object.values(touches);
            if(positions.length===2){
                touchDistance=Math.sqrt(
                     (positions[0][0]-positions[1][0])**2
                    +(positions[0][1]-positions[1][1])**2
                );
                touchCentre=[
                    (positions[0][0]+positions[1][0])/2,
                    (positions[0][1]+positions[1][1])/2
                ];
            }else{ //===1
                [downX,downY]=positions[0];
            }
        }
    }
    function touchmove(e){
        e.preventDefault();

        if(!state.service){
            let positions=Object.values(touches);
            let numberOfTouches=positions.length;

            if(numberOfTouches===2){
                for(let i=0;i<e.changedTouches.length;i++){
                    let changedTouch=e.changedTouches[i];
                    touches[changedTouch.identifier]=[
                        changedTouch.clientX,
                        changedTouch.clientY
                    ];
                }

                let newDistance=Math.sqrt(
                     (positions[0][0]-positions[1][0])**2
                    +(positions[0][1]-positions[1][1])**2
                );
                let newTouchCentre=[
                    (positions[0][0]+positions[1][0])/2,
                    (positions[0][1]+positions[1][1])/2
                ];

                let sf=newDistance/touchDistance;
                let zoomDifference=canvas.getZoom()*(sf-1);

                if(zoomDifference){
                    // old touch centre in map coordinates
                    let mapCentreCoordinates=canvas.toMapCoordinates(...touchCentre);
                    let newMapCentreCoordinates=canvas.toMapCoordinates(...newTouchCentre);

                    let zoomPointX=(mapCentreCoordinates[0]-newMapCentreCoordinates[0]*sf)/(1-sf);
                    let zoomPointY=(mapCentreCoordinates[1]-newMapCentreCoordinates[1]*sf)/(1-sf);

                    touchDistance=newDistance;

                    // the pixel in the centre of the two fingers
                    // ends up still in the centre of those fingers
                    // but in a different place now
                    zoom(zoomDifference,zoomPointX,zoomPointY);
                    canvas.drawSoon(map);
                }

                touchCentre=newTouchCentre;
            }else if(numberOfTouches===1){
                let position=positions[0];
                let newPosition=[e.changedTouches[0].clientX,e.changedTouches[0].clientY];
                touches[e.changedTouches[0].identifier]=newPosition;
                let x=newPosition[0]-position[0];
                let y=newPosition[1]-position[1];
                canvas.pan(
                    x,
                    y
                );

                canvas.drawSoon(map);

                positionStop();
            }
        }
    }
    function touchend(e){ // no longer two fingers
        if(e.target.id==="canvas"){
            e.preventDefault();
        }

        for(let i=0;i<e.changedTouches.length;i++){
            delete touches[e.changedTouches[i].identifier];
        }
    }
    function touchcancel(e){
        for(let i=0;i<e.changedTouches.length;i++){
            delete touches[e.changedTouches[i].identifier];
        }
    }

    function zoom(z,x,y){
        let zoom=canvas.getZoom();
        let newZoom=canvas.zoom(z);

        // distance to pan so that the mouse pixel
        // stays in the same place
        let offsetX=-(x*z);
        let offsetY=-(y*z);

        canvas.pan(offsetX,offsetY);

        positionStop();
    }
    function popstate(e){
        // stop can exist behind service
        if(e.state){
            // open service which isn't already open
            if(e.state.service&&(!state.service||e.state.service!==state.service)){
                openHref(e.state.service);
            }
            if(e.state.stop){
                if(state.stop!==e.state.stop){
                    openHref(e.state.stop);
                }
                if(!e.state.service&&state.service){
                    //common.closeService();
                    state.service.close();
                }
            }
            state=e.state;
        }else{
            if(state.service){
                closeService();
            }
            if(state.stop){
                closeStop();
            }
            state={stop:null,service:null,stopPosition:null}
        }
    }
    window.onpopstate=popstate;

    document.body.onmousedown=down;
    document.body.onmousemove=move;
    document.body.onmouseup=up;
    document.body.ontouchstart=touchstart;
    document.body.ontouchmove=touchmove;
    document.body.ontouchend=l.ontouchcancel=touchend;
    l.onwheel=wheel;
    document.getElementById("stop").onwheel=e=>{
        canvas.pan(0,-e.deltaY);
        canvas.drawSoon(map);
        positionStop();
    };
    document.body.onkeypress=e=>{
        if("-+=".includes(e.key)){
            let sf=(e.key==="-")?-1.3:1.3;
            zoom(
                sf,
                ...canvas.toMapCoordinates(
                    window.innerWidth/2,
                    window.innerHeight/2
                )
            );

            canvas.drawSoon(map);
        }else if(e.key==="i"){
            init();
        }
    };

    canvas.draw(map);

    a=map;b=canvas;
}

window.onload=async ()=>{
    grid=await (await fetch("/asset/map.txt")).text();
    init();
};
