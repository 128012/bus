fn main(){
    println!("cargo:rerun-if-changed=../map/map.svg");
    if !std::process::Command::new("sh")
        .current_dir("..")
        .arg("map/build.sh")
        .output()
        .expect("running sh")
        .status
        .success()
    {
        panic!("error producing map");
    }
}
