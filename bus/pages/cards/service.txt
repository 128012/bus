<code class="service-code" style="border-color:{colour};">
    <a href="/service/{code}"><span>{code}</span></a>
</code>
<div class="horizontal-overflow"><a href="/service/{code}" class="title">{first_description}</a></div>
