use crate::data::Data;

pub async fn live(atco:&str)->Result<Live,&'static str>{
    let url = format!("http://www.oxontime.com/pwi/departureBoard/{}",atco);
    println!("Requesting from Oxontime: \x1b[33m{}\x1b[0m", &url);

    let wget=std::process::Command::new("wget")
        .arg("-qO-")
        .arg(url)
        .output()
        .map_err(|_|"Couldn't run wget to download from Oxontime")?;
    if !wget.status.success(){
        Err("Oxontime might not have returned 200 or wget might have encountered an error")?
    }
    let rp=String::from_utf8(wget.stdout)
        .map_err(|_|"Oxontime returned invalid UTF-8")?;

    Ok(Live{
            as_of: std::time::Instant::now(),
            rows:serde_json::from_str::<serde_json::Value>(rp.as_str())
                .map_err(|_|"Invalid JSON from oxontime")?
                .as_object()
                .ok_or("JSON wasn't an object")?
                .get(atco)
                .ok_or("No atco key")?
                .get("calls")
                .ok_or("No calls key")?
                .as_array()
                .ok_or("oxontime.calls wasn't an array")?
                .into_iter()
                .map::<Result<(String,String,chrono::naive::NaiveTime),&'static str>,_>(|call|{
                    let call=call
                        .as_object()
                        .ok_or("oxontime.calls[x] wasn't an object")?;
                    Ok((
                        call.get("route_code")
                            .ok_or("no oxontime.calls[i].route_code")?
                            .as_str()
                            .ok_or("oxontime.calls[x].route_code wasn't a string")?
                            .to_uppercase(),
                        call.get("destination")
                            .ok_or("no oxontime.calls[i].destination")?
                            .as_str()
                            .ok_or("oxontime.calls[x].destination wasn't a string")?
                            .to_string(),
                        chrono::naive::NaiveTime::parse_from_str(
                            call.get("expected_arrival_time")
                                .ok_or("no oxontime.calls[i].expect_arrival_time")?
                                .as_str()
                                .ok_or("oxontime.calls[x].expected_arrival_time wasn't a string")?,
                            "%Y-%m-%d %H:%M:%S"
                        )
                            .or_else(|_|chrono::naive::NaiveTime::parse_from_str(
                                call.get("aimed_arrival_time")
                                    .ok_or("no oxontime.calls[i].aimed_arrival_time")?
                                    .as_str()
                                    .ok_or("oxontime.calls[x].aimed_arrival_time wasn't a string")?,
                                "%Y-%m-%d %H:%M:%S"
                            ).map_err(|_|"error parsing oxontime.calls[x].aimed_arrival_time as time"))
                            .map_err(|_|"error parsing both oxontime times as time")?
                    ))
                })
                .collect::<Result<Vec<_>,_>>()?
    })
}

#[derive(Clone)]
pub struct Live{
    pub as_of:std::time::Instant,
    //service code, destination, absolute eta time
    pub rows:Vec<(String,String,chrono::naive::NaiveTime)>
}
impl Live{
    pub fn json(&self,data:&Data) -> String {
        format!(
            "[{}]",
            self.rows.iter()
                .map(|row| format!(
                    r##"["{}","{}","{}"{}]"##,
                    row.0,
                    row.1,
                    row.2.format("%H:%M"),
                    data.services.get(&row.0)
                        .and_then(|service|service.colour.as_ref())
                        .map(|colour|format!(r##","{}""##,colour))
                        .unwrap_or(String::new())
                ))
                .collect::<Vec<String>>()
                .join(",")
        )
    }
}
