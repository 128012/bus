use std::collections::{HashMap,HashSet};

use types::service::{Trip,Timing,Direction,operator_short_name,Visit,OP,YearDay};
use types::stop;

// gtfs.agencies gtfs.routes
pub fn new_trip(trip:&gtfs_structures::Trip,gtfs:&gtfs_structures::Gtfs,dest:String)->Trip{
    Trip{
        dest,
        direction:match trip.direction_id{
            Some(gtfs_structures::DirectionType::Outbound)=>Direction::Outbound,
            Some(gtfs_structures::DirectionType::Inbound)=>Direction::Inbound,
            None=>Direction::Unknown
        },
        operator:operator_short_name(&gtfs.agencies.iter()
            .find(|agency|agency.id.as_ref().unwrap()
                  ==gtfs.routes[&trip.route_id].agency_id.as_ref().unwrap()
            )
            .unwrap()
            .name
        ),
        visits:trip.stop_times.iter()
           .map(|stop_time|Visit{
               atco:stop_time.stop.id.clone(),
               timing:new_timing(stop_time.timepoint),
               arr:stop_time.arrival_time.unwrap(),
               dep:stop_time.departure_time.unwrap(),
               set_down_only:if let gtfs_structures::PickupDropOffType::NotAvailable=stop_time.pickup_type{true}else{false}
           })
           .collect()
    }
}

fn new_timing(d:gtfs_structures::TimepointType)->Timing{
    match d{
        gtfs_structures::TimepointType::Exact=>Timing::Exact,
        gtfs_structures::TimepointType::Approximate=>Timing::Approximate,
    }
}

// gtfs.calendar
pub fn new_op(service_id:&str,gtfs:&gtfs_structures::Gtfs)->OP{
    let calendar=gtfs.calendar.get(service_id);

    let mut plus=Vec::new();
    let mut minus=Vec::new();
    for calendar_date in gtfs.calendar_dates.get(service_id).iter().copied().flatten(){
        match calendar_date.exception_type{
            gtfs_structures::Exception::Added=>plus.push(YearDay::Date(calendar_date.date)),
            gtfs_structures::Exception::Deleted=>minus.push(YearDay::Date(calendar_date.date))
        }
    }

    OP{
        start:calendar.as_ref()
            .map(|calendar|calendar.start_date)
            .unwrap_or(chrono::naive::NaiveDate::MIN),
        end:calendar.as_ref()
            .map(|calendar|calendar.end_date)
            .unwrap_or(chrono::naive::NaiveDate::MAX),
        weekdays:calendar.as_ref()
            .map(|c|[c.monday,c.tuesday,c.wednesday,c.thursday,c.friday,c.saturday,c.sunday])
            .unwrap_or([false;7]), // then they must all be exceptions
        plus,
        minus
    }
}

pub fn read_dir(
    path:String,
    stops:&mut HashMap<String,stop::Stop>,
    code_to_op_to_trips:&mut HashMap::<String,HashMap<OP,Vec<Trip>>>
)->Result<(),&'static str>{
    let gtfs=gtfs_structures::Gtfs::from_path(path)
        .map_err(|_|"The GTFS parser is not happy")?;
    gtfs.print_stats();


    // this being a local bus map gives me an excuse to ignore
    // operators whose numbers overlap with established services
    let ignore_agencies:Vec<&str>=gtfs.agencies.iter()
        .filter(|agency|agency.name=="OurBus Bartons"||agency.name=="Pulhams Coaches")
        .filter_map(|agency|agency.id.as_deref())
        .collect();

    // stops have more names than in NaPTAN
    for stop in gtfs.stops.values(){
        // not interested in stops not in NaPTAN
        stops.get_mut(&stop.id).map(|already_stop|{
            already_stop.add_name(&stop.name);
            if let Some(lat)=stop.latitude{
                already_stop.lat.replace(lat);
            }
            if let Some(lng)=stop.longitude{
                already_stop.lng.replace(lng);
            }
        });
    }

    // gtfs route=txc service
    for route in gtfs.routes.values()
        .filter(|route|route.agency_id.as_ref() // an Option
            // - ignores the probably zero services without this information
            .filter(|&agency_id|!ignore_agencies.contains(&agency_id.as_str()))
            .is_some()
        )
    {
        let trips:Vec<&gtfs_structures::Trip>=gtfs.trips.values()
            .filter(|trip|trip.route_id==route.id&&trip.stop_times.len()>0)
            .collect();

        if trips.iter().any(|trip|trip.stop_times.iter()
            // if GTFS stop has its position (they do so don't bother looking in NaPTAN)
            .filter_map(|stop_time|stop_time.stop.latitude
                .and_then(|lat|stop_time.stop.longitude
                    .map(|long|(lat as f32,long as f32))
                )
            )
            // close enough
            .any(|pos|stop::distance_squared(super::MAGDALEN_STREET,pos).sqrt()<super::WHEATLEY)
        ){
            let code=route.short_name.clone();
            let mut service_ids:HashSet<&str>=HashSet::new();
            
            for trip in trips.iter(){
                // (whilst we are at it)
                for stop_time in &trip.stop_times{
                    stops.get_mut(&stop_time.stop.id)
                        .map(|stop|stop.service_codes.insert(code.clone()));
                }
                // (more to the point)
                service_ids.insert(&trip.service_id);
            }

            // every service(=OP) for this trip
            for service_id in service_ids{
                let mut trips_with_this_service_id:Vec<Trip>=trips.iter()
                    .filter(|trip|trip.service_id==service_id)
                    .map(|trip|{
                        let dest_locality=stops.get(&trip.stop_times.last().unwrap().stop.id)
                            .map(|stop|stop.locality.clone())
                            .or_else(||trip.trip_headsign.clone())
                            .unwrap_or_else(||String::from("(unknown destination)"));
                        new_trip(trip,&gtfs,dest_locality)
                    })
                    .collect();

                code_to_op_to_trips.entry(code.clone())
                    .or_default()
                    // services with different ids can be identical
                    .entry(new_op(service_id,&gtfs))
                    .or_default()
                    .append(&mut trips_with_this_service_id);
            }
        }
    }
    Ok(())
}

