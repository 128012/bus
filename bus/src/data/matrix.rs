use std::convert::TryInto;
use std::collections::{HashMap,HashSet};

use types::stop::StopCollection;
use types::service;

#[derive(Clone,Debug)]
pub struct Matrix{
    pub name:String,
    pub rows:Vec<Row>,
    pub lines:Vec<Vec<(u8,u8)>>
}

impl Matrix{
    pub fn diagram(&self,colour:&str)->String{
        let row_ys=self.rows.iter()
            .scan(15,|this_then_next_row_y,this_row|{
                let this_row_y=*this_then_next_row_y;
                *this_then_next_row_y=this_row_y+if this_row.all_no_wait{30}else{44};
                Some(this_row_y)
            })
            .collect::<Vec<u16>>();
        format!(
            r##"<svg xmlns="http://www.w3.org/2000/svg" width="{svg_width}" height="{svg_height}">
            <g stroke="{colour}">{lines}</g>
            <g fill="white" stroke="black" stroke-width="3.5">{circles}</g>
            </svg>"##,
            svg_width=self.lines.iter()
                .flat_map(|points|points)
                .map(|(x,_)|x*30)
                .max().unwrap(), // right hand side of
            svg_height=row_ys.last().unwrap()+15,
            colour=colour,
            lines=self.lines.iter()
                .map(|points|format!(
                    r##"<path d="M {d}" fill="none" stroke-width="1.5" stroke-linecap="round"/>"##,
                    d={
                        let mut points=points.windows(2)
                            .flat_map(|ft|{
                                let (from_x,from_y)=ft[0];
                                let (to_x,to_y)=ft[1];
                                let from_y=row_ys[from_y as usize];
                                let to_y=row_ys[to_y as usize];

                                if from_x==0&&to_x==0{ // 0 means skipping past some stops
                                    vec![]
                                }else if to_x==0{
                                    vec![
                                        (from_x as u16*30-15,from_y),
                                        (from_x as u16*30-15-13,from_y+9)
                                    ]
                                }else if from_x==0{
                                    vec![
                                        (to_x as u16*30-15-13,from_y-9),
                                        (to_x as u16*30-15,to_y)
                                    ]
                                }else{
                                    vec![
                                        (from_x as u16*30-15,from_y),
                                        (to_x as u16*30-15,to_y+if to_y==from_y{
                                            5
                                        }else{0})
                                    ]
                                }
                            })
                            .collect::<Vec<(u16,u16)>>();
                        points.dedup();
                        points.into_iter()
                            .map(|(x,y)|format!("{},{}",x,y))
                            .collect::<Vec<_>>()
                            .join(" ")
                    }
                ))
                .collect::<Vec<_>>()
                .join(" "),
            circles=self.rows.iter().enumerate()
                .map(|(i,row)|format!(
                    r##"<circle cx="{x}" cy="{y}" r="8.75"/>"##,
                    x=row.active_line*30-15,
                    y=row_ys[i],
                ))
                .collect::<Vec<_>>()
                .join("")
        )
    }
    // NaPTAN includes timing points that timetables may not
    pub fn add_timing_points(&mut self,stops:impl StopCollection){
        for row in &mut self.rows{
            if let Some(stop)=stops.by_atco(&row.atco){
                row.timing_status=std::cmp::max(
                    row.timing_status,
                    stop.timing
                );
            }
        }
    }
}

#[derive(Clone,Debug)]
pub enum Cell{
	Visit(Visit),
	Repetition(chrono::Duration,u8),
	Gap(Option<String>)
}

#[derive(Clone,Debug,Eq,PartialEq)]
pub struct Visit{
    pub timing_status:service::Timing,
    pub arrival:u32,
    pub wait:chrono::Duration,
    pub set_down_only:bool,
    pub operator:&'static str
}
impl Visit{
    fn new(visit:&service::Visit,operator:&'static str)->Self{
        Self{
            timing_status:visit.timing,
            arrival:visit.arr,
            wait:chrono::Duration::seconds((visit.dep-visit.arr).into()),
            set_down_only:visit.set_down_only,
            operator
        }
    }
}

#[derive(Debug,Clone)]
struct RowNode{
	atco:String,
	timing_status:service::Timing,
	visits:Vec<Option<Visit>>,
	from:Vec<u8>,
	to:Vec<u8>,
    active_line:u8 // from right counterintuitively
}
impl RowNode{
	fn shortest_path_back(&self,row_graph:&[RowNode])->u8{
		self.from.iter()
			.map(|&idx|row_graph[idx as usize].shortest_path_back(&row_graph)+1)
			.min()
			.unwrap_or(0)
	}
	fn back_contains(&self,row_graph:&[RowNode],x:u8)->bool{
		self.from.iter()
			.any(|&idx|if idx==x{true}else{row_graph[idx as usize].back_contains(row_graph,x)})
	}
}

#[derive(Clone,Debug)]
pub struct Row{
	pub atco:String,
	pub timing_status:service::Timing,
	pub cells:Vec<Cell>,
	pub active_line:u8,	// line with the stop symbol
    pub all_no_wait:bool
}

struct LiteralRow{
	atco:String,
	timing_status:service::Timing,
	cells:Vec<Option<Visit>>,
	active_line:u8	// line with the stop symbol
}

/// the nodes until the path joins with another and there are still unexplored paths leading to the join. 
/// If this starts at an imaginary root node it should return all the stops in a sensible order
fn nodes_until_join(graph:&mut [(bool,RowNode)],start_at:u8)->(u8,Vec<u8>){ // widest width and node index
    let start=&graph[start_at as usize].1;

    // not all the paths leading here have been traversed or this has already been seen with a path
    // skipping stoops
    if graph[start_at as usize].0||start.from.iter()
        .any(|converging_path_i|!graph[*converging_path_i as usize].0)
    { // converged and still unexplored paths, will come back later
        (0,Vec::new())
    }else{ // go on
        graph[start_at as usize].0=true; // seen

        let to=start.to.clone(); 
        let start_active_line=start.active_line;

        let mut path_from_here=vec![start_at]; // path from here including this node

        let paths_and_widths=to.iter()
            .map(|&to_i|nodes_until_join(graph,to_i))
            .collect::<Vec<(u8,Vec<u8>)>>();
        let mut paths_and_widths_lr=paths_and_widths.clone();
        paths_and_widths_lr.sort_unstable_by_key(|(_,path)|path.len());

        // calculate the width from here forward as well as making all the future row nodes'
        // active lines absolute
        let width=paths_and_widths_lr.into_iter().rev()
            .fold(start_active_line,|width_on_left,(width,path)|{
                for row_node_i in path{
                    // make the active line absolute not relative to this node
                    graph[row_node_i as usize].1.active_line+=width_on_left;
                }
                width_on_left+width
            });

        let paths=paths_and_widths.into_iter()
            .map(|(_,path)|path)
            .collect::<Vec<_>>();

        //paths.sort_unstable_by_key(|path|path.len());

        path_from_here.append(&mut paths.into_iter()
            .flat_map(|path|path)
            .collect::<Vec<_>>()
        );
        (std::cmp::max(1,width),path_from_here) // 1 would be at the end etc
    }
}


#[derive(Clone)]
pub enum Column{
    Cells(Vec<Option<Visit>>), // cells
    Interval(chrono::Duration,u8)// interval,repetition
}
impl Column{
    fn as_cells<'a>(&'a self)->Option<&'a[Option<Visit>]>{
        if let Self::Cells(cells)=self{
            Some(cells)
        }else{
            None
        }
    }
    fn as_interval<'a>(&'a self)->Option<(&'a chrono::Duration,&'a u8)>{
        if let Self::Interval(interval,repetition)=self{
            Some((interval,repetition))
        }else{
            None
        }
    }
}

// assuming the journey all happen on the same day
pub fn construct_matrices(journeys:Vec<&service::Trip>)->Vec<Matrix>{

    // first bucket by directions

    let mut directions:HashMap<service::Direction,(HashSet<&str>,Vec<&service::Trip>)>=HashMap::new();
    for journey in journeys{
        let (dests,journeys)=directions.entry(journey.direction)
           .or_insert_with(||(HashSet::new(),Vec::new()));
        dests.insert(&journey.dest);
        journeys.push(journey);
    }
    // split the unknown direction journeys (if any) by their dest
    let names:HashMap<String,Vec<&service::Trip>>=directions.into_iter()
        .flat_map(|(direction,(dests_with_this_direction,journeys_with_this_direction))|if direction.is_unknown(){
            let mut dests:HashMap<String,Vec<&service::Trip>>=HashMap::new();
            for journey in journeys_with_this_direction{
                   dests.entry(format!("Towards {}",journey.dest))
                       .or_insert_with(Vec::new)
                       .push(journey);
            }
            dests
        }else{
            let mut dests_with_this_direction=dests_with_this_direction.into_iter().collect::<Vec<&str>>();
            dests_with_this_direction.sort_unstable();
            [(
                format!("{}, towards {}",direction.str(),dests_with_this_direction.join("/")),
                journeys_with_this_direction
            )].into_iter().collect()
        })
        .collect();

    let mut matrices:Vec<_>=names.into_iter()
        // to columns - get rid of gaps and empty journeys
        .map(|(name,journeys)|(
            name,
            journeys.iter()
                .map(|journey|(&journey.operator,journey.visits.iter()
                    // Option so that it can be taken
                    .map(|visit|Some(visit))
                    .collect::<Vec<_>>()
                ))
                .filter(|(_,visits)|visits.len()>0)
                .collect::<Vec<_>>(),
        ))
        //.filter(|(_,columns)|columns.len()>0)
        .map(|(name,mut columns_from_journeys)|{

        // create a graph

        let mut row_graph:Vec<RowNode>=vec![RowNode{ // imaginary root node
            atco:String::new(),
            timing_status:service::Timing::Exact,
            visits:Vec::new(),
            from:Vec::new(),
            to:Vec::new(),
            active_line:1 // everything is relative to this so leftmost is 1 not 0
        }];

        for (column_i,(operator,visits)) in columns_from_journeys.iter_mut().enumerate(){
            for row in &mut row_graph{
                while row.visits.len()<column_i{
                    row.visits.push(None);
                }
            }

            // lines don't go to the right unless they join another
            // - active_line stays the same down the journey unless it joins or leaves a line
            //   that is already made

            let first_visit=visits[0].take().unwrap();
            let mut from_i:u8=row_graph.iter().enumerate()
                .filter(|(_,row)|row.atco==first_visit.atco)
                .min_by_key(|(_,row)|row.shortest_path_back(&row_graph))
                .map(|(i,_)|i)
                .unwrap_or_else(||{
                    let index=row_graph.len();

                    row_graph.push(RowNode{
                        atco:first_visit.atco.clone(),
                        timing_status:service::Timing::Approximate, // replaced anyway
                        visits:vec![None;column_i],
                        from:vec![],
                        to:Vec::new(),
                        active_line:0
                    });
                    index
                }).try_into().unwrap();

            // most important TimingStatus
            row_graph[from_i as usize].timing_status=std::cmp::max(
                first_visit.timing,
                row_graph[from_i as usize].timing_status
            );
            row_graph[from_i as usize].visits.push(Some(Visit::new(first_visit,operator)));
            if !row_graph[from_i as usize].from.contains(&0){
                row_graph[from_i as usize].from.push(0);
                row_graph[0].to.push(from_i);
            }

            for visit in visits.iter_mut().skip(1){
                let visit=visit.take().unwrap();
                let this_i:u8=row_graph.iter().enumerate()
                    .filter(|(possible_i,row)|*possible_i!=from_i as usize
                        &&row.atco==visit.atco
                        &&!row_graph[from_i as usize].back_contains(
                            &row_graph,(*possible_i).try_into().unwrap()
                        ) // not cyclic
                    )
                    .min_by_key(|(_,row)|row.shortest_path_back(&row_graph))
                    .map(|(i,_)|i)
                    .unwrap_or_else(||{
                        row_graph.push(RowNode{
                            atco:visit.atco.clone(),
                            timing_status:service::Timing::Approximate, //replaced anyway
                            visits:vec![None;column_i],
                            from:Vec::new(),
                            to:Vec::new(),
                            active_line:0
                        });
                        row_graph.len()-1
                    }).try_into().unwrap();

                // most important TimingStatus
                row_graph[this_i as usize].timing_status=std::cmp::max(
                    visit.timing,
                    row_graph[this_i as usize].timing_status
                );
                row_graph[this_i as usize].visits.push(Some(Visit::new(visit,operator)));
                if !row_graph[from_i as usize].to.contains(&this_i){
                    row_graph[from_i as usize].to.push(this_i);
                }
                if !row_graph[this_i as usize].from.contains(&from_i){
                    row_graph[this_i as usize].from.push(from_i);
                }

                from_i=this_i;
            }
        }

        for (i,node) in row_graph.iter().enumerate(){
            for t in &node.to{
                assert!(row_graph[*t as usize].from.contains(&i.try_into().unwrap()),"From and To were not the same");
            }
            for f in &node.from{
                assert!(row_graph[*f as usize].to.contains(&i.try_into().unwrap()),"From and To were not the same");
            }
        }

        // no parallel edges
        for node in &row_graph{
            assert!(node.to.iter().all(|to_i|node.to.iter().filter(|other_to_i|other_to_i==&to_i).count()==1)
                &&node.from.iter().all(|from_i|node.from.iter().filter(|other_from_i|other_from_i==&from_i).count()==1)
            )
        }

        // prints a graphviz dot representation which can be visualised by this website 
        // if compiled with --cfg graphviz I think
        // http://magjac.com/graphviz-visual-editor/
        #[cfg(graphviz)]
        println!("strict digraph {{\n{}\n}}",row_graph.iter().enumerate()
            .flat_map(|(i,row_node)|row_node.to.iter()
                .copied()
                .map(move|to_i|(i,to_i as usize))
            )
            .map(|(f,t)|format!("{}->{}",f,t))
            .collect::<Vec<_>>()
            .join("\n")
        );

        let mut row_graph=row_graph.into_iter()
            .map(|row_node|(false,row_node)) // indicates whether it has been given an index in row_indices
            .collect::<Vec<_>>();

        let row_indices=nodes_until_join(&mut row_graph,0).1
            .into_iter().skip(1).collect::<Vec<_>>(); // fake root node

        assert!(row_graph.iter()
            .all(|(seen,_)|*seen),
            "A row was missed"
        );
        
        let lines:Vec<Vec<(u8,u8)>>=row_graph.iter().skip(1).zip(1..)
            .flat_map(|((_,row_node),node_i)|row_node.to.iter()
                .map(|to_node_i|{
                    let this_row_y=row_indices.iter()
                        .enumerate()
                        .find(|(_,current_i)|**current_i as usize==node_i)
                        .map(|(move_to_i,_)|move_to_i)
                        .unwrap().try_into().unwrap();
                    let to_row_y=row_indices.iter() // find where it is moved to
                        .enumerate()
                        .find(|(_,current_i)|current_i==&to_node_i)
                        .map(|(move_to_i,_)|move_to_i)
                        .unwrap().try_into().unwrap();

                    // if the two nodes are in vertically in line so a line between them would 
                    // vertical, and they skip some stops without stopping at them, the line would 
                    // overlap, so make sure it doesn't
                    if row_node.active_line==row_graph[*to_node_i as usize].1.active_line
                        &&row_graph[*to_node_i as usize].1.from.iter()
                        .any(|last_node_i|row_graph[*last_node_i as usize].1.back_contains(&row_graph.iter().map(|(_,row_node)|row_node).cloned().collect::<Vec<_>>(),node_i.try_into().unwrap())){
                        vec![
                            (
                                row_node.active_line,
                                this_row_y
                            ),
                            (
                                0, // 0 shall mean a bit to the left and up/down
                                this_row_y
                            ),
                            (
                                0,
                                to_row_y
                            ),
                            (
                                row_graph[*to_node_i as usize].1.active_line,
                                to_row_y
                            )
                        ]
                    }else{

                        // make lines vertical
                        let mut points=vec![
                            (
                                row_node.active_line,
                                this_row_y
                            ),
                            (
                                std::cmp::max(
                                    row_node.active_line,
                                    row_graph[*to_node_i as usize].1.active_line
                                ),
                                if row_node.active_line<row_graph[*to_node_i as usize].1.active_line{
                                    this_row_y
                                }else{
                                    to_row_y
                                }
                            ),
                            (
                                row_graph[*to_node_i as usize].1.active_line,
                                to_row_y
                            )
                        ];
                        points.dedup();
                        points
                    }
                })
                .collect::<Vec<_>>()
            )
            .chain(row_graph[0].1.to.iter() // lines going to the beginning
                .map(|&to_i|vec![
                    (row_graph[to_i as usize].1.active_line,0),
                    (row_graph[to_i as usize].1.active_line,row_indices.iter() // find where it is moved to
                        .enumerate()
                        .find(|(_,current_i)|current_i==&&to_i)
                        .map(|(move_to_i,_)|move_to_i)
                        .unwrap().try_into().unwrap()
                    )
                ])
            )
            .collect();

		let mut row_graph=row_graph.into_iter()
			.map(|(_,row)|Some(row)) // to be taken and get rid of now unnecessary seen variable
			.collect::<Vec<_>>();

		let mut literal_rows=Vec::new();
		for i in row_indices{
			let row=row_graph[i as usize].take().expect("There was a duplicate row.");

			literal_rows.push(LiteralRow{
				atco:row.atco,
				timing_status:row.timing_status,
				cells:row.visits,
				active_line:row.active_line
			});
		}
        
		let row_length=literal_rows.iter()
			.map(|row|row.cells.len())
			.max()
			.unwrap(); // did get rid of empty columns

		for row in &mut literal_rows{
			while row.cells.len()<row_length{
				row.cells.push(None)
			}
		}

        //////////////////
        // back to columns+labels

        let mut transposed_to_columns:Vec<Vec<Option<Visit>>>=Vec::new();
        for (row_i,row) in literal_rows.iter_mut().enumerate(){
            for (cell_i,cell) in std::mem::take(&mut row.cells).into_iter().enumerate(){
                // if this is the first row
                if row_i==0{
                    transposed_to_columns.push(Vec::new());
                }
                transposed_to_columns.get_mut(cell_i).unwrap().push(cell)
            }
        }

        // sort columns
        transposed_to_columns.sort_by(|a,b|a.iter().zip(b)
            .find(|(a,b)|a.is_some()&&b.is_some())
            .map(|(a,b)|a.as_ref().unwrap().arrival.cmp(&b.as_ref().unwrap().arrival))
            .unwrap_or(std::cmp::Ordering::Less) // don't know so same as the order TXC 
                                                 // put them in which is by departure time
        );
        transposed_to_columns.dedup();

        // collapse regular columns
        
       // initialise a stack to push Columns on to
       let mut columns=Vec::new();

        for these_cells in transposed_to_columns{

            // last_four - first is kept and last 3 collapsed
            let mut last_four=columns.iter().rev();

            // last 3 - matches if before interval, interval, after interval
            // after interval is collapsed into interval
            let mut last_three=columns.iter().rev();

            if let (Some(d),Some(c),Some(b),Some(a))=(
                last_four.next().and_then(Column::as_cells),
                
                last_four.next().and_then(Column::as_cells),
                last_four.next().and_then(Column::as_cells),
                last_four.next().and_then(Column::as_cells),
            ){
                let intervals=a.iter()
                    .zip(b)
                    .zip(c)
                    .zip(d)
                    .zip(&these_cells)
                    .map(|maybe_visits|if let (
                        (((Some(a),
                        Some(b)),
                        Some(c)),
                        Some(d)),
                        Some(e)
                    )=maybe_visits{
                        // this is inefficient
                        let mut intervals:Vec<chrono::Duration>=[
                            b.arrival-a.arrival,
                            c.arrival-b.arrival,
                            d.arrival-c.arrival,
                            e.arrival-d.arrival,
                        ].iter()
                            .copied()
                            .map(|s|chrono::Duration::seconds(s.into()))
                            .collect();
                        
                        // are they all the same
                        intervals.dedup();
                        
                        if intervals.len()==1{
                            Some(Some(intervals.remove(0))) // can be collapsed, and there is an interval
                        }else{
                            None // can't be collapsed
                        }
                    }else if let ((((None,None),None),None),None)=maybe_visits{
                         Some(None) // can be collapsed but no interval
                    }else{None})
                    .collect::<Option<Vec<Option<_>>>>();
                if let Some(intervals)=intervals{ // if can be collapsed
                    let mut intervals=intervals.into_iter().flatten().collect::<Vec<_>>();
                    intervals.dedup(); // either all gaps, or all the same interval
                    if intervals.len()==1{
                        columns.truncate(columns.len()-3);
                        columns.push(Column::Interval(intervals.remove(0),3));
                    }
                }
            }else if let (Some(last_cells),Some((interval,repetition)),Some(before_interval))=(
                last_three.next().and_then(Column::as_cells),
                last_three.next().and_then(Column::as_interval),
                last_three.next().and_then(Column::as_cells)
            ){
                if before_interval.iter()
                    .zip(last_cells)
                    .zip(&these_cells)
                    .map(|maybe_visits|if let (
                        (Some(before_interval),
                        Some(last_visits)),
                        Some(these_visits)
                    )=maybe_visits{
                        (last_visits.arrival-before_interval.arrival) as u64==(interval.num_seconds().abs() as u64*((*repetition+1) as u64))
                            &&(these_visits.arrival-last_visits.arrival) as u64==interval.num_seconds().abs() as u64
                    }else if let ((None,None),None)=maybe_visits{
                        true
                    }else{false})
                    .all(|x|x)
                {
                    columns.pop();

                    // increase repetition
                    if let Column::Interval(_,repetition)=columns.last_mut().unwrap(){
                        *repetition+=1;
                    }else{
                        unreachable!()
                    }
                }
            };
            columns.push(Column::Cells(these_cells));
        }

        // back to rows
        let mut rows=literal_rows.into_iter()
            .map(|literal_row|Row{
                atco:literal_row.atco,
                timing_status:literal_row.timing_status,
                cells:Vec::new(),
                //lines:literal_row.lines,
                active_line:literal_row.active_line,
                all_no_wait:true
            })
            .collect::<Vec<_>>();
        for column in columns{
            match column{
                Column::Cells(cells)=>for (cell_i,cell) in cells.into_iter().enumerate(){
                    let no_wait=if let Some(visit)=&cell{
                        visit.wait.is_zero()
                    }else{true};
                    rows[cell_i].all_no_wait=no_wait&&rows[cell_i].all_no_wait;

                    rows[cell_i].cells.push(if let Some(visit)=cell{
                        Cell::Visit(visit)
                    }else{Cell::Gap(None)});
                },
                Column::Interval(interval,repetition)=>for row in &mut rows{
                    row.cells.push(Cell::Repetition(interval,repetition));
                }
            }
        }

        Matrix{
            name,
            rows,
            lines
        }
    })
    .collect();
    matrices.sort_unstable_by(|a,b|b.name.cmp(&a.name));
    matrices
}
