use std::collections::HashSet;

use types::stop::Stop;
use types::service;

pub fn read_naptan_csv(path:&str)->Result<impl Iterator<Item=Result<Stop,&'static str>>,&'static str>{
    Ok(csv::ReaderBuilder::new()
        .from_path(path)
        .map_err(|_|"Couldn't open `data/naptan.csv`")?
        .records()
        .collect::<Result<Vec<_>,_>>()
        .map_err(|_|"Error reading a record in data/naptan.csv")?
        .into_iter()
        .filter(|record|if let Some(x)=record.get(42){x!="del"}else{true})
        .map(naptan_to_stop)
    )
}
pub fn naptan_to_stop(record:csv::StringRecord)->Result<Stop,&'static str>{
    Ok(Stop{
        atco:record.get(0).ok_or("No atco")?.to_uppercase(),
        naptan:record.get(1).map(str::to_lowercase),
        names:vec![record.get(4).ok_or("No NaPTAN name")?.to_string()],
        locality:record.get(18).ok_or("No locality")?.to_string(),
        street:record.get(10).ok_or("No street")?.to_string(),
        easting:record.get(27).ok_or("No easting")?.parse()
                .map_err(|_|"Easting had an invalid value")?,
        northing:record.get(28).ok_or("No northing")?.parse()
            .map_err(|_|"Northing had an invalid value")?,
        timing:service::Timing::from_uppercase(record.get(33)
            .ok_or("No StopType field")?
        ),
        service_codes:HashSet::new(),
        lat:record.get(30)
            .and_then(|f|f.parse().ok()),
        lng:record.get(29)
            .and_then(|f|f.parse().ok()),
    })
}
