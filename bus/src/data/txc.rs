use std::collections::HashMap;

use types::{stop,service};

use chrono::Timelike;

pub fn read_file(
    path:String,
    stops:&mut HashMap<String,stop::Stop>,
    code_to_op_to_trips:&mut HashMap::<String,HashMap<service::OP,Vec<service::Trip>>>
)->Result<(),&'static str>{
    let text=std::fs::read_to_string(path)
        .map_err(|_|"reading file")?;
    let txc_file=txc_parser::File::from_text(&text)?;
    let txc=txc_file.timetable()?;

    for stop in txc.stops.values(){
        // not interested in stops not in NaPTAN
        stops.get_mut(stop.atco).map(|already_stop|{
            already_stop.add_name(&stop.name);
            if let Some((lat,lng))=stop.latlng{
                already_stop.lat.replace(lat);
                already_stop.lng.replace(lng);
            }
        });
    }
    
    for mut service in txc.services{
        if service.journeys.iter()
            .chain(service.lines.values()
                .flat_map(|line|line.journeys.iter())
            )
            .any(|journey|journey.visits.iter()
                .any(|visit|stops.get(visit.atco).map(|stop|stop.distance_squared(super::CARFAX)<super::WHEATLEY_SQUARED).unwrap_or(false))
            ){
                for (code,dest,journey) in std::mem::take(&mut service.journeys).into_iter()
                    .map(|journey|(service.code.clone(),None,journey))
                    .chain(service.lines.into_values()
                        .flat_map(|line|line.journeys.into_iter()
                            .map(move|journey|(line.code.clone(),line.dest.clone(),journey))
                        )
                    )
                {
                    if journey.visits.len()>0{
                        for visit in &journey.visits{
                            stops.get_mut(visit.atco)
                                .map(|stop|stop.service_codes.insert(code.clone()));
                        }

                        let dest_locality=stops.get(journey.visits.last().unwrap().atco)
                            .map(|stop|stop.locality.clone())
                            .or_else(||dest.map(|s|s.to_string()))
                            .unwrap_or_else(||String::from("(unknown destination)"));

                        code_to_op_to_trips.entry(code)
                            .or_default()
                            .entry(new_op(&journey))
                            .or_default()
                            .push(new_trip(journey,dest_locality));
                    }
                }
        }
    }
    Ok(())
}

pub fn new_op(journey:&txc_parser::journey::Journey)->service::OP{ // (already inherit from service)
    service::OP{
        start:journey.start.unwrap_or(chrono::naive::NaiveDate::MIN),
        end:journey.end.unwrap_or(chrono::naive::NaiveDate::MAX),
        weekdays:journey.regular_day_operation.weekdays().unwrap_or([false;7]),
        plus:journey.but_on.iter().map(|&b|service::YearDay::BankHoliday(b)).collect(),
        minus:journey.not_on.iter().map(|&b|service::YearDay::BankHoliday(b)).collect()
    }
}

pub fn new_trip(journey:txc_parser::journey::Journey,dest:String)->service::Trip{
    service::Trip{
        dest,
        direction:match journey.direction{
            txc_parser::Direction::Outbound=>service::Direction::Outbound,
            txc_parser::Direction::Inbound=>service::Direction::Inbound,
            _=>service::Direction::Unknown
        },
        operator:service::operator_short_name(&journey.operator),
        visits:journey.visits.into_iter()
            .map(|visit|service::Visit{
                atco:visit.atco.to_string(),
                timing:match visit.timing_status{
                    txc_parser::visit::TimingStatus::PTP=>service::Timing::Exact,
                    _=>service::Timing::Approximate
                },
                arr:visit.arrival.num_seconds_from_midnight(),
                dep:(visit.arrival+visit.wait.unwrap_or(chrono::Duration::zero())).num_seconds_from_midnight(),
                set_down_only:if let txc_parser::visit::Activity::SetDown=visit.activity{true}else{false}
            })
        .collect()
    }
}
