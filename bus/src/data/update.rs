use crate::data::Data;

pub async fn update(data:&crate::State)->Result<(),&'static str>{
    println!("Downloading and updating data.");

    download().await?;

    println!();
    // re-read data
    *data.write().await=Data::new()?;
    Ok(())
}

pub async fn download()->Result<(),&'static str>{
    if !async_std::process::Command::new("/bin/sh")
        .arg("update.sh")
        .status().await
        .map_err(|_|"Couldn't run update.sh")?
        .success()
    {
        Err("update.sh was apparently not successful")
    }else{
        Ok(())
    }
}

