pub mod stop{
    use crate::data::{Data,numeric};
    use crate::views;

    use types::stop::Stop;

    use std::collections::BTreeMap;

    pub async fn json(stop:&Stop,data:&Data,live: bool) -> String {
        format!(
            r##"{{
                "atco":"{atco}",
                {naptan}
                "names":[{names}],
                "locality":"{locality}",
                "servedBy":{service_codes},
                "street":"{street}"
                {live}
            }}"##,
            atco=stop.atco,
            naptan = match stop.naptan{
                Some(ref naptan)=>format!(r##""naptan":"{}","##,naptan),
                None=>String::new()
            },
            names=stop.names.iter()
                .map(|name|format!("\"{}\"",name))
                .collect::<Vec<_>>()
                .join(","),
            locality=stop.locality,
            service_codes = format!(
                "[{}]",
                stop.service_codes.iter()
                    .map(|code| format!(
                        r##"["{}"{}]"##,
                        code,
                        data.get_service(code)
                            .and_then(|service|service.colour.as_ref())
                            .map(|colour|format!(r##","{}""##,colour))
                            .unwrap_or(String::new())
                    ))
                    .collect::<Vec<String>>()
                    .join(",")
            ),
            street=stop.street,
            live = if live {
                data.live(&stop.atco).await//stop.live(&data).await
                    .map(|l| format!(",\"live\":{}", l.json(&data)))
                    .unwrap_or(String::new())
            } else {
                String::new()
            }
        )
    }

    pub fn card(stop:&Stop)->String{
        format!(
            include_str!("../pages/cards/stop.txt"),
            atco=stop.atco,
            name=stop.names.get(0).as_ref().map(|x|x.as_str()).unwrap_or("<No name?>"),
            street=stop.street,
            locality=stop.locality,
            empty=if stop.service_codes.len()==0{" No services in my timetables seem to stop here."}else{""},
            naptan=stop.naptan.as_ref().map(|x|x.as_str()).unwrap_or(""),
            naptan_numeric=numeric(stop.naptan.as_ref().map(|x|x.as_str()).unwrap_or("")),
            other_names={
                if stop.names.len()>1{
                    format!(
                        r" Also called {}",
                        stop.names.iter().skip(1)
                            .map(|name|format!(r##"<span class="code">{}</span>"##,name))
                            .collect::<Vec<_>>()
                            .join("/")
                    )
                }else{
                    String::new()
                }
            }
        )
    }

    pub async fn live_html(stop:&Stop,data:&Data)->String{
    	match data.live(&stop.atco).await{
    	    Ok(live)=>{
                let mut rows=BTreeMap::new();
    	            for (code,destination,eta) in live.rows{
    	                rows.entry(code)
    	                    .or_insert_with(||(destination,Vec::new()))
    	                    .1.push(eta);
    	            }
    	        let rows=rows.iter().map(|(code,(destination,etas))|format!(
    	                r##"<li>
    	                    <a href="/service/{code}" class="inline-service-code" style="border-color:{colour}">{code}</a>
    	                    <div class="times">
    	                        <h2>{first_eta}</h2>
    	                        {subsequent_etas}
    	                    </div>
    	                    <h2 class="dest">{dest}</h2>
    	                </li>"##,
    	                colour=data.get_service(code)
                            .and_then(|service|service.colour.as_ref())
                            .map(|x|x.as_str())
                            .unwrap_or("gray"),
    	                code=code,
    	                first_eta=etas[0].format("%H:%M"),
    	                subsequent_etas={
    	                    let subsequent_etas=etas.into_iter().skip(1)
        	                    .map(|eta|eta.format("%H:%M").to_string())
        	                    .collect::<Vec<_>>();
        	                if etas.len()>1{
        	                    format!("<p>then {}</p>",subsequent_etas.join(", "))
        	                }else{
        	                    String::new()
        	                }
        	            },
        	            dest=destination
    	        )).collect::<Vec<_>>().join("");
                if rows.len()>0{
                    format!(
    	                r##"<ul class="live">{}</ul>"##,
    	                rows
    	            )
    	        }else{
                    String::from("<p>Oxontime doesn't predict that any buses will stop here soon.</p>")
                }
    	    },
            Err(e)=>{
                println!("\x1b[31mError getting live times: \x1b[0m{}",e);
                String::from(r##"<p>live times not available for some reason.</p>"##)
    	    }
  	}
    }

    pub fn served_by_html(stop:&Stop,data:&Data)->String{
        stop.service_codes.iter()
            .map(|code|format!(
                r##"<li class="service-code" style="border-color:{colour};">
                    <a href="/service/{code}"><code>{code}</code></a>
                </li>"##,
                colour=data.get_service(code).unwrap().colour.as_ref().map(|x|x.as_str()).unwrap_or("gray"),
                code=code
            ))
            .collect::<Vec<_>>()
            .join("")
    }
    pub async fn html(stop:&Stop,data:&Data)->tide::Response{
        tide::Response::builder(200)
            .content_type(tide::http::mime::HTML)
            .body(format!(
                include_str!("../pages/stop.html"),
                name=stop.names[0],
                street=stop.street,
                locality=stop.locality,
                empty=if stop.service_codes.len()==0{" No services in my timetables seem to stop here."}else{""},
                naptan=stop.naptan.as_ref().map(|x|x.as_str()).unwrap_or(""),
                naptan_numeric=numeric(stop.naptan.as_ref().map(|x|x.as_str()).unwrap_or("")),
                other_names={
                    if stop.names.len()>1{
                        format!(
                            " Also called {}",
                            stop.names.iter().skip(1)
                                .map(|name|format!(r##"<span class="code">{}</span>"##,name))
                                .collect::<Vec<_>>()
                                .join("/")
                        )
                    }else{
                        String::new()
                    }
                },
                atco=stop.atco,
	        served_by=views::stop::served_by_html(&stop,&data),
	        live=views::stop::live_html(&stop,&data).await
	    ))
            .build()
    }
}
pub mod service{
    use crate::views;
    use crate::data::matrix;
    use types::stop::StopCollection;
    use types::service::{ConciseOP,Service,formatted_hms,duration_as_hms_string,weekday_name};

    fn get_matrices_not_for_stops(service:&Service,date:chrono::naive::NaiveDate,bank_holidays:&txc_bank::BankHolidays)->Vec<matrix::Matrix>{
        matrix::construct_matrices(service.on(date,bank_holidays).collect())
    }
    fn get_matrices(service:&Service,date:chrono::naive::NaiveDate,stops:impl StopCollection,bank_holidays:&txc_bank::BankHolidays)->Vec<matrix::Matrix>{
        let mut matrices=views::service::get_matrices_not_for_stops(&service,date,bank_holidays);
        for matrix in &mut matrices{
            matrix.add_timing_points(stops);
        }
        matrices
    }

    pub fn html(service:&Service,date:chrono::naive::NaiveDate,stops:impl StopCollection,bank_holidays:&txc_bank::BankHolidays)->tide::Response{
        let colour=service.colour.as_ref().map(|x|x.as_str()).unwrap_or("grey");
        tide::Response::builder(200)
            .content_type(tide::http::mime::HTML)
            .body(format!(
                include_str!("../pages/service.html"),

                code=service.code(),
                colour=colour,
                description=service.dests.join(" + "),
                min=(date-chrono::Duration::days(1)).format("%Y-%m-%d"),
                max=(date+chrono::Duration::days(14)).format("%Y-%m-%d"),
                date=date.format("%Y-%m-%d"),

                tables={
                    let matrices=views::service::get_matrices(&service,date,stops,bank_holidays);
                    if matrices.len()==0{
                        String::from(r##"<p>There are no journeys on this day.</p>"##)
                    }else{matrices.iter()
                        .map(|matrix|{
                            let height=matrix.rows.len();

                            format!(
                                r##"<h2>{name}</h2>
                                <div class="matrix-container"><table class="matrix">{rows}</table></div>"##,
                                name=matrix.name,

                                rows={
                                    matrix.rows.iter().enumerate() // know first row for repetition columns
                                        .map(|(row_i,row)|format!(
                                            r##"<tr class="timing-status--{timing_status}"><th>{label}</th>{possible_svg}{cells}</tr>"##,
                                            timing_status=row.timing_status.acronym(),
                                            label=stops.by_atco(&row.atco)
                                                .map(|stop|format!(
                                                    r##"<span class="light">{locality},</span> <a href="/stop/{atco}">{name}</a>"##,
                                                    locality=stop.locality,
                                                    atco=row.atco,
                                                    name=stop.names.get(0).unwrap()
                                                ))
                                                .unwrap_or_else(||row.atco.to_string()),
                                            cells=row.cells.iter()
                                                .map(|cell|match cell{
                                                    matrix::Cell::Visit(visit)=>{
                                                        let (class,inner)=if !visit.wait.is_zero(){(
                                                            Some(if visit.set_down_only{
                                                                "different-arr-dep set-down"
                                                            }else{
                                                                "different-arr-dep"
                                                            }),
                                                            format!(
                                                                r##"{arr}<span class="for">for {dur}</span>"##,
                                                                arr=formatted_hms(visit.arrival),
                                                                dur=duration_as_hms_string(&visit.wait)
                                                            )
                                                        )}else{(
                                                            if visit.set_down_only{
                                                                Some("set-down")
                                                            }else{None},
                                                            formatted_hms(visit.arrival)//format("%H:%M").to_string()
                                                        )};
                                                        format!(
                                                            r##"<td class="{classes}operator--{operator}">{inner}</td>"##,
                                                            classes=if let Some(class)=class{
                                                                format!("{} ",class)
                                                            }else{String::new()},
                                                            operator=visit.operator,
                                                            inner=inner
                                                        )
                                                    },
                                                    matrix::Cell::Repetition(interval,times)=>{
                                                        if row_i==0{
                                                            format!(
                                                                r##"<td rowspan="{height}">then every {interval} minutes ({times} times) until</td>"##,
                                                                height=height,
                                                                interval=interval.num_minutes(),
                                                                times=times
                                                            )
                                                        }else{String::new()}
                                                    },
                                                    matrix::Cell::Gap(maybe_operator)=>format!(
                                                        r##"<td{class}>-</td>"##,
                                                        class=maybe_operator.as_ref()
                                                            .map(|operator|format!(r##" class="operator--{}"##,operator))
                                                            .unwrap_or_else(String::new)
                                                    )
                                                })
                                                .collect::<Vec<_>>()
                                                .join(""),
                                            possible_svg=if row_i==0{
                                                format!(
                                                    r##"<td rowspan="{height}" class="line-diagram-cell">{svg}</td>"##,
                                                    height=height,
                                                    svg=matrix.diagram(&colour)
                                                )
                                            }else{String::new()}
                                        ))
                                        .collect::<Vec<_>>()
                                        .join("")
                                },
                            )
                        })
                        .collect::<Vec<_>>()
                        .join("")
                    }
                },
            ))
        .build()
    }
    pub fn json(service:&Service,date:chrono::naive::NaiveDate,stops:impl StopCollection,bank_holidays:&txc_bank::BankHolidays)->String{
        format!(
            r##"{{
                "code":"{code}",
                {colour}
                "description":"{description}",
                "points":{points},
                "stops":{{{stop_positions}}},
                "matrices":[{matrices}]
            }}"##,
            code=service.code(),
            colour=service.colour.as_ref()
                .map(|colour|format!(r##""colour":"{}","##,colour))
                .unwrap_or_else(String::new),
            description=service.dests.join(" + "),
            points=serde_json::to_string(&service.paths).unwrap(),
            stop_positions=&service.stops_at.iter()
                .filter_map(|atco|stops.by_atco(&atco)
                    .map(move|stop|(atco,(stop.lat,stop.lng)))
                )
                .map(|(atco,(lat,lon))|format!(r##""{}":[{},{}]"##,atco,lat.unwrap(),lon.unwrap()))
                .collect::<Vec<_>>()
                .join(","),
            matrices=views::service::get_matrices(&service,date,stops,bank_holidays).iter()
                .map(|matrix|format!(
                    r##"{{"name":"{name}","rows":[{rows}]}}"##,
                    name=matrix.name,
                    rows={
                            matrix.rows.iter().enumerate()
                                .map(|(row_i,row)|format!(
                                    r##"{{"timingStatus":"{timing_status}","active_line":{active_line},"atco":"{atco}"{label},"visits":[{visits}]}}"##,
                                    timing_status=row.timing_status.acronym(),
                                    active_line=row.active_line,
                                    atco=row.atco,
                                    label=stops.by_atco(&row.atco)
                                        .map(|stop|format!(
                                            r##","name":"{}","locality":"{}""##,
                                            stop.names.get(0).unwrap(),
                                            stop.locality
                                        ))
                                        .unwrap_or_else(||String::new()),
                                    visits=row.cells.iter()
                                        .map(|cell|match cell{
                                            matrix::Cell::Visit(visit)=>format!(
                                                r##""{arr}-{for}|{operator}{s}""##,
                                                arr=formatted_hms(visit.arrival),
                                                for=if visit.wait.is_zero(){
                                                    String::new()
                                                }else{
                                                    duration_as_hms_string(&visit.wait)
                                                },
                                                operator=visit.operator,
                                                s=if visit.set_down_only{"|s"}else{""}
                                            ),
                                            matrix::Cell::Repetition(interval,times)=>{
                                                if row_i==0{
                                                    format!(
                                                        r##""{interval}m*{times}""##,
                                                        interval=interval.num_minutes(),
                                                        times=times
                                                    )
                                                }else{String::from(r##""*""##)}
                                            }
                                            matrix::Cell::Gap(maybe_operator)=>format!(
                                                r##""-{operator}""##,
                                                operator=maybe_operator.as_ref()
                                                    .map(|operator|format!("|{}",operator))
                                                    .unwrap_or_else(String::new)
                                            )
                                        })
                                        .collect::<Vec<_>>().join(",")
                                ))
                                .collect::<Vec<_>>().join(",")
                    }
                ))
                .collect::<Vec<_>>()
                .join(",")
        )
    }

    pub fn card(service:&Service)->String{
        format!(
            include_str!("../pages/cards/service.txt"),
            colour=service.colour.as_ref().map(|x|x.as_str()).unwrap_or(""),
            code=service.code(),
            first_description=service.dests.join(" + ")
        )
    }
    pub fn short_json(service:&Service)->String{
        format!(
            r##"{{"code":"{code}","description":"{description}"{colour}}}"##,
            code=service.code(),
            description=service.dests.join(" + "),
            colour=if let Some(colour)=&service.colour{
                format!(r##","colour":"{}""##,colour)
            }else{String::new()}
        )
    }

    pub fn diagram(service:&Service,date:chrono::naive::NaiveDate,idx:usize,bank_holidays:&txc_bank::BankHolidays)->Option<String>{
        let colour=service.colour.as_ref().map(|x|x.as_str()).unwrap_or("grey");
        views::service::get_matrices_not_for_stops(&service,date,bank_holidays)
            .get(idx)
            .map(|matrix|matrix.diagram(colour))
    }
    pub fn op_text(op:&ConciseOP,n:usize)->String{
        format!("On {} plus {} minus {} for {} journeys",
            op.weekdays.iter()
                .map(|&(a,b)|if a==b{
                    weekday_name(a).to_string()
                }else{
                    format!("{}-{}",weekday_name(a),weekday_name(b))
                    })
                .collect::<Vec<_>>()
                .join(", "),
            op.plus.iter().map(|x|x.format("%d-%m-%y").to_string()).collect::<Vec<_>>().join(", "),
            op.minus.iter().map(|x|x.format("%d-%m-%y").to_string()).collect::<Vec<_>>().join(", "),
            n
        )
    }
}
