const MAGDALEN_STREET:(f32,f32)=(51.7543528376,-1.2590858453);
const WHEATLEY:f32=0.1196968192;
const CARFAX:(f32,f32)=(451347.0,206168.0);
const WHEATLEY_SQUARED:f32=72_000_000.0;

// store naptan as numeric all the time
pub mod update;
pub mod matrix;
mod live;

mod naptan;
mod gtfs;
mod txc;

use crate::views;

use types::stop::{Stop,StopCollection};
use types::service::{self,Service};
//use spider::Spider;
//use stops_on_roads::coordinates::BBox;

use std::collections::HashMap;
use std::cmp::Ordering;
use async_std::sync::Mutex;

pub struct Data{
    services:HashMap<String,Service>,

    bank_holidays:txc_bank::BankHolidays,

    groups:Vec<Group>, // all stops
    atco:HashMap<String,(usize,usize)>, //atco and index in groups and index in specific group
    naptan:HashMap<String,(usize,usize)>,

    numeric_naptan_to_atco:HashMap<String,String>, // for quickly finding what stop is meant by a numeric naptan code

    words:Vec<(String,usize)>, // words and indices in groups

    live_cache:Mutex<HashMap<String,live::Live>>,

    map:stops_on_roads::Map,
    //spider:Spider
}
impl Data{
    pub fn new()->Result<Self,&'static str>{
        let mut stops=naptan::read_naptan_csv("data/downloaded/naptan.csv")?
            .map(|r|r.map(|stop|(stop.atco.clone(),stop)))
            .collect::<Result<HashMap<String,Stop>,_>>()?;

        let service_colours:HashMap<String,String>=csv::ReaderBuilder::new()
            .from_path("data/service_colours.csv")
            .map_err(|_|"Couldn't open `data/service_colours.csv`")?
            .records()
            .collect::<Result<Vec<_>,_>>()
            .map_err(|_|"Error reading a record in data/service_colours.csv")?
            .into_iter()
            .map::<Result<(String,String),_>,_>(|record|Ok((
                record.get(0)
                    .ok_or("No values in the service_colours record")?
                    .to_uppercase(), // code
                record.get(1)
                    .ok_or("Only one value in the service_colours record")? // colour
                    .to_string()
            )))
            .collect::<Result<Vec<(String,String)>,&'static str>>()?
            .into_iter()
            .map(|(k,v)|(k.to_uppercase(),v))
            .collect();

        // because stops_on_roads::Map does not care about stops' services
        // and NaPTAN seems to be quite complete
        let mut map=stops_on_roads::Map::new("data/downloaded/osm.json",&stops);
        let services:HashMap<String,Service>={
            let mut code_to_op_to_trips:HashMap::<String,HashMap<service::OP,Vec<service::Trip>>>=HashMap::new();

            if let Ok(dir)=std::fs::read_dir("data/downloaded/gtfs"){
                for gtfs_name in dir
                    .filter_map(|entry|entry.ok())
                    .filter_map(|entry|entry.file_name().into_string().ok())
                {
                    gtfs::read_dir(
                        format!("data/downloaded/gtfs/{}",gtfs_name),
                        &mut stops,
                        &mut code_to_op_to_trips
                    )?;
                }
            }
            if let Ok(dir)=std::fs::read_dir("data/downloaded/txc"){
                for (provider,txc_name) in dir
                    .filter_map(|entry|entry.ok())
                    .filter_map(|entry|entry.file_name().into_string().ok())
                    .flat_map(|provider|std::fs::read_dir(format!("data/downloaded/txc/{}",provider)).ok().into_iter()
                         .flat_map(move|entries|{let provider=provider.clone();entries.into_iter()
                            .map(move|entry|(provider.clone(),entry))
                         })
                    )
                    .filter_map(|(provider,entry)|entry.ok().map(|entry|(provider,entry)))
                    .filter_map(|(provider,entry)|entry.file_name().into_string().ok().map(|entry|(provider,entry)))
                {
                    txc::read_file(
                        format!("data/downloaded/txc/{}/{}",provider,txc_name),
                        &mut stops,
                        &mut code_to_op_to_trips
                    )?;
                }
            }

            code_to_op_to_trips.into_iter()
                .map(|(code,ops)|{
                    let paths=map.add_code(&code,ops.values().flatten());
                    let colour=service_colours.get(&code)
                        .map(|x|x.as_str());
                    (
                        code.clone(),
                        Service::new(
                            code,
                            colour,
                            paths,
                            ops,
                        )
                    )
                })
                .collect()
        };


        // initially all groups manually specified

        // the name is an Option so that it can be decided in the stop groups file
        // or from the longest stop name of the members
        
        let groups:Vec<Group>={
            let mut groups_without_necessarily_names:Vec<(Vec<Stop>,Option<String>)>=csv::ReaderBuilder::new()
                .comment(Some(b'#'))
                .from_path("data/groups.csv")
                .map_err(|_|"Couldn't open `data/groups.csv`")?
                .records()
                .collect::<Result<Vec<_>,_>>()
                .map_err(|_|"Error reading a record in data/groups.csv")?
                .into_iter()
                .map(|record|(
                    record.get(1).expect("No second column in data/groups.csv").split("+")
                        .map(|atco|stops.remove(atco).expect("atco in data/groups.csv which doesn't exist"))
                        .collect::<Vec<Stop>>(),
                    Some(record.get(0).expect("No first column in data/groups.csv").to_string())
                ))
                .collect::<Vec<_>>();

            fn close_stops(mut stops:&mut HashMap<String,Stop>,mut names:&mut Vec<String>,x:f32,y:f32)->Vec<Stop>{
                let mut the_close_stops=Vec::new();

                while let Some(close_atco_code)=stops.iter()
                    .find(|(_,stop)|stop.distance_squared((x,y))<160_000.0 // = 400^2
                        &&stop.names.iter()
                            .any(|name|names.contains(name))
                    )
                    .map(|(atco,_)|atco.clone())
                {
                    let stop=stops.remove(&close_atco_code).unwrap();
                    for name in &stop.names{
                        if !names.contains(name){
                            names.push(name.clone());
                        }
                    }
                    // try with different position
                    the_close_stops.append(&mut close_stops(
                        &mut stops,
                        &mut names,
                        stop.easting,
                        stop.northing
                    ));
                    the_close_stops.push(stop);
                }

                the_close_stops
            }

            let all_atco_codes:Vec<String>=stops.keys().cloned().collect();

            for atco in &all_atco_codes{
                // might have been removed already for a different group
                if stops.contains_key(atco.as_str()){
                    let stop=&stops[atco];
                    let (x,y)=(stop.easting,stop.northing);
                    let mut names=stop.names.clone();

                    // add to the list of groups
                    groups_without_necessarily_names.push((
                        close_stops(
                            &mut stops,
                            &mut names,
                            x,
                            y
                        ),
                        None
                    ));
                }
            }

            groups_without_necessarily_names.into_iter()
                .map(|(mut stops,name)|{
                    let name=name.unwrap_or_else(||{
                        let all_names:Vec<&str>=stops.iter()
                            .flat_map(|stop|&stop.names)
                            .map(|name|name.as_str())
                            .collect();
                        let without_brackets:Vec<&str>=all_names.iter()
                            .filter(|name|!(name.contains("(")&&name.contains(")")))
                            .map(|name|*name)
                            .collect();
                        if without_brackets.len()>0{
                            without_brackets
                        }else{
                            all_names
                        }.iter()
                            .filter(|name|!name.contains(","))
                            .max_by_key(|name|name.len())
                            .unwrap()
                            .to_string()
                    });
                    // sort to make it easier to know if this group matches what is requested
                    stops.sort_unstable_by_key(|stop|stop.atco.clone());

                    // choose the best name for each stop
                    for stop in &mut stops{
                        let indices_of_brackets:Vec<usize>=stop.names.iter().enumerate()
                            .filter(|(_,name)|name.contains("(")&&name.contains(")"))
                            .map(|(i,_)|i)
                            .collect();
                        if indices_of_brackets.len()==1{
                            stop.names.swap(0,indices_of_brackets[0]);
                        }
                    }
                    Group::new(stops,name)
                })
                .collect()
        };

        let mut words=groups.iter().enumerate()
            .flat_map(|(i,group)|{
                let mut words=group.words().collect::<Vec<String>>();
                words.sort_unstable();
                words.dedup();
                words.into_iter()
                    .map(move|word|(word,i))
            })
            .collect::<Vec<(String,usize)>>();

        words.sort_unstable_by_key(|(word,_)|word.clone());

        Ok(Self{
            services,

            bank_holidays:txc_bank::BankHolidays::from_csv(
                &std::fs::read_to_string("data/bank.csv")
                    .map_err(|_|"reading data/bank.csv")?
            )?,

            atco:groups.iter().enumerate()
            	.flat_map(|(group_index,group)|group.stops.iter().enumerate()
            		.map(move|(stop_index,stop)|(stop.atco.clone(),(group_index,stop_index)))
            	)
            	.collect(),
            naptan:groups.iter().enumerate()
               	.flat_map(|(group_index,group)|group.stops.iter().enumerate()
               		.filter_map(move|(stop_index,stop)|stop.naptan.clone() // Option
               			.map(|naptan|(naptan,(group_index,stop_index)))
               		)
               	)
              	.collect(),
            groups,

            // for quickly finding what stop is meant by a numeric naptan code
            numeric_naptan_to_atco:stops.values()
                .filter_map(|stop|stop.naptan.as_ref()
                    .map(|naptan|(numeric(&naptan),stop.atco.clone()))
                )
                .collect::<HashMap<String,String>>(),

            words, // of stop (group) names
            live_cache:Mutex::new(HashMap::new()),
            //spider:Spider::new(map),
            map
        })
    }

    pub fn get_service<'a>(&'a self,code:&str)->Option<&Service>{
        let code=code.to_uppercase();
        self.services.get(&code)
    }

    pub fn by_any_code<'a>(&'a self,code:&str)->Option<&'a Stop>{
        if code.starts_with("693"){
            self.numeric_naptan_to_atco
                .get(code)
                .and_then(|atco|self.by_atco(&atco.to_uppercase()))
        }else if code.starts_with("oxf"){
            self.naptan.get(&code.to_lowercase())
               	.map(|(group_index,stop_index)|self
               		.groups.get(*group_index).unwrap()
               		.stops.get(*stop_index).unwrap()
               	)
        }else if code.starts_with("340"){
            self.atco.get(&code.to_uppercase())
            	.map(|(group_index,stop_index)|self
            		.groups.get(*group_index).unwrap()
            		.stops.get(*stop_index).unwrap()
            	)
        }else{
            None
        }
    }

    pub fn stops<'a>(&'a self)->Stops<'a>{
        Stops{
            data:&self
        }
    }
    pub fn by_atco<'a>(&'a self,atco:&str)->Option<&'a Stop>{
        self.atco.get(atco)
            .map(|(group_index,stop_index)|self
                .groups.get(*group_index).unwrap()
                .stops.get(*stop_index).unwrap()
            )
    }

    pub fn by_naptan<'a>(&'a self,naptan:&str)->Option<&'a Stop>{
        self.naptan.get(naptan)
           	.map(|(group_index,stop_index)|self
           		.groups.get(*group_index).unwrap()
           		.stops.get(*stop_index).unwrap()
           	)
    }

    pub fn by_atco_codes<'a>(&'a self,mut codes_query:Vec<String>)->Option<&'a Group>{
        codes_query.sort_unstable();
        
        self.groups.iter().find(|group|
            group.stops.len()==codes_query.len()
                &&group.stops.iter()
                    .zip(codes_query.iter())
                    .all(|(stop,atco)|&stop.atco==atco)
        )
    }

    fn search_stop_names(&self,query:&str)->Vec<SearchResult>{
        let mut query_words=query.split_whitespace();

        // get matches for the first term
        let mut filtered=if let Some(term)=query_words.next(){
            // use binary search for the first word
            let definite=self.words.binary_search_by(|(word,_)|{
                if word.starts_with(term){
                    Ordering::Equal
                }else if word.as_str()>term{
                    Ordering::Greater
                }else{
                    Ordering::Less
                }
            });
            if let Ok(definite)=definite{
                let (first,second)=self.words.split_at(definite);
                first.iter()
                    .rev()
                    .take_while(|(word,_)|word.starts_with(term))
                    .collect::<Vec<_>>().into_iter()
                    .rev()
                    .chain(second.iter()
                        // second includes the definite match
                        .take_while(|(word,_)|word.starts_with(term))
                    )
                    .map(|(_,i)|self.groups.get(*i).unwrap())
                    .collect::<Vec<_>>()
            }else{
                return vec![];
            }
        }else{
            return vec![];
        };

        // filter for the rest of the terms
        for term in query_words{
            filtered=filtered.into_iter()
                .filter(|group|group.words().any(|word|word.to_lowercase().starts_with(term)))
                .collect();
        }
        filtered.truncate(7);
        filtered.into_iter()
            .map(|group|SearchResult::Group(group))
            .collect()
    }

    pub fn search(&self,query:&str)->Vec<SearchResult>{
        let query=query.to_lowercase().chars()
            .filter(|c|c.is_ascii_alphanumeric()||c==&' ')
            .collect::<String>();

        if query.starts_with("oxf"){
            // naptan code
            self.by_naptan(&query)
                .map(|stop|vec![SearchResult::Stop(stop)])
                .unwrap_or_else(||self.search_stop_names(&query))
        }else if query.starts_with("693"){
            self.numeric_naptan_to_atco
                .get(&query)
                .and_then(|atco|self.by_atco(&atco.to_uppercase()))
                .map(|stop|vec![SearchResult::Stop(stop)])
                .unwrap_or_else(||Vec::new()) // stop names won't start with 693
        }else if query.starts_with("340"){
            // atco code
            let uppercase_query=query.to_uppercase();
            self.by_atco(&uppercase_query)
                .map(|stop|vec![SearchResult::Stop(stop)])
                // can't view stops that aren't in NaPTAN directly
                .unwrap_or_else(||Vec::new()) // stop names won't start with 340
        }else{
            // either a stop or service

            let mut results=self.search_stop_names(&query);
            if let Some(service)=self.get_service(&query){
                results.push(SearchResult::Service(service));
            }
            results
        }
    }

    pub async fn live(&self,atco:&str)->Result<live::Live,&'static str>{
        // don't request the same live times frequently; that would be pointless!
        let mut map=self.live_cache.lock().await;

        let recent=map.get(atco)
            .filter(|live|live.as_of.elapsed().as_secs()<30)
            .is_some();

        let live=if recent{
            map[atco].clone()
        }else{
            let live=live::live(&atco).await?;
            map.insert(atco.to_string(),live.clone());
            live
        };
        Ok(live)
    }
    pub fn map(&self)->&stops_on_roads::Map{
        &self./*spider.*/map//()
    }
    pub fn bank_holidays(&self)->&txc_bank::BankHolidays{
        &self.bank_holidays
    }
    /*pub fn spider(&self,stop:&Stop)->String{
        let latlng=(stop.lat.unwrap(),stop.lng.unwrap());
        let bbox=BBox::new_around_point_degrees(latlng);
        self.spider.at_bbox(bbox)
    }*/
}

#[derive(Copy,Clone)]
pub struct Stops<'a>{
    data:&'a Data
}
impl<'a> StopCollection for Stops<'a>{
    fn by_atco<'b>(&'b self,s:&str)->Option<&'b Stop>{
        self.data.by_atco(s)
    }
}

pub enum SearchResult<'a>{
    Service(&'a Service),
    Stop(&'a Stop),
    Group(&'a Group)
}
impl SearchResult<'_>{
    pub async fn json(self,data:&Data)->String{
        match self{
            Self::Service(service)=>views::service::short_json(&service),
            Self::Stop(stop)=>views::stop::json(&stop,data,false).await,
            Self::Group(group)=>group.json(data,false).await
        }
    }
    pub fn html(self)->String{
        match self{
            Self::Service(service)=>format!(
                r##"<li class="no-padding popout box no-padding">{}</li>"##,
                views::service::card(&service)
            ),
            Self::Stop(stop)=>format!(
                r##"<li class="stop box no-padding">{}</li>"##,
                views::stop::card(&stop)
            ),
            Self::Group(group)=>format!(
                r##"<li class="stop box no-padding">{}</li>"##,
                group.card()
            )
        }
    }

}

// any number of stops - could be 1
pub struct Group{
    name:String,
    stops:Vec<Stop>
}
impl Group{
    pub async fn html(&self,data:&Data)->tide::Response{
            tide::Response::builder(200)
                .content_type(tide::http::mime::HTML)
                .body(format!(
                    include_str!("../pages/group.html"),
                    name=self.name,
                    stops=futures_util::future::join_all(self.stops.iter()
                        .map(|stop|async move{format!(
                             include_str!("../pages/cards/big_stop.txt"),
                             card=views::stop::card(&stop),
                             served_by=views::stop::served_by_html(&stop,&data),
                             live=views::stop::live_html(&stop,&data).await
                        )})
                    ).await.join("")
                ))
                .build()
    }
	pub async fn json(&self,data:&Data,live:bool)->String{
		format!(
			r##"{{"name":"{}","stops":[{}]}}"##,
			self.name,
			futures_util::future::join_all(self.stops.iter()
				.map(|stop|views::stop::json(stop,&data,live))
			).await.join(",")
		)
	}
	pub fn card(&self)->String{
        if self.stops.len()==1{
            views::stop::card(&self.stops[0])
        }else{
            format!(
                include_str!("../pages/cards/group.txt"),
                link=self.stops.iter()
                .map(|stop|stop.atco.clone())
                .collect::<Vec<_>>()
                .join("+"),
                name=self.name,
                number=self.stops.len(),
                street=self.stops.get(0)
                    .map(|stop|stop.street.as_str())
                    .unwrap_or(""),
                locality=self.stops.get(0)
                    .map(|stop|stop.locality.as_str())
                    .unwrap_or("")
            )
        }
	}
    fn words(&self)->impl Iterator<Item=String>+'_{ // return value captures &self
        self.stops.iter()
            .flat_map(|stop|stop.names.iter()
                .flat_map(|name|name.split_whitespace())
            )
            .chain(self.name.split_whitespace())
            .chain(self.stops[0].locality.split_whitespace())
            .map(|s|s.to_lowercase().chars()
                .filter(|c|c.is_ascii_alphanumeric())
                .collect::<String>()
            )
    }
	pub fn new(mut stops:Vec<Stop>,name:String)->Self{
        debug_assert!(stops.len()>0);
		stops.sort_unstable_by_key(|stop|stop.atco.clone());
		Self{
			stops,
			name
		}
	}
}
pub enum StopOrGroup<'a>{
	Stop(&'a Stop),
	Group(&'a Group)
}
impl StopOrGroup<'_>{
	pub async fn html(&self,data:&Data)->tide::Response{
		match self{
			Self::Stop(stop)=>views::stop::html(&stop,&data).await,
			Self::Group(group)=>group.html(&data).await
		}
	}
	pub async fn json(&self,data:&Data,live:bool)->String{
		match self{
			Self::Stop(stop)=>views::stop::json(&stop,&data,live).await,
			Self::Group(group)=>group.json(&data,live).await
		}
	}
	/*pub fn card(&self)->String{
		match self{
			Self::Stop(stop)=>stop.card(),
			Self::Group(group)=>group.card()
		}
	}*/
}

pub fn numeric(s:&str)->String{
    s.chars().map(|c|match c{
       'a'..='c'=>'2',
       'd'..='f'=>'3',
       'g'..='i'=>'4',
       'j'..='l'=>'5',
       'm'..='o'=>'6',
       'p'..='s'=>'7',
       't'..='v'=>'8',
       'w'..='z'=>'9',
       x=>if x.is_ascii_digit(){x}else{'?'}
    })
    .collect::<String>()
}
