// rq.state() is where data is stored, in this Arc
use std::sync::Arc;
use async_std::sync::RwLock;
use std::net::{Ipv4Addr,SocketAddr};
use chrono::Timelike;

mod data;
mod views;
use data::Data;

static DEFAULT_PORT:u16=8080;

type State=Arc<RwLock<Data>>;

fn main(){
    // has this been started in the wrong dir?
    if std::fs::metadata("data").is_err()&&std::fs::metadata("bus/data").is_ok(){
        std::env::set_current_dir("bus").expect("Couldn't cd into the bus directory.");
    }

    let allow_update_request=std::env::args()
        .any(|x|x.eq_ignore_ascii_case("allow-update-request"));
    if std::env::args()
        .any(|x|x.eq_ignore_ascii_case("download")){
        if let Err(e)=async_std::task::block_on(data::update::download()){
            println!("\x1b[31mCouldn't download new data: \x1b[0m{}",e);
        }
    }

    let https=std::env::var("BUS_CERT")
        .and_then(|cert|std::env::var("BUS_KEY")
            .map(|key|(key,cert))
        )
        .ok()
        .or_else(||if std::path::Path::new("../cert").exists()&&std::path::Path::new("../key").exists(){
            Some(("key".into(),"cert".into()))
        }else{None});
        
    match data::Data::new(){
        Ok(read_data)=>{
            let stored_data=Arc::new(RwLock::new(read_data));

            // Server
            let mut server=tide::Server::with_state(stored_data.clone());

            server.at("/").serve_file("asset/spa.html")
                .expect("asset/spa.html doesn't exist");
            server.at("/html").serve_file("asset/html.html")
                .expect("asset/html.html doesn't exist");
            server.at("/asset").serve_dir("asset/")
                .expect("asset dir doesn't exist");
            server.at("/service/:code").get(service);
            server.at("/service/:code/:direction_idx/diagram.svg").get(line_diagram);
            server.at("/service/:code/map.svg").get(svg_route_map);
            server.at("/service/:code/ops.txt").get(ops_text);
            server.at("/stop/:code").get(stop);
            //server.at("/stop/:code/spider.svg").get(spider);
            server.at("/search").get(search);
            if allow_update_request{
                println!("Allowing /update to update data.");
                server.at("/update").get(update_endpoint);
            }

            // This is for 404. Maybe there is a better way
            server.with(tide::utils::After(fill_empty_error_pages));

            // Updater
            let updater=async_std::task::spawn(async move{
                    let stored_data=stored_data.clone();
                    // update data every 03:00
                    loop{
                        async_std::task::sleep(std::time::Duration::from_secs(
                            (3600*24-chrono::Local::now()
                                .naive_local().time()
                                .num_seconds_from_midnight()+3600*3
                            ).into()
                        )).await;
                        update(&stored_data).await;
                    }
            });

            let mut tasks:Vec<std::pin::Pin<Box<
                dyn std::future::Future<Output=std::io::Result<()>>
            >>>=vec![Box::pin(updater)];

            if let Some((key,cert))=https{
                println!("Listening on ports 80 and 443 (80 only redirects to HTTPS)");
                tasks.push(Box::pin(server.listen(
                    tide_rustls::TlsListener::build()
                        .addrs(SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(),443)) // 0.0.0.0
                        .key(key)
                        .cert(cert)
                )));

                let mut https_redirect_server=tide::new();
                https_redirect_server.at("/").get(redirect_to_https);
                https_redirect_server.at("*").get(redirect_to_https);
                tasks.push(Box::pin(https_redirect_server.listen(
                    SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(),80)
                )));
            }else{
                let http_only_port=std::env::args()
                    .find_map(|x|x.parse::<u16>().ok())
                    .unwrap_or(DEFAULT_PORT);
                println!("Listening on port \x1b[36m{}\x1b[0m.\n",http_only_port);
                tasks.push(Box::pin(server.listen(SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(),http_only_port))))
            };

            // don't let the data updater continue if the server can't listen
            // if it wasn't the updater that resolved with an error
            let (output,which_resolved,_)=async_std::task::block_on(futures_util::future::select_all(tasks));
            if which_resolved!=0{ // it wasn't the updater that failed
                if let Err(e)=output{
                    println!(
                        "\x1b[31mError listening. Is the port in use and are you allowed to use it? \x1b[0m{}",
                        e
                    );
                }
            }
        },
        Err(err)=>println!("\x1b[31mError reading data, so exiting: \x1b[0m{}",err)
    }
}

async fn service(rq:tide::Request<State>)->tide::Result{
    Ok(if let Ok(id)=rq.param("code"){
        let (code,ext)=name_and_ext(id);
        let date=rq.url().query_pairs()
            .find(|(k,_)|k=="on")
            .and_then(|(_,date)|date.parse().ok())
            .unwrap_or_else(||chrono::Local::now().naive_local().date());

        let data=rq.state().read().await;

        if let Some(service)=data.get_service(code){
            if let Some("json")=ext{
                 plain(views::service::json(&service,date,data.stops(),data.bank_holidays()))
            }else{
                views::service::html(&service,date,data.stops(),data.bank_holidays())
            }
        }else{
            tide::Response::builder(404)
                .content_type(tide::http::mime::HTML)
                .body(include_str!("../pages/404/service.html"))
                .build()
        }
    }else{
        not_found()
    })
}

async fn svg_route_map(rq:tide::Request<State>)->tide::Result{
    Ok(if let Ok(code)=rq.param("code"){
        let data=rq.state().read().await;
        if let Some(service)=data.get_service(code){
            tide::Response::builder(200)
                .content_type(tide::http::mime::SVG)
                .body(data.map().svg_with_points(&service.paths,service.colour.as_deref()))
                .build()
        }else{
            not_found()
        }
    }else{
        not_found()
    })
}

async fn ops_text(rq:tide::Request<State>)->tide::Result{
    Ok(if let Ok(code)=rq.param("code"){
        let data=rq.state().read().await;
        if let Some(service)=data.get_service(code){
            let today=chrono::Local::now().naive_local().date();
            let month=today+chrono::Duration::weeks(4);
            tide::Response::builder(200)
                .content_type(tide::http::mime::PLAIN)
                .body(service.ops.iter()
                      .map(|(op,trips)|(op.concise(month,data.bank_holidays()),trips.len()))
                      .filter(|(op,_)|op.any())
                      .map(|(op,n)|views::service::op_text(&op,n))
                      .collect::<Vec<_>>()
                      .join("\n")
                 )
                .build()
        }else{
            not_found()
        }
    }else{
        not_found()
    })
}

/*async fn spider(rq:tide::Request<State>)->tide::Result{
    Ok(if let Ok(code)=rq.param("code"){
        let data=rq.state().read().await;
        if let Some(stop)=data.by_any_code(code){
            tide::Response::builder(200)
                .content_type(tide::http::mime::SVG)
                .body(data.spider(&stop))
                .build()
        }else{
            not_found()
        }
    }else{
        not_found()
    })
}*/

async fn line_diagram(rq:tide::Request<State>)->tide::Result{
    Ok(if let (Ok(code),Ok(direction_idx_s))=(rq.param("code"),rq.param("direction_idx")){
        if let Ok(direction_idx)=direction_idx_s.parse(){
            let date=rq.url().query_pairs()
                .find(|(k,_)|k=="on")
                    .and_then(|(_,date)|date.parse().ok())
                .unwrap_or_else(||chrono::Local::now().naive_local().date());
            let data=rq.state().read().await;

            if let Some(service)=data.get_service(code){
                if let Some(diagram)=views::service::diagram(&service,date,direction_idx,data.bank_holidays()){
                    tide::Response::builder(200)
                        .content_type(tide::http::mime::SVG)
                        .body(diagram)
                        .build()
                }else{
                    not_found()
                }
            }else{
                not_found()
            }
        }else{
            not_found()
        }
    }else{
        not_found()
    })
}

async fn stop(rq:tide::Request<State>)->tide::Result{
    let data=rq.state().read().await;
    
    Ok(if let Ok(id)=rq.param("code"){
        let (code,ext)=name_and_ext(id);

        if let Some(stop)={
            if code.contains("+"){
                data.by_atco_codes(code.split("+")
                    .map(|x|x.to_string())
                    .collect()
                )
                    .map(|group|data::StopOrGroup::Group(group))
            }else{
                data.by_any_code(code)
                    .map(|group|data::StopOrGroup::Stop(group))
            }
        }{
            if let Some("json")=ext{
                let wants_live=rq.url().query_pairs()
                   .any(|(k,_)|k=="live");
                plain(stop.json(&data,wants_live).await)
            }else{
                stop.html(&data).await
            }
        }else{
            tide::Response::builder(404)
                .content_type(tide::http::mime::HTML)
                .body(include_str!("../pages/404/stop.html"))
                .build()
        }
    }else{
        not_found()
    })
}

async fn search(rq:tide::Request<State>)->tide::Result{
    let data=rq.state().read().await;

    let query=rq.url().query_pairs()
        .find(|(k,_)|k.eq_ignore_ascii_case("q"))
        .map(|(_,query)|query);
        
    Ok(if let Some(query)=query{
        let results=data.search(&query);
        if rq.url().query_pairs()
            .any(|(k,v)|k=="format"&&v=="json"){
            plain(format!(
                "[{}]",
                futures_util::future::join_all(results.into_iter()
                    .map(|result|result.json(&data))
                )
                    .await
                    .join(",")
            ))
        }else{
            html(format!(
                include_str!("../pages/search.html"),
                empty=if results.len()==0{"No results."}else{""},
                query=&query,
                results=results.into_iter()
                    .map(|result|result.html())
                    .collect::<Vec<_>>()
                    .join("")
            ))
        }
    }else{
        tide::Redirect::see_other("/").into()
    })
}

async fn fill_empty_error_pages(rp:tide::Response)->tide::Result{
    Ok(if rp.is_empty().unwrap_or(false){
        match rp.status(){
            tide::StatusCode::NotFound=>not_found(),
            tide::StatusCode::InternalServerError=>internal_server_error(),
            _=>rp // redirects
        }
    }else{rp})
}

async fn update_endpoint(rq:tide::Request<State>)->tide::Result{
    update(rq.state()).await;
    Ok(not_found())
}

async fn update(stored_data:&State){
    if let Err(e)=data::update::update(stored_data).await{   
        println!("\x1b[31mError updating some data, so continuing to use some current data: \x1b[0m{}",e);
    }
}

async fn redirect_to_https(rq:tide::Request<()>)->tide::Result{
    let mut new_url=rq.url().clone();
    new_url.set_scheme("https").unwrap();
    Ok(tide::Redirect::permanent(new_url).into())
}

fn not_found()->tide::Response{
    tide::Response::builder(404)
        .content_type(tide::http::mime::HTML)
        .body(include_str!("../pages/404/generic.html"))
        .build()
}

fn internal_server_error()->tide::Response{
    tide::Response::builder(500)
        .content_type(tide::http::mime::HTML)
        .body(include_str!("../pages/500.html"))
        .build()
}

/// if the passed string contains a dot it is a name and extension
fn name_and_ext(s:&str)->(&str,Option<&str>){
    let mut segments=s.splitn(2,".");
    // there are either one or two elements
    (segments.next().unwrap(),segments.next())
}

fn html(body:String)->tide::Response{
    tide::Response::builder(200)
        .content_type(tide::http::mime::HTML)
        .body(body)
        .build()
}
fn plain(body:String)->tide::Response{
    tide::Response::builder(200)
        .content_type(tide::http::mime::PLAIN)
        .body(body)
        .build()
}
