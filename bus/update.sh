#!/bin/sh
set -e

[ -e /tmp/bus ] && rm -rf /tmp/bus # might be either file or directory
mkdir /tmp/bus

# get rid of old data
if [ -e data/downloaded ]; then
    [ -e data/downloaded~ ] && rm -r data/downloaded~
    mv data/downloaded data/downloaded~
fi

mkdir data/downloaded
mkdir data/downloaded/txc
mkdir data/downloaded/txc/obc
mkdir data/downloaded/txc/scox
mkdir data/downloaded/txc/grayline

wget -qOdata/downloaded/naptan.csv 'https://naptan.api.dft.gov.uk/v1/access-nodes?dataFormat=csv&atcoAreaCodes=340'

wget -qO- 'https://data.bus-data.dft.gov.uk/timetable/dataset/11458/download/'>/tmp/bus/obc.zip
    busybox unzip -qo /tmp/bus/obc.zip -ddata/downloaded/txc/obc
wget -qO- 'https://data.bus-data.dft.gov.uk/timetable/dataset/2047/download/'>/tmp/bus/scox.zip
    busybox unzip -qo /tmp/bus/scox.zip -ddata/downloaded/txc/scox
wget -qO- 'https://data.bus-data.dft.gov.uk/timetable/dataset/11586/download/'>/tmp/bus/grayline.zip
    busybox unzip -qo /tmp/bus/grayline.zip -ddata/downloaded/txc/grayline

# first -> download
#       -> can copy from yesterday? -> copy from yesterday
#                                   -> download
# 
# !first && can copy from yesterday -> copy from yesterday
#                                   -> download

if ! [ $(date +%d) -eq "01" ] && [ -f 'data/downloaded~/osm.json' ]
then
    cp data/downloaded~/osm.json data/downloaded/osm.json
else
    wget -qO data/downloaded/osm.json 'https://overpass.kumi.systems/api/interpreter?data=[timeout:900][bbox:51.649493,-1.332672,51.831657,-1.117897][out:json];((way[highway~"^(((motorway|trunk|primary|secondary|tertiary)(_link)?)|unclassified|residential|living_street|service|track)$"][area!=yes];way[highway="pedestrian"][psv="yes"][area!=yes];);node(w););out%20skel;'
fi

# [timeout:900][bbox:51.649493,-1.332672,51.831657,-1.117897][out:json];
# (
#   (
#     way[highway~"^(((motorway|trunk|primary|secondary|tertiary)(_link)?)|unclassified|residential|living_street|service|track)$"][area!=yes];
#     way[highway="pedestrian"][psv="yes"][area!=yes];
#   );
#   node(w);
# );
# out skel;
