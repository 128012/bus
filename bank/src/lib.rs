// to not have to type these out twice

use std::collections::HashMap;

use chrono::Datelike;

#[derive(Clone,Copy,PartialEq,Eq,Hash,Ord,PartialOrd,Debug)]
pub enum BankHoliday{
    ChristmasDay,
    BoxingDay,
    
    NewYearsDay,
    
    GoodFriday,
    EasterMonday,
    
    LateSummer, //NotScotland
    MayDay,
    Spring, // SpringBank

    // Displacement Holidays
    ChristmasDaySubstitute,
    BoxingDaySubstitute,
    NewYearsDaySubstitute,

    // EarlyRunOffDays
    ChristmasEve,
    NewYearsEve,

    Range(chrono::naive::NaiveDate,chrono::naive::NaiveDate),
    Other(chrono::naive::NaiveDate)
}
impl BankHoliday{
    pub fn date(&self,until:chrono::naive::NaiveDate,bank_holidays:&BankHolidays)->Vec<chrono::naive::NaiveDate>{
        // and has not already happened
        let today=chrono::offset::Local::now().naive_local().date();
        match self{
            Self::ChristmasEve=>(0..).map(|x|{
                let first_year=if today.month()==12&&today.day()>24{
                    today.year()+1
                }else{
                    today.year()
                };
                chrono::naive::NaiveDate::from_ymd_opt(first_year+x,12,24).unwrap()
            }).take_while(|&x|x<=until).collect(),
            Self::ChristmasDay=>(0..).map(|x|{
                let first_year=if today.month()==12&&today.day()>25{
                    today.year()+1
                }else{
                    today.year()
                };
                chrono::naive::NaiveDate::from_ymd_opt(first_year+x,12,25).unwrap()
            }).take_while(|&x|x<=until).collect(),
            Self::BoxingDay=>(0..).map(|x|{
                let first_year=if today.month()==12&&today.day()>26{
                    today.year()+1
                }else{
                    today.year()
                };
                chrono::naive::NaiveDate::from_ymd_opt(first_year+x,12,26).unwrap()
            }).take_while(|&x|x<=until).collect(),
            Self::NewYearsEve=>(0..).map(|x|{
                let first_year=if today.month()==12&&today.day()>31{
                    today.year()+1
                }else{
                    today.year()
                };
                chrono::naive::NaiveDate::from_ymd_opt(first_year+x,12,31).unwrap()
            }).take_while(|&x|x<=until).collect(),
            Self::NewYearsDay=>(0..).map(|x|{
                let first_year=if today.month()==1&&today.day()>1{
                    today.year()+1
                }else{
                    today.year()
                };
                chrono::naive::NaiveDate::from_ymd_opt(first_year+x,1,1).unwrap()
            }).take_while(|&x|x<=until).collect(),

            Self::GoodFriday=>bank_holidays.good_fridays.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::EasterMonday=>bank_holidays.easter_mondays.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::LateSummer=>bank_holidays.late_summers.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::MayDay=>bank_holidays.may_days.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::Spring=>bank_holidays.springs.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),

            Self::ChristmasDaySubstitute=>bank_holidays.christmas_day_substitutes.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::BoxingDaySubstitute=>bank_holidays.boxing_day_substitutes.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            Self::NewYearsDaySubstitute=>bank_holidays.new_years_day_substitutes.iter().copied().filter(|&x|x>=today).take_while(|&x|x<=until).collect(),
            
            &Self::Range(start,end)=>start.iter_days().filter(|&x|x>=today).take_while(|&x|x<=end&&x<=until).collect(),
            &Self::Other(on)=>if on>=today && on<=until{vec![on]}else{vec![]}
        }
    }
}
pub struct BankHolidays{
    pub good_fridays:Vec<chrono::naive::NaiveDate>,
    pub easter_mondays:Vec<chrono::naive::NaiveDate>,
    pub late_summers:Vec<chrono::naive::NaiveDate>, //NotScotland
    pub may_days:Vec<chrono::naive::NaiveDate>,
    pub springs:Vec<chrono::naive::NaiveDate>, //SpringBank

    // displacement holidays
    pub christmas_day_substitutes:Vec<chrono::naive::NaiveDate>,
    pub boxing_day_substitutes:Vec<chrono::naive::NaiveDate>,
    pub new_years_day_substitutes:Vec<chrono::naive::NaiveDate>,
}
impl BankHolidays{
    pub fn bank_holiday_happens_on(&self,bank_holiday:&BankHoliday,date:&chrono::naive::NaiveDate)->bool{
        match bank_holiday{
            BankHoliday::ChristmasDay=>date.month()==12&&date.day()==25,
            BankHoliday::BoxingDay=>date.month()==12&&date.day()==26,

            BankHoliday::NewYearsDay=>date.month()==1&&date.day()==1,

            BankHoliday::GoodFriday=>self.good_fridays.contains(date),
            BankHoliday::EasterMonday=>self.easter_mondays.contains(date),

            BankHoliday::LateSummer=>self.late_summers.contains(date),
            BankHoliday::MayDay=>self.may_days.contains(date),
            BankHoliday::Spring=>self.springs.contains(date),
            
            BankHoliday::ChristmasDaySubstitute=>self.christmas_day_substitutes.contains(date),
            BankHoliday::BoxingDaySubstitute=>self.boxing_day_substitutes.contains(date),
            BankHoliday::NewYearsDaySubstitute=>self.new_years_day_substitutes.contains(date),

            BankHoliday::ChristmasEve=>date.month()==12&&date.day()==24,
            BankHoliday::NewYearsEve=>date.month()==12&&date.day()==31,

            BankHoliday::Range(start,end)=>start<=date&&date<=end,
            BankHoliday::Other(on)=>date==on
        }
    }
    pub fn from_csv(s:&str)->Result<Self,&'static str>{
        let mut bank_holidays=s
            .lines()
            .map(|line|line.trim().to_lowercase())
            .filter(|line|line.len()>0&&line.chars().next()!=Some('#'))
            .map::<Result<(String,Vec<chrono::naive::NaiveDate>),&'static str>,_>(|line|{
                let mut values=line.split(',');
                Ok((
                    values.next().ok_or("No bank holiday name in data/bank_holidays.txt")?.to_string(),
                    values.next().ok_or("No dates on which the bank holiday happens in data/bank_holidays.txt")?
                        .split("+")
                        .filter(|date|date.len()>0)
                        .map(|date|date.parse::<chrono::naive::NaiveDate>()
                            .map_err(|_|"invalid date in data/bank_holidays.txt")
                        )
                        .collect::<Result<Vec<_>,_>>()?
                ))
 
            })
            .collect::<Result<HashMap<_,_>,_>>()?;

        Ok(Self{
            good_fridays:bank_holidays.remove("goodfriday")
                .ok_or("No GoodFriday in data/bank_holidays.txt")?,
            easter_mondays:bank_holidays.remove("eastermonday")
                .ok_or("No EasterMonday in data/bank_holidays.txt")?,
            late_summers:bank_holidays.remove("latesummer")
                .ok_or("No LateSummer in data/bank_holidays.txt")?,
            may_days:bank_holidays.remove("mayday")
                .ok_or("No MayDay in data/bank_holidays.txt")?,
            springs:bank_holidays.remove("spring")
                .ok_or("No Spring in data/bank_holidays.txt")?,
                
            christmas_day_substitutes:bank_holidays.remove("christmasdaysubstitute")
                .ok_or("No ChristmasDaySubstitute in data/bank_holidays.txt")?,
            boxing_day_substitutes:bank_holidays.remove("boxingdaysubstitute")
                .ok_or("No BoxingDaySubstitute in data/bank_holidays.txt")?,
            new_years_day_substitutes:bank_holidays.remove("newyearsdaysubstitute")
                .ok_or("No NewYearsDaySubstitute in data/bank_holidays.txt")?,
        })
    }
}

