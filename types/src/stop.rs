use std::collections::HashSet;

use crate::service;

pub struct Stop{
    pub atco:String,
    pub naptan:Option<String>,
    pub names:Vec<String>,
    pub locality:String,
    pub street:String,
    pub easting:f32,
    pub northing:f32,
    pub lat:Option<f64>,
    pub lng:Option<f64>,
    pub timing:service::Timing,

    pub service_codes:HashSet<String>,
}
impl Stop{
    pub fn distance_squared(&self,(x,y):(f32,f32))->f32{
        distance_squared((x,y),(self.easting,self.northing))
    }
    pub fn add_latlng(&mut self,(lat,lng):(f64,f64)){
        self.lat.replace(lat);
        self.lng.replace(lng);
    }
    pub fn add_name(&mut self,new_name:&str){
        // name not already added
        // and name!=locality current name
        if !self.names.iter()
            .any(|name|name.as_str()==new_name||{
                if new_name.len()>self.locality.len(){
                    let (a,b)=new_name.split_at(self.locality.len());
                    a==self.locality.as_str()&&(
                        (b.chars().next().unwrap()==' '&&b[1..]==*name.as_str())
                      ||(
                            (new_name.len()>self.locality.len()+1)
                          &&(b[..2]==*", "&&b[2..]==*name.as_str())
                      )
                    )
                }else{false}
            })
        {
            self.names.push(new_name.to_string())
        }
    }
}
impl std::fmt::Debug for Stop{
    fn fmt(&self,f:&mut std::fmt::Formatter<'_>)->std::fmt::Result{
        write!(f,"{}: {}",self.atco,self.names.join(", "))
    }
}

pub trait StopCollection: Copy{
    fn by_atco<'a>(&'a self,atco:&str)->Option<&'a Stop>;
}


pub fn distance_squared((ax,ay):(f32,f32),(bx,by):(f32,f32))->f32{
    (ax-bx).powi(2)+(ay-by).powi(2)
}
#[test]
fn test_distance(){
    assert_eq!(25.0,distance_squared((3.0,5.0),(6.0,9.0)))
}
