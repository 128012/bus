use std::collections::{HashSet,HashMap};
use chrono::Datelike;

pub struct Service{
    pub code:String,
    pub colour:Option<String>,
    pub paths:Vec<[(f64,f64);2]>,

    pub ops:HashMap<OP,Vec<Trip>>,

    pub stops_at:HashSet<String>,
    pub dests:Vec<String>,
}
impl Service{
    pub fn new(
        code:String,
        colour:Option<&str>,
        paths:Vec<[(f64,f64);2]>,
        ops:HashMap<OP,Vec<Trip>>,
    )->Self{
        let mut dests=vec![];
        let mut stops_at=HashSet::new();
        for trip in ops.values().flatten(){
            if !dests.contains(&trip.dest){
                dests.push(trip.dest.clone());
            }
            for Visit{atco,..} in &trip.visits{
                if !stops_at.contains(atco){
                    stops_at.insert(atco.clone());
                }
            }
        }
        Self{
            dests,
            colour:colour.map(|x|x.to_string()),
            paths,
            code,
            stops_at,
            ops
        }
    }

    pub fn trips(&self)->impl Iterator<Item=& Trip>{
        self.ops.values().flatten()
    }

    pub fn code(&self)->&str{
        &self.code
    }

    pub fn atco_pairs(&self)->HashSet<(&str,&str)>{
        atco_pairs(self.trips())
    }

    pub fn on<'a>(&'a self,date:chrono::naive::NaiveDate,bank_holidays:&'a txc_bank::BankHolidays)->impl Iterator<Item=&'a Trip>{
        self.ops.iter()
            .filter(move|(op,_)|op.on(date,bank_holidays))
            .flat_map(|(_,trips)|trips)
    }
}

#[derive(Eq,PartialEq,Hash,Debug)]
pub struct OP{
    pub start:chrono::naive::NaiveDate,
    pub end:chrono::naive::NaiveDate,
    //pub weekdays:Vec<(u8,u8)>,
    pub weekdays:[bool;7],
    pub plus:Vec<YearDay>,
    pub minus:Vec<YearDay>
}
impl OP{
    pub fn on(&self,date:chrono::naive::NaiveDate,bank_holidays:&txc_bank::BankHolidays)->bool{
        let weekday:usize=date.weekday().num_days_from_monday() as usize;
        (
            self.weekdays[weekday]
            ||self.plus.iter().any(|x|x.on(date,bank_holidays))
        )
            &&(!self.minus.iter().any(|x|x.on(date,bank_holidays)))
            &&(self.start<=date)
            &&(date<=self.end)
    }
    pub fn concise(&self,until:chrono::naive::NaiveDate,bank_holidays:&txc_bank::BankHolidays)->ConciseOP{
        let today=chrono::offset::Local::now().naive_local().date();
        if today<self.start||self.end<until{
            ConciseOP{
                weekdays:vec![],
                plus:today.iter_days()
                    .take_while(|&d|d<=until)
                    .filter(|&d|self.on(d,bank_holidays))
                    .collect(),
                minus:vec![]
            }
        }else{
            ConciseOP{
                weekdays:weekdays_to_contiguous_groups(self.weekdays),
                plus:self.plus.iter()
                    .flat_map(|x|x.date(until,bank_holidays))
                    .collect(),
                minus:self.minus.iter()
                    .flat_map(|x|x.date(until,bank_holidays))
                    .collect()
            }
        }
    }


}
pub struct ConciseOP{
    // if start+end relevant, weekdays is not used; instead dates are specified in plus
    pub weekdays:Vec<(u8,u8)>,
    pub plus:Vec<chrono::naive::NaiveDate>,
    pub minus:Vec<chrono::naive::NaiveDate>
}
impl ConciseOP{
    pub fn any(&self)->bool{
        self.weekdays.len()>0
            ||self.plus.len()>0
            ||self.minus.len()>0
    }
}

fn weekdays_to_contiguous_groups(days:[bool;7])->Vec<(u8,u8)>{
    //let days=[0,0,1,1,1,1,1].map(|x|x==1);
    std::iter::once(days[0])
        .filter_map(|first_day|first_day.then(||0))
        .chain(days.windows(2)
            .map(|x|[x[0],x[1]])
            .enumerate()
            .filter(|&(_,[a,b])|!a&&b)
            .map(|(i,_)|i+1)
        )
        .map(|beginning|(
            u8::try_from(beginning).unwrap(),
            u8::try_from(beginning+days.iter().skip(beginning+1)
                .take_while(|day|**day)
                .count()
                ).unwrap()
        ))
        .collect()
}


pub fn atco_pairs<'a>(trips:impl Iterator<Item=&'a Trip>)->HashSet<(&'a str,&'a str)>{
    trips.flat_map(|trip|trip.visits.windows(2)
        .map(|two_acto_codes|if let [a,b]=two_acto_codes{
            let a_atco=a.atco.as_ref();
            let b_atco=b.atco.as_ref();

            if a_atco>b_atco{ // order doesn't matter
                (b_atco,a_atco)
            }else{
                (a_atco,b_atco)
            }
        }else{unreachable!("windows returns slices of length 2")})
    )
    // don't want duplicates
    .collect::<HashSet<(&str,&str)>>()
}

#[derive(Eq,PartialEq,Hash,Debug)]
pub enum YearDay{
    BankHoliday(txc_bank::BankHoliday),
    Date(chrono::naive::NaiveDate)
}
impl YearDay{
    fn date(&self,until:chrono::naive::NaiveDate,bank_holidays:&txc_bank::BankHolidays)->Vec<chrono::naive::NaiveDate>{
        match self{
            Self::BankHoliday(b)=>b.date(until,bank_holidays),
            Self::Date(d)=>vec![*d]
        }
    }
    fn on(&self,date:chrono::naive::NaiveDate,bank_holidays:&txc_bank::BankHolidays)->bool{
        match self{
            Self::BankHoliday(b)=>bank_holidays.bank_holiday_happens_on(b,&date),
            Self::Date(d)=>*d==date
        }
    }

}


pub struct Trip{
    pub direction:Direction,
    pub operator:&'static str,
    pub dest:String,
    pub visits:Vec<Visit>
}

// constructed in gtfs trip constructor
pub struct Visit{
    pub atco:String,
    pub timing:Timing,
    pub arr:u32, // seconds since midnight
    pub dep:u32,
    pub set_down_only:bool
}

#[derive(Debug,Copy,Clone,Eq,PartialEq,Ord,PartialOrd)]
pub enum Timing{
    Approximate,
    Exact,
}
impl Timing{
    pub fn from_uppercase(s:&str)->Self{
        match s{
            "PTP"=>Self::Exact,
            _=>Self::Approximate
        }
    }
    pub fn acronym(&self)->&'static str{
        match self{
            Self::Exact=>"PTP",
            Self::Approximate=>"OTH"
        }
    }
}


#[derive(Copy,Clone,Hash,PartialEq,Eq)]
pub enum Direction{
    Inbound,
    Outbound,
    Unknown
}
impl Direction{
    pub fn is_unknown(&self)->bool{
        if let Self::Unknown=self{
            true
        }else{
            false
        }
    }
    pub fn str(&self)->&'static str{
        match self{
            Self::Outbound=>"Outbound",
            Self::Inbound=>"Inbound",
            Self::Unknown=>""
        }
    }
}

pub fn duration_as_hms_string(duration:&chrono::Duration)->String{
    let h=duration.num_hours();
    let m=duration.num_minutes()-h*60;
    let s=duration.num_seconds()-m*60;
    let r=format!(
        "{}{}{}",
        if h>0{
            format!("{}h",h)
        }else{String::new()},
        if m>0{
            format!("{}m",m)
        }else{String::new()},
        if s>0{
            format!("{}s",s)
        }else{String::new()}
    );
    if r.len()>0{
        r
    }else{
        String::from("now")
    }
}
pub fn formatted_hms(t:u32)->String{
    let h=t/3600;
    let m=(t%3600)/60;
    //let s=(t%3600)%60;
    format!(
        "{h:0>2}:{m:0>2}",
        h=h,
        m=m,
        /*s=if s==0{
            String::new()
        }else{
            format!(":{}",s)
        }*/
    )
}

pub fn operator_short_name(long:&str)->&'static str{
    match long{
        "Oxford Bus Company"=>"obc",
        "Stagecoach Oxfordshire"=>"scox",
        "Arriva Beds and Bucks"=>"arriva",
        _=>"other"
    }
}
pub fn weekday_name(x:u8)->&'static str{
    match x{
        0=>"Monday",
        1=>"Tuesday",
        2=>"Wednesday",
        3=>"Thursday",
        4=>"Friday",
        5=>"Saturday",
        6=>"Sunday",
        _=>unreachable!()
    }
}
