#!/bin/sh
set -e

inkscape \
'--actions=select-by-class:not;delete;export-filename:bus/asset/map.svg;export-plain-svg;export-do;export-filename:bus/asset/map.png;export-background:white;export-background-opacity:1;export-do' \
map/map.svg

sed 's/>[[:space:]]\+</></' -i bus/asset/map.svg 2>&1 >/dev/shm/y
