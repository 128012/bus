function correct_string(s){
	return Number(s)-54.23984;
}
function correct_d(d){
	let bits=d.split(" ");
	if(bits[0]=="m"){
		let [x,y]=bits[1].split(",");
		return "m "+correct_string(x)+","+y+" "+bits.slice(2).join(" ");
	}
}
function correct_l(l){
	if(l.getAttribute("x")){
		l.setAttribute("x",correct_string(l.getAttribute("x")));
	}else if(l.getAttribute("cx")){
		l.setAttribute("cx",correct_string(l.getAttribute("cx")));
	}else if(l.getAttribute("sodipodi:cx")){
		l.setAttribute("sodipodi:cx",correct_string(l.getAttribute("sodipodi:cx")));
	}
	if(l.getAttribute("d")){
		l.setAttribute("d",correct_d(l.getAttribute("d")));
	}
	for(let c of l.children){
		correct_l(c);
	}
}
correct_l(document.documentElement);
